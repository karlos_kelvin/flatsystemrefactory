package br.com.cactus.flatsystem.beans;

import javax.faces.bean.ViewScoped;

@ViewScoped
public class AboutBean {

	private static final String PAG = "/pages/user/about.xhtml?faces-redirect=true";
	
	public String paginaSobre(){
		return PAG;
	}
}
