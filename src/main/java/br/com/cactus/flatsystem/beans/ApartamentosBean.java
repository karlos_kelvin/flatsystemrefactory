package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Apartamento;
import br.com.cactus.flatsystem.entities.TipoApartamento;
import br.com.cactus.flatsystem.service.ApartamentoService;
import br.com.cactus.flatsystem.service.TipoApartamentoService;

@ViewScoped
public class ApartamentosBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final String PAG_CADASTRO = "/pages/user/cadastroApartamentos?faces-redirect=true";
    private final String PAG_LISTA = "/pages/user/listaApartamentos?faces-redirect=true";

    private Apartamento apartamento = new Apartamento();
    private List<Apartamento> lista;

    private int tipo_apartamentoSelecionado;

    private List<TipoApartamento> listaTipo = new ArrayList<TipoApartamento>();

    @Inject
    private ApartamentoService apartamentoService;

    @Inject
    private TipoApartamentoService tipo_ApartamentoService;

    public String novo() {
        apartamento = new Apartamento();
        tipo_apartamentoSelecionado = 0;

        return PAG_CADASTRO;
    }

    public String salvar() {
        FacesContext context = FacesContext.getCurrentInstance();
        apartamento.setTipo(retornaTipoApartamento());
        apartamentoService.salvar(apartamento);
        FacesMessage msg = new FacesMessage("Apartamento salvo com sucesso!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        context.addMessage(null, msg);
        this.novo();
        this.lista = null;
        return PAG_LISTA;
    }

    public String cancelar() {
        this.lista = null;
        return PAG_LISTA;
    }

    public String editar() {
        tipo_apartamentoSelecionado = apartamento.getTipo().getId().intValue();
        return PAG_CADASTRO;
    }

    public String update() {
        apartamentoService.update(apartamento);
        this.lista = null;
        return PAG_LISTA;
    }

    public String remover() {
        apartamentoService.remover(apartamento);
        this.novo();
        this.lista = null;
        return PAG_LISTA;
    }

    // Lista de tipos apartamentos Cadastrados
    public List<SelectItem> getListaTipoApartamento() {
        List<SelectItem> lst = new ArrayList<SelectItem>();
        listaTipo = tipo_ApartamentoService.lista();

        lst.add(new SelectItem("", "Selecione"));
        for (int i = 0; i < listaTipo.size(); i++) {
            lst.add(new SelectItem(listaTipo.get(i).getId(), listaTipo.get(i).getDescricao()));
        }
        return lst;
    }

    public TipoApartamento retornaTipoApartamento() {
        int indice = -1;
        for (int i = 0; i < listaTipo.size(); i++) {
            if (listaTipo.get(i).getId() == Long.valueOf(getTipo_apartamentoSelecionado())) {
                indice = i;
                break;
            } else {
                continue;
            }
        }
        if (indice == -1) {
            return null;
        } else {
            return listaTipo.get(indice);
        }
    }

    public Apartamento getApartamento() {
        return apartamento;
    }

    public void setApartamento(Apartamento apartamento) {
        this.apartamento = apartamento;
    }

    public List<Apartamento> getLista() {
        if (this.lista == null) {
            lista = apartamentoService.lista();
        }
        return lista;
    }

    public void setLista(List<Apartamento> lista) {
        this.lista = lista;
    }

    public List<TipoApartamento> getListaTipo() {
        return listaTipo;
    }

    public void setListaTipo(List<TipoApartamento> listaTipo) {
        this.listaTipo = listaTipo;
    }

    public int getTipo_apartamentoSelecionado() {
        return tipo_apartamentoSelecionado;
    }

    public void setTipo_apartamentoSelecionado(int tipo_apartamentoSelecionado) {
        this.tipo_apartamentoSelecionado = tipo_apartamentoSelecionado;
    }

    public String listaApartamentos() {
        this.lista = null;
        return PAG_LISTA;
    }

}
