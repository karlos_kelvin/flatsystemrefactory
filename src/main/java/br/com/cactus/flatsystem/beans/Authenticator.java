package br.com.cactus.flatsystem.beans;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;


import br.com.cactus.flatsystem.entities.User;
import br.com.cactus.flatsystem.service.LoginService;
import javax.inject.Inject;


@Named
public class Authenticator {
	
	@Inject
	private LoginService service;

	@Inject
	private UserSession session;

	private String username;
	private String password;

	public String login() {
		try {
			User user = service.login(username, password);
			session.setUser(user);
			return "/pages/user/successfulPage?faces-redirect=true";
		} catch (IllegalArgumentException ex) {
			message("Erro! Verifique usuário e senha.");
		}
		return "";
	}

	private void message(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(message));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserSession getSession() {
		return session;
	}

	
}
