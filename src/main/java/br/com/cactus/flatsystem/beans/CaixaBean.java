package br.com.cactus.flatsystem.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.model.StreamedContent;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Compra;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.entities.Venda;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.FormasPagamentoService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.service.RelatorioService;
import br.com.cactus.flatsystem.service.TransacaoService;
import br.com.cactus.flatsystem.service.VendaService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;

@ApplicationScoped
public class CaixaBean {

	private final String PAG_CADASTRO = "/pages/user/caixa?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaCaixa?faces-redirect=true";

	private Caixa caixa = new Caixa();
	
	//valores para sangria suprimento e abertura
	private double valorAbertura;
	private double valorSuprimento;
	private double valorSangria;
	private String observacao;
	
	private int selectedTransacao;
	
	private Lancamento lancamentoSelecionado;
	private Lancamento lancamentoSelecionado2;
	
	private Transacao transacao;
	private FormasPagamento formaPagamento;
	
	private List<Transacao> trasacoes;
	private List<Venda> listaVenda;
	private List<Compra> listaCompra;
	private List<Lancamento> listaSangria;
	private List<Lancamento> listaSuprimento;
	private List<Lancamento> lancamentosCaixa;
	private List<Caixa> listaCaixas;
	

	@Inject
	private LancamentoService lancamentoService;
	@Inject
	private CaixaService caixaService;
	@Inject
	private TransacaoService transacaoService;
	@Inject
	private VendaService vendaService;
	@Inject
	private FormasPagamentoService formaPagamentoService;

	public CaixaBean() {
		
	}

	public String novo() {
		caixa = new Caixa();
		return PAG_CADASTRO;
	}

	public void fecharCaixa() {
		try {
			caixa = caixaService.caixaAtual();
			caixa.setDataFechamento(new Date());
			caixaService.salvar(caixa);
			
			MessagesController.addInfo("Info:", "Caixa fechado com sucesso!");
		} catch (Exception e) {
			MessagesController.addError("Erro:", "Caixa fechado!");
		}
	}

	public String abrir() {
		if (getCaixaAberto()) {
			// ja existe um caixa aberto
		} else {
			
			if (valorAbertura > 0) {
				caixa = new Caixa(new Date());
				caixa.getLancamentos().add(criaLancamentoEntrada());
				caixaService.salvar(caixa);
				MessagesController.addInfo("Info:", "Caixa aberto com sucesso!");
				valorAbertura = 0;
			}else{
				MessagesController.addWarn("Info:", "O caixa não pode ser aberto sem um valor inicial!");
			}
		}
		observacao = null;
		return PAG_CADASTRO;
	}

	private Lancamento criaLancamentoEntrada() {
		
		Lancamento abertura 
					= new Lancamento(transacaoService.findById(2), //suprimento
									 null, 
									 valorAbertura, 
									 observacao, 
									 new Date(), 
									 true);
		
		abertura.setFormaPagamento(formaPagamentoService.findById(1));
		abertura.setCaixa(caixa);
		return abertura;
	}

	private boolean getCaixaAberto() {
		boolean retorno = false;

		try {
			retorno = caixaService.caixaAtual() != null;
		} catch (CaixaFechadoException e) {
			retorno = false;
		}
		return retorno;
	}

	/**
	 * 
	 * @return Retorna o somatório dos lançaamentos por transação do caixa atual
	 */
	public List<Lancamento> getLancamentoTransacoes() {
		List<Lancamento> retorno = null;
		try {
			Caixa caixa = caixaService.caixaAtual();
			if (caixa != null){
				retorno = lancamentoService.getLancamentoTransacoes(caixa);
			}
		} catch (CaixaFechadoException e) {
			//MessagesController.addWarn("Atenção", e.getMessage());
		}
		return retorno;
	}
	
	/**
	 * 
	 * @return retorna todos o lançamentos de uma transação 
	 */
	public List<Lancamento> getTodosLancamentosTransacoes() {
		List<Lancamento> retorno = null;
		try {
			Caixa caixa = caixaService.caixaAtual();
			if (this.lancamentoSelecionado != null) {
				if (caixa != null && this.lancamentoSelecionado.getTransacao() != null){
					retorno = lancamentoService.getLancamentoTransacoes(caixa, this.lancamentoSelecionado.getTransacao());
				}
			}
			this.transacao = null;
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return retorno;
	}
	
	public void autalizaListaLancamentoTransaoes(){
		getTodosLancamentosTransacoes();
	}

	public List<Lancamento> getLancamentoFormasPagamento() {
		List<Lancamento> retorno = null;
		try {
			Caixa caixa = caixaService.caixaAtual();
			if (caixa != null){
				retorno = lancamentoService.getLancamentoFormasPagamento(caixa);
			}			
		} catch (CaixaFechadoException e) {
			//MessagesController.addWarn("Atenção", e.getMessage());
		}
		return retorno;
	}

	/**
	 * 
	 * @return retorna todos o lançamentos de uma forma de pagamento 
	 */
	public List<Lancamento> getTodosLancamentosPorFormaDePagamento() {
		List<Lancamento> retorno = null;
		try {
			Caixa caixa = caixaService.caixaAtual();
			if (this.lancamentoSelecionado2 != null) {
				if (caixa != null && this.lancamentoSelecionado2.getFormaPagamento() != null){
					retorno = lancamentoService.getTodosLancamentosPorFormaDePagamento(caixa, this.lancamentoSelecionado2.getFormaPagamento());
				}
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
		} 
		return retorno;
	}
	
	public void atualizaListaLancamentoPorFormaDePagamento(){
		getTodosLancamentosPorFormaDePagamento();
	}
	
	public String editar() {
		return PAG_CADASTRO;
	}

	public String continuar() {
		return PAG_CADASTRO;
	}

	public String remover() {
		this.caixaService.remover(this.caixa);
		return null;
	}

	public String cancelar() {
		return "/pages/successfulPage";
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	public double getValorAbertura() {
		return valorAbertura;
	}

	public void setValorAbertura(double valorAbertura) {
		this.valorAbertura = valorAbertura;
	}

	public int getSelectedTransacao() {
		return selectedTransacao;
	}

	public void setSelectedTransacao(int selectedTransacao) {
		this.selectedTransacao = selectedTransacao;
	}

	public List<SelectItem> getTrasacoes() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		trasacoes = transacaoService.lista();
		for (Transacao t : trasacoes) {
			lst.add(new SelectItem(t.getId(), String.valueOf(t.getId() + " - " + t.getDescricao())));
		}
		return lst;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<Venda> getListaVenda() {
		if (listaVenda == null) {
			try {
				listaVenda = vendaService.getVendaPorCaixa(caixaService.caixaAtual());
			} catch (CaixaFechadoException e) {
				//MessagesController.addWarn("Aten��o", e.getMessage());
			}
		}
		return listaVenda;
	}

	public List<Compra> getListaCompra() {
		return listaCompra;
	}

	public double getTotalVenda() {
		double retorno = 0;
		if (listaVenda != null) {
			for (Venda v : listaVenda) {
				retorno += v.getTotal();
			}
		}
		return retorno;
	}

	public void setListaCompra(List<Compra> listaCompra) {
		this.listaCompra = listaCompra;
	}

	public List<Lancamento> getListaSangria() {
		return listaSangria;
	}

	public void setListaSangria(List<Lancamento> listaSangria) {
		this.listaSangria = listaSangria;
	}

	public List<Lancamento> getListaSuprimento() {
		return listaSuprimento;
	}

	public void setListaSuprimento(List<Lancamento> listaSuprimento) {
		this.listaSuprimento = listaSuprimento;
	}

	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}

	public double getValorTotalCaixa() {
		double retorno = 0;
		lancamentosCaixa = new ArrayList<Lancamento>();
		try {
			Caixa caixa = caixaService.caixaAtual();
			if (caixa != null){
				lancamentosCaixa = lancamentoService.getLancamentosCaixa(caixa);
			}
			for (Lancamento l : lancamentosCaixa) {
				retorno += l.getValorPorTransacao();
			}
		} catch (CaixaFechadoException e) {
			
		}
		
		return retorno;
	}
	
	public Date getDataAbertura(){
		Date d = null;
		
		try {
			d = caixaService.caixaAtual().getDataAbertura();
		} catch (CaixaFechadoException e) {
			e.printStackTrace();
			MessagesController.addError("Erro", "Nenhum caixa aberto!");
		}
		
		return d;
	}

	public List<Caixa> getListaCaixas(){
		if (listaCaixas == null || listaCaixas.size() == 0) {
			listaCaixas = caixaService.listaCaixasFechados();
		}
		
		return listaCaixas;
	}
	
	public void setListaCaixas(List<Caixa> listaCaixas) {
		this.listaCaixas = listaCaixas;
	}
	
	public String visualizarLancamentos(Caixa c){
		return null;
	}
	
	public boolean temCaixaAberto(){
		return caixaService.temCaixaAberto();
	}

	public Lancamento getLancamentoSelecionado() {
		return lancamentoSelecionado;
	}

	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}

	public List<Lancamento> getLancamentosCaixa() {
		if( (lancamentosCaixa != null) || (lancamentosCaixa.isEmpty())){
			lancamentosCaixa = lancamentoService.getLancamentosCaixa(caixa);
		}
		
		return lancamentosCaixa;
	}

	public void setLancamentosCaixa(List<Lancamento> lancamentosCaixa) {
		this.lancamentosCaixa = lancamentosCaixa;
	}

	public double getValorSuprimento() {
		return valorSuprimento;
	}

	public void setValorSuprimento(double valorSuprimento) {
		this.valorSuprimento = valorSuprimento;
	}

	public double getValorSangria() {
		return valorSangria;
	}

	public void setValorSangria(double valorSangria) {
		this.valorSangria = valorSangria;
	}
	
	public String realizarSuprimento(){
		if (this.valorSuprimento > 0){
			try {
				lancamentoService.criaSuprimento(this.valorSuprimento, this.observacao);
				this.valorSuprimento = 0;	this.observacao = null;
				
				MessagesController.addInfo("Info", "Suprimento realizado com sucesso!");
			} catch (CaixaFechadoException e) {
				e.printStackTrace();
			}
			
		}else{
			MessagesController.addError("Erro", "Valor zerado!");
		}
		this.valorSuprimento = 0;
		return PAG_CADASTRO;
	}
	
	public String realizarSangria(){
		
		if(this.valorSangria > 0){
			try {
				lancamentoService.criaSangria(this.valorSangria, this.observacao);
				this.valorSangria = 0; this.observacao = null;
				MessagesController.addInfo("Info", "Sangria realizada com sucesso!");
				
			} catch (CaixaFechadoException e) {
				e.printStackTrace();
				MessagesController.addError("Erro", e.getMessage());
			}						
		}else{
			MessagesController.addError("Erro", "Valor zerado!");
		}
		this.valorSangria = 0;
		return PAG_CADASTRO;
	}

	public Transacao getTransacao() {
		return transacao ;
	}

	public void setTransacao(Transacao transacao) {
		
		this.transacao = transacao;
		getTodosLancamentosTransacoes();
	}

	public FormasPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormasPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
		getTodosLancamentosPorFormaDePagamento();
	}

	public Lancamento getLancamentoSelecionado2() {
		return lancamentoSelecionado2;
	}

	public void setLancamentoSelecionado2(Lancamento lancamentoSelecionado2) {
		this.lancamentoSelecionado2 = lancamentoSelecionado2;
	}
	
	public String listaCaixas(){
		this.listaCaixas = null;
		return PAG_LISTA;
	}
	
	public StreamedContent getRelatorioCaixaAtual() throws CaixaFechadoException {
		fecharCaixa();
		
		RelatorioService relatorio = new RelatorioService(FacesContext.getCurrentInstance());
		
		return relatorio.getRelatorioCaixa(caixa);
	}

	public StreamedContent getRelatorioCaixaLista(){
		RelatorioService relatorio = new RelatorioService(FacesContext.getCurrentInstance());
		
		return relatorio.getRelatorioCaixa(caixa);
	}

	
}
