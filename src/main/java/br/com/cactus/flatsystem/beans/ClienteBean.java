package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;

import org.primefaces.model.StreamedContent;

import br.com.cactus.flatsystem.entities.Cliente;
import br.com.cactus.flatsystem.service.ClienteService;
import br.com.cactus.flatsystem.util.RelatorioUtil;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ClienteBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private final String PAG_CADASTRO = "/pages/user/cadastroCliente?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaCliente?faces-redirect=true";

	private Cliente cliente = new Cliente();
	
	private List<Cliente> lista;
	
	//classe para relatários
	private StreamedContent arquivoRetorno;
	private int tipoRelatorio;
	private boolean contemErro;
        
	@Inject
	private ClienteService clienteService;
	
	public String novo(){
		cliente = new Cliente();
		return PAG_CADASTRO;
	}
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String salvar(){			
		if (verificaCPF()){
			return null;
		}else{
			clienteService.salvar(cliente);
			this.novo();
			this.lista = null;
			return "/pages/user/listaCliente";
		}		
	}
	
	public void novoClienteHospedagem(){
		cliente = new Cliente();
	}
	
	public void salvarClienteHospedagem(){
		
		if (!verificaCPF()){
			if (cliente == null){
				clienteService.salvar(cliente);
				this.novoClienteHospedagem();
				this.lista = null;	
			}else {
				clienteService.update(cliente);
				this.novoClienteHospedagem();
				this.lista = null;
			}
		}
		
	}
	
	public void cancelarDialog(){
		   this.novoClienteHospedagem();
	}
	
	public String editar(){
		return PAG_CADASTRO;
	}
	
	public String update(){
		 if(verificaCPF()){
			 return null;
		 }else{
			clienteService.update(cliente);
			this.lista = null;
			return PAG_LISTA;
		 }
	}
	
	public String remover(){
		clienteService.remover(cliente);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public boolean verificaCPF(){
		if (cliente.getCpf().equals("") || cliente.getId() == null){
			return clienteService.verificaCPF(cliente.getCpf());
		}else {
			return clienteService.verificaCPFUpdate(cliente.getCpf(), cliente.getId());
		}
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Cliente> getLista() {
		if (this.lista == null){
			lista = clienteService.lista();
		}
		return lista;
	}

	public void setLista(List<Cliente> lista) {
		this.lista = lista;
	}

	public StreamedContent getArquivoRetorno() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		RelatorioUtil relatorioUtil = new RelatorioUtil();
		try {
			arquivoRetorno = relatorioUtil.geraRelatorio(null, "Clientes", "Clientes", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivoRetorno;
	}
	

	public void setArquivoRetorno(StreamedContent arquivoRetorno) {
		this.arquivoRetorno = arquivoRetorno;
	}

	public int getTipoRelatorio() {
		return tipoRelatorio;
	}

	public void setTipoRelatorio(int tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	public String listaCliente(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public void limpar(){
		this.lista =  null;
	}

	public boolean isContemErro() {
		return contemErro;
	}

	public void setContemErro(boolean contemErro) {
		this.contemErro = contemErro;
	}
	
}
