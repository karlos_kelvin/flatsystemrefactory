package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import br.com.cactus.flatsystem.entities.Compra;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.service.CompraService;
import br.com.cactus.flatsystem.service.FormasPagamentoService;
import br.com.cactus.flatsystem.service.ProdutoService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@ViewScoped
public class CompraBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8972315498728646927L;
	
	private final String PAG = "/pages/user/entradaProduto?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaCompra.xhtml?faces-redirect=true";
	
	private Compra compra = new Compra();
	private Produto produto;
	private List<Produto> listaProduto;
	private List<FormasPagamento> listaFormaPagamento;
	private float quantidade;
	

	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private FormasPagamentoService formaPagamentoService;
	
	@Inject
	private CompraService compraService;

	
	public String novo(){
		compra = new Compra(true);
		produto = new Produto();
		quantidade = 0;
		return PAG;
	}
	
	public String continuar(){
		return PAG;
	}
	
	public String salvar(){
		String retorno = null;
		if ((quantidade > 0) && (produto != null)) {
			
			ItemMovimento im = new ItemMovimento();
			im.setProduto(produtoService.findById(produto.getId().intValue()));
			im.setQuantidade(quantidade);
			im.setMovimento(this.compra);
			
			if (this.compra.getId() == null || this.compra.getId() == 0) {
				compra.setData(new Date());
			}else {
				compra.getItensMovimento().clear();
			}
			
			compra.getItensMovimento().add(im);
			compraService.salvar(compra);
			this.novo();
			
			retorno = "/pages/user/listaProduto";
		}else{
			MessagesController.addError("Erro:", "Informe a quantidade do produto !");
		}
		
		return retorno;
		
	}
	
	public String cancelar(){
		this.compra.setFornecedor(null);
		this.produto = null;
		this.quantidade = 0;
		return "/pages/user/listaProduto";
	}
	
	public List<SelectItem> getListaFormaPagamentos(){
		List<SelectItem> lst = new ArrayList<SelectItem>();
		
		listaFormaPagamento = formaPagamentoService.lista();
		
		lst.add(new SelectItem("", "Selecione"));
		for(int i = 0; i < listaFormaPagamento.size(); i++){
			lst.add(new SelectItem(listaFormaPagamento.get(i).getDescricao(), String.valueOf(listaFormaPagamento.get(i).getDescricao())));
		}
		
		return lst;
	}
	
	
	
	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public List<Produto> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(List<Produto> listaProduto) {
		this.listaProduto = listaProduto;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	public List<FormasPagamento> getFormaPagamento() {
		return listaFormaPagamento;
	}

	public void setFormaPagamento(List<FormasPagamento> formaPagamento) {
		this.listaFormaPagamento = formaPagamento;
	}
	
	public String editar(){
		
		this.produto = this.compra.getItemMovimento().getProduto();
		this.quantidade = this.compra.getItemMovimento().getQuantidade();
		return PAG;
		
	}
	
	public String listaCompras(){
		return PAG_LISTA;
	}

}
	