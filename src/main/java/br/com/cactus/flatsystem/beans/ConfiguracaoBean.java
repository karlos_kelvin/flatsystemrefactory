package br.com.cactus.flatsystem.beans;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Configuracao;
import br.com.cactus.flatsystem.entities.Estabelecimento;
import br.com.cactus.flatsystem.service.ConfiguracaoService;
import br.com.cactus.flatsystem.service.EstabelecimentoService;


@ApplicationScoped
public class ConfiguracaoBean {
	
	private static final String PAG = "/pages/admin/configuracaoDoSistema?faces-redirect=true";
	private static final String PAG_CANCEL = "/pages/user/successfulPage?faces-redirect=true";
	
	private Estabelecimento estabelecimento;
	private Configuracao config;
	
	@Inject
	private EstabelecimentoService estabelecimentoService;
	@Inject
	private ConfiguracaoService configService;
	
	@PostConstruct
	public String novo(){
		if (estabelecimentoService.lista().isEmpty()){
			estabelecimento = new Estabelecimento();
		}else {
			estabelecimento = estabelecimentoService.lista().get(0);
		}
		
		if (configService.lista().isEmpty()){
			config = new Configuracao();
		}else {
			config = configService.lista().get(0);
		}
		
		return null;
	}
	
	public String salvarEstabelecimento(){
		if (estabelecimentoService.lista().isEmpty()){
			estabelecimentoService.salvar(estabelecimento);
			FacesMessage msg = new FacesMessage("Sucesso!", "Estabelecimento Salvo!");  
		    FacesContext.getCurrentInstance().addMessage(null, msg);  
		}else {
			estabelecimentoService.update(estabelecimento);
			FacesMessage msg = new FacesMessage("Sucesso!", "Estabelecimento Editado! ");  
		    FacesContext.getCurrentInstance().addMessage(null, msg);  
		}
		
		return PAG;
	}
	
	public String salvarConfig(){
		if (configService.lista().isEmpty()){
			configService.salvar(config);
			 FacesMessage msg = new FacesMessage("Sucesso!", "Configuração Salva!");  
		     FacesContext.getCurrentInstance().addMessage(null, msg);  
		}else{
			configService.update(config);
			FacesMessage msg = new FacesMessage("Sucesso!", "Configuração Editada!");    
		    FacesContext.getCurrentInstance().addMessage(null, msg);  
		}
		
		return PAG;
	}

	public String cancelar(){
		return PAG_CANCEL;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Configuracao getConfig() {
		return config;
	}

	public void setConfig(Configuracao config) {
		this.config = config;
	}
	
	public String pagConfiguracao(){
		return PAG;
	}
	
}
