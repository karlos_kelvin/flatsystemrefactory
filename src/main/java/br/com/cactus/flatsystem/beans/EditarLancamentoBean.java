package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.inject.Named;

import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.service.TransacaoService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;



@Named
public class EditarLancamentoBean implements Serializable{

	
	private static final long serialVersionUID = -2953572595192569202L;
	
	private static final String HOSPEDAGEM = "HOSPEDAGEM";
	
	private static final String PAG_LISTA = "/pages/user/listaLancamento?faces-redirect=true";
	private static final String PAG_DETALHES_HOSPEDAGEM = "/pages/user/detalhesHospedagem?faces-redirect=true";
	
	//ATRIBUTOS
	private Lancamento lancamento = new Lancamento();
	private Transacao transacao;
	private FormasPagamento formaPagamento;
	private Date data;
	
	private String paginaRetorno;
	
	//SERVICES
	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private TransacaoService transacaoService;
	
	@Inject
	private CaixaService caixaService;

	
	public String editar(){
		String retorno = "";
		
		if(testaValores()){
			lancamento = lancamentoService.findByID(lancamento);
			
				lancamento.setTransacao(transacao);
				lancamento.setFormaPagamento(formaPagamento);
				
				lancamentoService.update(lancamento);
				
				retorno = PAG_LISTA;
		}
		
		return retorno;
				
	}
	
	public boolean hospedagemFechada(){
		boolean retorno = false;
		
		//Devido ao lancamento nao esta vinculado a uma hospedagem
		try {
			if (!lancamento.getHospedagem().isAberta()){
				retorno = true;
			}
		} catch (NullPointerException e) {
			
		}
		
		
		return retorno;
	}
	
	public boolean testaValores(){
		boolean retorno = ((this.formaPagamento != null) && 
				   (this.transacao != null) && 
				   (lancamento.getValor() > 0));
		
		if(lancamento.getTransacao() == null){
			MessagesController.addWarn("Transacao", "Transação não informada!");
		}
		
		if (lancamento.getFormaPagamento() == null){
			MessagesController.addWarn("Forma de Pagamento", "Forma de Pagamento não informada!");
		}
		
		if (lancamento.getValor() <= 0){
			MessagesController.addWarn("Valor", "Valor não pode ser menor ou igual a ZERO!");
		}
		
		return retorno;
	}
	
	public String continuar(){
		return "/pages/user/editarLancamento";
	}
	
	public String cancelar(){
		String retorno = null;
		
		//Devido a lancamento nao esta vinculado a Hospedagem
		try {
			if(paginaRetorno.equals(HOSPEDAGEM)){
				retorno = PAG_DETALHES_HOSPEDAGEM;
			}
		} catch (Exception e) {
			retorno = PAG_LISTA;
		}
		
		
		return retorno;
	}
	
	public String getDataAtual(){
		String retorno = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
		retorno = sdf.format(new Date());
		return retorno;
	}

	//GETTERS AND SETTERS
	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}

	public FormasPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormasPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	public static String getHospedagem() {
		return HOSPEDAGEM;
	}
	
	
	
}
