package br.com.cactus.flatsystem.beans;

import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.Endereco;
import br.com.cactus.flatsystem.service.EnderecoService;
import javax.inject.Inject;


@ViewScoped
public class EnderecoBean {
	
	private final String PAG_CADASTRO = "/pages/user/cadastroEndereco";
	private final String PAG_LISTA = "/pages/user/listaEndereco";
	
	private Endereco endereco = new Endereco();
	private List<Endereco> lista;
	
	@Inject
	private EnderecoService enderecoService;
	
	public String novo() {
		this.endereco = new Endereco();
		return PAG_CADASTRO;
	}
	
	public String salvar() {
		enderecoService.salvar(this.endereco);
		this.novo();
		this.lista = null;
		return null;//voltar página
	}
	
	public String update() {
		enderecoService.update(this.endereco);
		this.lista = null;
		return PAG_CADASTRO;
	}
	
	public String remover() {
		enderecoService.remover(this.endereco);
		this.novo();
		this.lista = null;
		return null;
	}
	
	public String editar() {
		return PAG_CADASTRO;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Endereco> getLista() {
		if (this.lista == null){
			this.lista = enderecoService.lista();
		}
		return lista;
	}

	public void setLista(List<Endereco> lista) {
		this.lista = lista;
	}
	
	

}
