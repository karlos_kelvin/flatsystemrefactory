package br.com.cactus.flatsystem.beans;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@ViewScoped
public class ErrorBean {

	public ErrorBean() {
	}

	public String getStackTrace() {
		// Get the current JSF context
		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestMap();

		// Fetch the exception
		Throwable ex = (Throwable) requestMap.get("javax.servlet.error.exception");

		// Create a writer for keeping the stacktrace of the exception
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);

		// Fill the stack trace into the write
		fillStackTrace(ex, pw);

		return writer.toString();
	}

	/**
	 * Write the stack trace from an exception into a writer.
	 * 
	 * @param ex
	 *            Exception for which to get the stack trace
	 * @param pw
	 *            PrintWriter to write the stack trace
	 */
	private void fillStackTrace(Throwable ex, PrintWriter pw) {
		if (null == ex) {
			return;
		}

		ex.printStackTrace(pw);

		Throwable cause = ((Exception) ex).getCause();
		if (null != cause) {
			pw.println("Root Cause:");
			fillStackTrace(cause, pw);
		}

	}
}