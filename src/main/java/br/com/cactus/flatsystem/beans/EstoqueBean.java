package br.com.cactus.flatsystem.beans;

import java.io.Serializable;

import javax.faces.view.ViewScoped;

import org.primefaces.model.chart.PieChartModel;

import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.service.SaldoEstoqueService;
import javax.inject.Inject;


@ViewScoped
public class EstoqueBean implements Serializable{

	private final String PAG = "/pages/user/estoqueProduto?faces-redirect=true";
	private final String PAG_PRODUTOS = "/pages/user/listaProduto?faces-redirect=true";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8940066699187074598L;
	
	private Produto produto;
	private PieChartModel chartProduto = new PieChartModel();
	
	@Inject
	private SaldoEstoqueService saldoEstoqueService;
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
		preencheSaldo();
		
	}
	
	private void fillChart() {   
		if (chartProduto != null) {
			chartProduto.clear();
		}else{
			chartProduto = new PieChartModel();
		}
		
        chartProduto.set("Compra", produto.getTotal_compra());
        chartProduto.set("Vendas", produto.getTotal_venda());

    }
	
	public void preencheSaldo(){
		if(produto != null){
			produto.setTotal_compra(saldoEstoqueService.totalCompra(produto).doubleValue());
			produto.setTotal_venda(saldoEstoqueService.totalVenda(produto).doubleValue());
			produto.setSaldo(produto.getTotal_compra() - produto.getTotal_venda());
		}
	}
	
	public PieChartModel getChartProduto() {
		fillChart();
        return chartProduto;  
    }
	
	public String novo(){
		preencheSaldo();
		return PAG;
	}
	
	public String voltar(){
		
		return PAG_PRODUTOS;
	}

}
