package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.service.FormasPagamentoService;
import javax.inject.Inject;


@ViewScoped
public class FormasPagamentoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String PAG_LISTA = "/pages/user/listaFormasPagamento?faces-redirect=true";

	private FormasPagamento formas_pagamento = new FormasPagamento();
	private List<FormasPagamento> lista;
	
	@Inject
	private FormasPagamentoService formas_PagamentoService;
	
	public String novo(){
		formas_pagamento = new FormasPagamento();
		
		return "/pages/user/cadastroFormasPagamento";
	}
	
	public String salvar(){
		//FacesContext context = FacesContext.getCurrentInstance();
		formas_PagamentoService.salvar(formas_pagamento);
		//FacesMessage msg = new FacesMessage("Apartamento salvo com sucesso!");
		//msg.setSeverity(FacesMessage.SEVERITY_INFO);
        //context.addMessage(null, msg);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String editar(){
		return "/pages/user/cadastroFormasPagamento";
	}
	
	public String update(){
		formas_PagamentoService.update(formas_pagamento);
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String remover(){		
		formas_PagamentoService.remover(formas_pagamento);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public FormasPagamento getFormas_Pagamento() {
		return formas_pagamento;
	}

	public void setFormas_Pagamento(FormasPagamento formas_pagamento) {
		this.formas_pagamento = formas_pagamento;
	}

	public List<FormasPagamento> getLista() {
		if (this.lista == null){
			lista = formas_PagamentoService.lista();
		}
		return lista;
	}

	public void setLista(List<FormasPagamento> lista) {
		this.lista = lista;
	}
	
	public boolean isDisable(long id){
		boolean retorno = false;
		if (id == 1) {
			retorno = true;
		}
		return retorno;
	}
	
	public String listaFormasPagamento(){
		this.lista = null;
		return PAG_LISTA;
	}
}
