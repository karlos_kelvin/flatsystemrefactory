package br.com.cactus.flatsystem.beans;

import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.Endereco;
import br.com.cactus.flatsystem.entities.Fornecedor;
import br.com.cactus.flatsystem.service.FornecedorService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;


@ViewScoped
public class FornecedorBean {

	private final String PAG_CADASTRO = "/pages/user/cadastroFornecedor?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaFornecedor?faces-redirect=true";

	private Fornecedor fornecedor = new Fornecedor();
	private List<Fornecedor> lista;

	@Inject
	private FornecedorService fornecedorService;
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}

	public String novo() {
		this.fornecedor = new Fornecedor();
		return PAG_CADASTRO;
	}

	public String editar() {
		if (this.fornecedor.getEndereco()==null) {
			this.fornecedor.setEndereco(new Endereco());			
		}
		return PAG_CADASTRO;
	}

	public String update() {
		fornecedorService.update(this.fornecedor);
		this.lista = null;
		return PAG_CADASTRO;
	}

	public String salvar() {
		String retorno = null;
		if (verificaCampos()) {
			fornecedorService.salvar(this.fornecedor);
			this.novo();
			this.lista = null;
			MessagesController.addInfo("info:", "Dados salvos com sucesso!");
			retorno = PAG_LISTA; 
		}
		return retorno;// voltar página
	}

	public boolean verificaCampos() {
		boolean retorno = true;
		if (this.fornecedor.getRazaoSocial().equals("")
				|| this.fornecedor.getFantasia().equals("")) {
			MessagesController.addError("Campos Obrigatórios:","Preencha a razão social e/ou a fantasia!");
			retorno = false;
		}
		if (this.fornecedor.getCnpj().equals("")) {
			MessagesController.addError("Campos Obrigatórios:","Preencha o campo CNPJ!");
			retorno = false;
		}
		Endereco end = this.fornecedor.getEndereco();
		if (end.getCidade().equals("")) {
			MessagesController.addError("Campos Obrigatórios:","Preencha o campo cidade!");
			retorno = false;
		}
		if (end.getRua().equals("")) {
			MessagesController.addError("Campos Obrigatórios:","Preencha o campo rua!");
			retorno = false;
		}
		if (end.getNumero().equals("")) {
			MessagesController.addError("Campos Obrigatórios:","Preencha o campo número!");
			retorno = false;
		}
		return retorno;
	}

	public String remover() {
		fornecedorService.remover(this.fornecedor);
		this.novo();
		this.lista = null;
		return null;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public List<Fornecedor> getLista() {
		if (this.lista == null) {
			this.lista = fornecedorService.lista();
		}
		return lista;
	}

	public void setLista(List<Fornecedor> lista) {
		this.lista = lista;
	}

	public String listaFornecedores(){
		this.lista = null;
		return PAG_LISTA;
	}
}
