package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.StreamedContent;

import br.com.cactus.flatsystem.entities.Apartamento;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Cliente;
import br.com.cactus.flatsystem.entities.Configuracao;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.entities.Venda;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.service.ApartamentoService;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.CalendarioService;
import br.com.cactus.flatsystem.service.ClienteService;
import br.com.cactus.flatsystem.service.ConfiguracaoService;
import br.com.cactus.flatsystem.service.FormasPagamentoService;
import br.com.cactus.flatsystem.service.HospedagemService;
import br.com.cactus.flatsystem.service.ItemVendaService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.service.TransacaoService;
import br.com.cactus.flatsystem.service.VendaService;
import br.com.cactus.flatsystem.util.DiariasFacade;
import br.com.cactus.flatsystem.util.EventIPousada;
import br.com.cactus.flatsystem.util.RelatorioUtil;
import br.com.cactus.flatsystem.util.StatusHospedagemEnum;
import br.com.cactus.flatsystem.view.MessagesController;



@ViewScoped
public class HospedagemBean implements Serializable {

	private static final String PAG_CADASTRO = "/pages/user/cadastroHospedagem?faces-redirect=true";
	private static final String PAG_EDITAR   = "/pages/user/editarHospedagem?faces-redirect=true";
	private static final String PAG_DETALHES = "/pages/user/detalhesHospedagem?faces-redirect=true";
	private static final String PAG_LISTA 	 = "/pages/user/listaHospedagem?faces-redirect=true";

	private static final long serialVersionUID = 1L;

	private Hospedagem hospedagem = new Hospedagem();
	private ItemMovimento itemMovimento = new ItemMovimento();

	private ScheduleModel ModeloEvento = new DefaultScheduleModel();

	private EventIPousada event = new EventIPousada();

	private List<Hospedagem> lista;

	private List<Cliente> listaClientes;
	private List<Venda> listaVendas;
	private List<Apartamento> listaApartamentos;
	private List<ItemMovimento> listaItens;
	private List<Transacao> listaTransacao;
	private List<FormasPagamento> listaFormasPagamento;

	private Configuracao config;

	private String clienteSelecionado;
	private int apartamentoSelecionado;

	private double valorRestante;

	private int idFpg;
	private int idTrn;
	
	// classe para relatórios
	private StreamedContent arquivoRetorno;
	private int tipoRelatorio;
	private Cliente cliente;

	private Lancamento lancamento = new Lancamento();
	
	private boolean contemErro;

	@Inject
	private HospedagemService hospedagemService;

	@Inject
	TransacaoService transacaoService;

	@Inject
	private ClienteService clienteService;

	@Inject
	private ApartamentoService apartamentoService;

	@Inject
	private CalendarioService calendarioService;

	@Inject
	private VendaService vendaService;

	@Inject
	private ItemVendaService itemVendaService;

	@Inject
	private FormasPagamentoService formas_PagamentoService;

	@Inject
	private LancamentoService lancamentoService;

	@Inject
	private CaixaService caixaService;

	@Inject
	private ConfiguracaoService configService;
	
	@Inject
	private ClienteBean clienteBean;

	public String novo() {
		hospedagem = new Hospedagem(StatusHospedagemEnum.ABERTO);
		lancamento = new Lancamento();
		apartamentoSelecionado = 0;
		IniciaLancamento();
		return PAG_CADASTRO;
	}

	public void novoLancamento() {
		lancamento = new Lancamento();
		IniciaLancamento();
	}

	public String salvarLancamento() {
		String retorno = PAG_DETALHES;
		try {
			hospedagem = hospedagemService.findById(hospedagem);

			if (this.lancamento.getValor() > 0) {
				lancamento.setTransacao(transacaoService.findById(idTrn));
				lancamento.setFormaPagamento(formas_PagamentoService
						.findById(idFpg));
				lancamento.setHospedagem(hospedagem);
				if (lancamento.isConfirmado()) {
					lancamento.setDataConfirmado(new Date());
					lancamento.setCaixa(caixaService.caixaAtual());
				} else
					lancamento.setCaixa(null);
				hospedagem.setLancamento(lancamento);
				// lancamentoService.salvar(lancamento);
				hospedagemService.update(hospedagem);
			} else {
				retorno = null;
				MessagesController.addError("Erro",
						"Lançamento com valor menor ou igual a ZERO!");
			}
		} catch (CaixaFechadoException e) {
			retorno = null;
			MessagesController
					.addError("Erro",
							"Não foi possível realizar operação pois não existe caixa aberto!");
			this.lancamento = null;
		}
		// Inicializar();
		// this.novo();
		// this.lista = null;

		return retorno;
	}

	public String salvar() {
		String retorno = null;
		if(validaHospedagem()){
			try {
				hospedagem.setApartamento(RetonaApartamento());
				hospedagem.setDataReserva(new Date());
				if (lancamento!= null && this.lancamento.getValor() > 0) {
	
					if (lancamento.isConfirmado())
						lancamento.setCaixa(caixaService.caixaAtual());
					else
						lancamento.setCaixa(null);
	
					lancamento.setHospedagem(hospedagem);
					hospedagem.setLancamento(lancamento);
					//lancamentoService.salvar(lancamento);
				}
				
				hospedagemService.salvar(hospedagem);
	
				Inicializar();
				this.novo();
				this.lista = null;
	
			} catch (InvalidDataAccessApiUsageException msg) {
				MessagesController.addError("Hospede não Informado",
						"É necessário um hospede para abrir uma hospedagem!");
				msg.printStackTrace();
			} catch (CaixaFechadoException e) {
				MessagesController
						.addError("Erro",
								"Não foi possível realizar operação pois não existe caixa aberto!");
			}
			retorno = PAG_LISTA;
		}
		return retorno;
	}

	private boolean validaHospedagem() {
		boolean retorno = false;
		if(hospedagem.validadeDatas()){
			retorno = true;
		}else{
			MessagesController.addError("Datas Inválidas", "Verifique as datas da hospedagem!");
		}
		return retorno;
	}

	public String cancelar() {
		Inicializar();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public void remover() {
		hospedagemService.remover(hospedagem);
		
	}

	public String cancelarCheckout() {
		this.hospedagem.setDesconto(0);
		this.hospedagem.setAcrescimo(0);
		atualizarValorTotal();
		return PAG_DETALHES;
	}

	public String update() {
		hospedagemService.update(hospedagem);
		this.lista = null;
		return PAG_LISTA;
	}

	public Apartamento RetonaApartamento() {
		int indice = -1;
		for (int i = 0; i < listaApartamentos.size(); i++) {
			if (listaApartamentos.get(i).getNumero_apartamento() == Long
					.valueOf(getApartamentoSelecionado())) {
				indice = i;
				break;
			} else {
				continue;
			}
		}
		if (indice == -1)
			return null;
		else
			return listaApartamentos.get(indice);
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(EventIPousada event) {
		this.event = event;
	}

	public List<Hospedagem> getListaHospedagensAbertas() {
		return /* listaHospedagensAbertas = */getCalendarioService()
				.HospedagensAbertas();
	}

	public List<Hospedagem> getListaPreReservasAbertas() {
		return /* listaPreReservasAbertas = */getCalendarioService()
				.PreReservasAbertas();
	}

	public List<Hospedagem> getListaReservasAbertas() {
		return getCalendarioService().ReservasAbertas();
	}

	public List<ItemMovimento> getListaItens() {
		this.listaVendas = vendaService.listaVendasHospedagem(hospedagem);
		// Lista que vai receber todos os itens das vendas da hospedagem.
		List<ItemMovimento> listaTmp = new ArrayList<>();
		for (int i = 0; i < this.listaVendas.size(); i++) {
			listaTmp.addAll(itemVendaService
					.listaItemHospedagem(this.listaVendas.get(i)));
		}
		setListaItens(listaTmp);
		return listaItens;
	}

	public float getTotalItensHospedagem() {

		return hospedagem.getTotalItensHospedagem();
	}
	
	public String removerProduto() {
		itemVendaService.remover(itemMovimento);
		return PAG_DETALHES;
	}

	/** LANçAMENTOS **/
	public void IniciaLancamento() {
		if (this.lancamento != null)
			this.lancamento.setValor(0);
	}

	public double getValorAPagar() {
		return getValorTotalHospedagem() - hospedagem.getTotalLancamentosConfirmados();
	}

	public String removerLancamento() {
		List<Lancamento> listTemp = new ArrayList<>();
		listTemp.addAll(hospedagem.getListaLancamentos());
		listTemp.remove(lancamento);
		hospedagem.setListaLancamentos(listTemp);
		hospedagemService.salvar(hospedagem);
		lancamentoService.remover(lancamento);
		return PAG_DETALHES;
	}

	public String alterarLancamento() {
		String retorno = PAG_DETALHES;
		try {
			Caixa caixa = caixaService.caixaAtual();

			if (lancamento.isConfirmado()) {
				lancamento.setConfirmado(false);
				lancamento.setCaixa(null);
			} else {
				lancamento.setCaixa(caixa);
				lancamento.setConfirmado(true);
			}
			lancamentoService.update(lancamento);
		} catch (CaixaFechadoException e) {
			retorno = null;
			MessagesController
					.addError("Erro",
							"Não foi possível realizar operação pois não existe caixa aberto!");
		}
		return retorno;
	}

	public void onCellEdit(CellEditEvent event) {
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();

		if (newValue != null && !newValue.equals(oldValue)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Valor Alterado", "Anterior: " + oldValue + ", Novo:"
							+ newValue);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		double valor = (double) newValue;

		lancamento.setValor(valor);
		lancamentoService.update(lancamento);
	}

	@PostConstruct
	private void Inicializar() {
		List<Hospedagem> listaMapa;
		try {
			ModeloEvento.clear();
			listaMapa = new ArrayList<Hospedagem>();
			listaMapa.clear();
			listaMapa.addAll(getListaPreReservasAbertas());
			listaMapa.addAll(getListaReservasAbertas());
			listaMapa.addAll(getListaHospedagensAbertas());

			for (Hospedagem hosp : listaMapa) {

				EventIPousada evento = new EventIPousada();
				evento.setAllDay(false);
				evento.setHospedagem(hosp);
				evento.setEndDate(hosp.getDataPrevCheckout());
				evento.setStartDate(hosp.getDataPrevCheckin());
				evento.setTitle("Apartamento: "
						+ hosp.getApartamentoReservado().toUpperCase() + "\n"
						+ "Checkin: " + hosp.getDataPrevCheckin() + "\n"
						+ "Checkout: " + hosp.getDataPrevCheckout());
				evento.setData(hosp.getId());
				evento.setEditable(false);
				if (hosp.getTipo().equals("P"))
					evento.setStyleClass("prereserva");
				else if (hosp.getTipo().equals("R"))
					evento.setStyleClass("reserva");
				else if (hosp.getTipo().equals("H"))
					evento.setStyleClass("hospedagem");
				// Adiciona no calendário
				ModeloEvento.addEvent(evento);
			}
		} finally {
			listaMapa = null;
		}
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (EventIPousada) selectEvent.getObject();
	}

	public void addEvent(ActionEvent actionEvent) {
		if (event.getId() == null)
			ModeloEvento.addEvent(event);
		else
			ModeloEvento.updateEvent(event);

		event = (EventIPousada) new DefaultScheduleEvent();
	}

	public ScheduleModel getEventModel() {
		return ModeloEvento;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.ModeloEvento = eventModel;
	}

	// Loockup para clientes
	public List<String> complete(String query) {
		List<String> results = new ArrayList<String>();
		listaClientes = clienteService.lista();
		for (int i = 0; i < listaClientes.size(); i++) {
			if (listaClientes.get(i).getNome().contains(query))
				results.add(listaClientes.get(i).getNome());
		}
		return results;
	}

	// Lista de Apartamentos Cadastrados
	public List<SelectItem> getListaApartamentos() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		listaApartamentos = apartamentoService.lista();

		if (hospedagem.getApartamento().getNumero_apartamento() != 0)
			lst.add(new SelectItem(hospedagem.getApartamento()
					.getNumero_apartamento(), String.valueOf(hospedagem
					.getApartamento().getNumero_apartamento()
					+ " - "
					+ hospedagem.getApartamento().getTipo().getDescricao())));
		else
			lst.add(new SelectItem("", "Selecione"));

		for (int i = 0; i < listaApartamentos.size(); i++) {
			lst.add(new SelectItem(listaApartamentos.get(i)
					.getNumero_apartamento(), String.valueOf(listaApartamentos
					.get(i).getNumero_apartamento()
					+ " - "
					+ listaApartamentos.get(i).getTipo().getDescricao())));
			if ((hospedagem.getApartamento().getNumero_apartamento() != 0)
					&& (listaApartamentos.get(i).getNumero_apartamento() == hospedagem
							.getApartamento().getNumero_apartamento())) {
				lst.remove(i + 1);
			}
		}

		return lst;
	}

	public List<SelectItem> getFormasPagamento() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		listaFormasPagamento = formas_PagamentoService.lista();
		if (!listaFormasPagamento.isEmpty()) {
			for (int i = 0; i < listaFormasPagamento.size(); i++) {
				lst.add(new SelectItem(listaFormasPagamento.get(i).getId(),
						listaFormasPagamento.get(i).getDescricao()));
			}
		}
		return lst;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public SelectItem[] getTransacoes() {
		listaTransacao = transacaoService.transacoesDeReceita();
		SelectItem[] items = new SelectItem[listaTransacao.size()];
		int i = 0;
		for (Transacao t : listaTransacao) {
			items[i++] = new SelectItem(t.getId(), t.getDescricao());
		}
		return items;
	}

	public String continuar() {
		return PAG_CADASTRO;
	}

	public FormasPagamento getFpgSelecionado() {

		FormasPagamento retorno = null;

		for (FormasPagamento f : listaFormasPagamento) {
			if (f.getId() == idFpg) {
				retorno = f;
				break;
			} else {
				continue;
			}
		}

		return retorno;

	}

	public Transacao getTrnSelecionado() {
		Transacao retorno = null;

		for (Transacao t : listaTransacao) {
			if (t.getId() == idTrn) {
				retorno = t;
				break;
			} else {
				continue;
			}
		}

		return retorno;
	}

	public String realizarCheckout() {

		try {
			hospedagem = hospedagemService.findById(hospedagem);
			double valorRestante = getValorAPagar();
			if (valorRestante > 0) {
				Lancamento lancamento = new Lancamento();
				lancamento.setCaixa(caixaService.caixaAtual());
				lancamento.setConfirmado(true);
				lancamento.setData(new Date());
				lancamento.setFormaPagamento(getFpgSelecionado());
				lancamento.setTransacao(getTrnSelecionado());
				lancamento.setValor(valorRestante);
				lancamento.setHospedagem(hospedagem);
				hospedagem.setLancamento(lancamento);
			}
			//hospedagem.setValorTotal(hospedagem.getValorTotal()	+ getTotalLancamentosConfirmados());
			hospedagem.setStatus(StatusHospedagemEnum.FECHADO.getFlag());
			hospedagemService.salvar(hospedagem);
			
			lista = null;

		} catch (CaixaFechadoException e) {
			MessagesController.addError("Caixa Fechado", "Não existe um caixa Aberto!");
		}

		return PAG_LISTA;
	}

	public double getValorTotalHospedagem() {
		double retorno = 0;
		
		double diarias = DiariasFacade.getTotalDiariasHospedagem(getConfig(), hospedagem); 
		hospedagem.setQtdDiarias(diarias);
		
		retorno =  hospedagem.calculaTotalHospedagem(diarias);

		return retorno;
	}

	public boolean isHospedagemAberta() {
		return hospedagem.isAberta();
	}
	
	public double totalDiarias(){
		hospedagem.setQtdDiarias(DiariasFacade.getTotalDiariasHospedagem(getConfig(), hospedagem) );
		return hospedagem.getQtdDiarias();
	}

	private Configuracao getConfig() {
		config =  configService.lista().get(0);
		return config;
		
	}

	public void atualizarValorTotal() {
		double retorno = 0;

		if (hospedagem.getDesconto() > getValorAPagar()) {
			MessagesController
					.addError("Opps!",
							"O desconto não pode ser maior que o valor total da hospedagem!");
		} else if (hospedagem.getDesconto() < 0) {
			MessagesController.addError("Opps!",
					"O desconto não pode ser negativo!");
		} else if (hospedagem.getAcrescimo() < 0) {
			MessagesController.addError("Opps!",
					"O Acréscimo não pode ser negativo!");
		} else {
			retorno = getValorAPagar() - hospedagem.getDesconto()
					+ hospedagem.getAcrescimo();
			setValorRestante(retorno); 
			//hospedagem.setValorTotal(retorno); kelvin disse que não deve ser setado
		}

	}

	public int getIdFpg() {
		return idFpg;
	}

	public void setIdFpg(int idFpg) {
		this.idFpg = idFpg;
	}

	public int getIdTrn() {
		return idTrn;
	}

	public void setIdTrn(int idTrn) {
		this.idTrn = idTrn;
	}

	public void setListaItens(List<ItemMovimento> listaItens) {
		this.listaItens = listaItens;
	}

	public ItemMovimento getItemMovimento() {
		return itemMovimento;
	}

	public void setItemMovimento(ItemMovimento itemMovimento) {
		this.itemMovimento = itemMovimento;
	}

	public List<Venda> getListaVendas() {
		return listaVendas = vendaService.listaVendasHospedagem(hospedagem);
	}

	public void setListaVendas(List<Venda> listaVendas) {
		this.listaVendas = listaVendas;
	}

	public Hospedagem getHospedagem() {
		return hospedagem;
	}

	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}

	public List<Hospedagem> getLista() {
		if (this.lista == null) {
			lista = hospedagemService.lista();
		}
		return lista;
	}

	public void setLista(List<Hospedagem> lista) {
		this.lista = lista;
	}

	public String getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(String clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}

	public int getApartamentoSelecionado() {
		return apartamentoSelecionado;
	}

	public void setApartamentoSelecionado(int apartamentoSelecionado) {
		this.apartamentoSelecionado = apartamentoSelecionado;
	}

	@Inject
	public CalendarioService getCalendarioService() {
		if (calendarioService == null) {
			calendarioService = new CalendarioService();
		}
		return calendarioService;
	}
	
	//Navegação
	public void editar() {
		lancamento = null;
		//return PAG_EDITAR;
	}

	public StatusHospedagemEnum[] getStatusHospedagem() {
		return StatusHospedagemEnum.values();
	}

	public int getTotalDeHospedes() {
		return hospedagemService.getTotalHospedes();
	}

	public String listaHospedagem() {
		this.lista = null;
		return PAG_LISTA;
	}

	public void setValorRestante(double valorAPagar) {
		this.valorRestante = valorAPagar;
	}

	public double getValorRestante() {
		return valorRestante;
	}
	
	//Relatório
	public StreamedContent ArquivoRetorno() {
		FacesContext context = FacesContext.getCurrentInstance();
		RelatorioUtil relatorioUtil = new RelatorioUtil();

		HashMap<String, Long> paramentroRelatorio = new HashMap<String, Long>();
		paramentroRelatorio.put("IdHospedagem", hospedagem.getId());

		try {
			arquivoRetorno = relatorioUtil.geraRelatorio(
					paramentroRelatorio,
					"FichaDeHospedes",
					"FichaHospedagem - Nº "
							+ String.valueOf(hospedagem.getId()), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivoRetorno;
	}

	public StreamedContent getArquivoRetorno() {
		return arquivoRetorno;
	}

	public void setArquivoRetorno(StreamedContent arquivoRetorno) {
		this.arquivoRetorno = arquivoRetorno;
	}

	public int getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	public void insereCliente(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		if(!clienteBean.verificaCPF()){
			clienteService.salvar(cliente);
			if (clienteBean != null) {
				clienteBean.novo();
			}
			this.hospedagem.setCliente(cliente);
			contemErro = false;
			context.execute("PF('cadCliente').hide();");
		}else{
			contemErro = true;
			context.execute("PF('cadCliente').jq.effect('shake', {times:5}, 100);");			
			
		}
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ClienteBean getClienteBean() {
		return clienteBean;
	}

	public void setClienteBean(ClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}

	public boolean isContemErro() {
		return contemErro;
	}

	public void setContemErro(boolean contemErro) {
		this.contemErro = contemErro;
	}

	public String novoCliente() {
		clienteBean.novoClienteHospedagem();
		return PAG_CADASTRO;
	}
	

}
