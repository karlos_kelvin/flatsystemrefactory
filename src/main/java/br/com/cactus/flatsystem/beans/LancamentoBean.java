package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.service.TransacaoService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;


@ViewScoped
public class LancamentoBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2331826931029224094L;

	private static final String PAG_LISTA = "/pages/user/listaLancamento?faces-redirect=true";

	private static final String PAG_CADASTRO = "/pages/user/cadastroLancamento?faces-redirect=true";

	private Lancamento lancamento = new Lancamento();
	private List<Lancamento> listaLancamentos;
	private Transacao transacao;
	private FormasPagamento formaPagamento;
	private Date data;

	
	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private TransacaoService transacaoService;
	
	@Inject
	private CaixaService caixaService;
	
	
	public String novo(){
		lancamento = new Lancamento();	
		this.listaLancamentos = null;
		data = new Date();
		return "/pages/user/cadastroLancamento" + "?faces-redirect=true";
	}
	
	public String salvar(){
		String retorno = null;
		try{
			
			if(testaValores()){
				lancamento.setFormaPagamento(formaPagamento);
				lancamento.setTransacao(transacao);
				if (lancamento.getData() == null) {
					lancamento.setData(new Date());
				}
				Caixa c = caixaService.caixaAtual();
				if(lancamento.isConfirmado()){
					lancamento.setCaixa(c);
				}
				lancamentoService.salvar(lancamento);
				this.novo();
				retorno = PAG_LISTA;
			}
		} catch (CaixaFechadoException e) {
			MessagesController.addError("Erro", "Não foi possível realizar operaçãoo pois não existe caixa aberto!");
		}
		
		return retorno;
	}
	
	public boolean testaValores(){
		boolean retorno = ((this.formaPagamento != null) && 
						   (this.transacao != null) && 
						   (lancamento.getValor() > 0));
		
		if(this.formaPagamento == null) {
			MessagesController.addError("Erro", "É necessário informar a forma de pagamento.");
		}
		if(this.transacao == null){
			MessagesController.addError("Erro", "É necessário informar a transação.");
		}
		if(lancamento.getValor() <= 0){
			MessagesController.addError("Erro", "O valor deve ser maior que zero!");
		}		

		return retorno;
	}
	
	public String cancelar(){
		this.novo();
		this.listaLancamentos = null;
		return PAG_LISTA;
	}
	
	public String editarLancamento(){
		return "/pages/user/editarLancamento?faces-redirect=true";
	}
	
	public String continuar(){
		return PAG_CADASTRO;
	}
	
	public String update(){
		lancamentoService.update(lancamento);
		this.listaLancamentos = null;
		return PAG_LISTA;
	}
	
	public String remover(){
		lancamentoService.remover(lancamento);
		this.novo();
		this.listaLancamentos = null;
		return PAG_LISTA;
	}
	
	public List<Lancamento> lista(){
		if (this.listaLancamentos == null){
			this.listaLancamentos = lancamentoService.lista();
		}
		return listaLancamentos;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public List<Lancamento> getListaLancamentos() {
		if (this.listaLancamentos == null){
			this.listaLancamentos = lancamentoService.lista();
		}
		return listaLancamentos;
	}

	public void setListaLancamentos(List<Lancamento> listaLancamentos) {
		this.listaLancamentos = listaLancamentos;
	}
	
	
	public String listaLancamentos(){
		this.listaLancamentos = null;
		return PAG_LISTA;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}

	public FormasPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormasPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public String getDataAtual(){
		String retorno = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
		retorno = sdf.format(new Date());
		return retorno;
	}
	
	public String alterarLancamento(){
		Caixa c = null;
		boolean caixaAtual = false;
		
		try {
			c = caixaService.caixaAtual();
			
			if ( lancamento.getCaixa() != null) { //lancamento possui um caixa
				if(lancamento.getCaixa().isClosed()){
					MessagesController.addError("Erro", "Impossível alterar lançamentos de caixas fechados.");
				}else {//se tem um caixa e ele não está fechado então o lancamento já estava confirmado
					lancamento.setConfirmado(false);
					lancamento.setCaixa(null);
				}
			}else{
				lancamento.setConfirmado(true);
				lancamento.setCaixa(c);
				
			}
			lancamentoService.update(lancamento);
		} catch (CaixaFechadoException e) {
			MessagesController.addError("Erro", "Não foi possível realizar operação pois não existe caixa aberto!");
			e.printStackTrace();
		}
		
		return PAG_LISTA;
	}
	
	public String removerLancamento(){
		//não pode remover lancamentos de caixas fechados
		if(lancamento.isConfirmado()){
			if(lancamento.getCaixa().isClosed()){
				MessagesController.addError("Erro", "Impossível alterar lançamentos de caixas fechados.");
			}else{
				lancamentoService.remover(lancamento);
			}
		}else {
			lancamentoService.remover(lancamento);
		}
		this.listaLancamentos = null;
		return PAG_LISTA;
	}
}
