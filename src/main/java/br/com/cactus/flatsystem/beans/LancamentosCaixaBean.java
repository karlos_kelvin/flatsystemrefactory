package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.LancamentoService;
import javax.inject.Inject;


@ViewScoped
public class LancamentosCaixaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2331826931029224094L;
	
	private final String PAG = "/pages/user/lancamentosCaixa?faces-redirect=true";

	private List<Lancamento> listaLancamentos = new ArrayList<Lancamento>();
	private Caixa caixa = new Caixa();
	
	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private CaixaService caixaService;
	
	
	public String novo(){
		return PAG;
	}
	
	public List<Lancamento> getListaLancamentos() {
		if(this.listaLancamentos == null || this.listaLancamentos.size() == 0){
			this.listaLancamentos = lancamentoService.getLancamentosCaixa(caixa);
		}
		return this.listaLancamentos;
	}

	public void setListaLancamentos(List<Lancamento> listaLancamentos) {
		this.listaLancamentos = listaLancamentos;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
		this.listaLancamentos = lancamentoService.getLancamentosCaixa(this.caixa);
	}
	
	
	
	
}
