package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.Compra;
import br.com.cactus.flatsystem.service.CompraService;
import br.com.cactus.flatsystem.service.ProdutoService;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;


@ViewScoped
public class ListaCompraBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6594335797614195740L;
	
	private final String PAG 		  = "/pages/user/listaCompra?faces-redirect=true";
	private final String PAG_CADASTRO = "/pages/user/entradaProduto?faces-redirect=true";
	
	private List<Compra> listaCompra;
	private Compra compra;
	
	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private CompraService compraService;
	
	public List<Compra> getListaCompra() {
		if (this.listaCompra == null || this.listaCompra.size() == 0) {
			this.listaCompra = this.compraService.lista();
		}
		return listaCompra;
	}

	public void setListaCompra(List<Compra> listaCompra) {
		this.listaCompra = listaCompra;
	}
	
	public String remover(){
		compraService.remover(this.compra);
		compra = null;
		this.listaCompra = null;
		MessagesController.addInfo("Info:",	 "Compra Excluída Com Sucesso!");
		return PAG;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	
	public String editar(){
		return PAG_CADASTRO;
	}
	
}
