package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;

import org.primefaces.model.StreamedContent;

import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.service.HospedagemService;
import br.com.cactus.flatsystem.util.StatusHospedagemEnum;
import br.com.cactus.flatsystem.util.TipoHospedagemEnum;
import br.com.cactus.flatsystem.view.MessagesController;
import javax.inject.Inject;


@ViewScoped
public class ListaHospedagemBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Páginas
	private static final String PAG_CADASTRO = "/pages/user/cadastroHospedagem?faces-redirect=true";
	private static final String PAG_DETALHES = "/pages/user/detalhesHospedagem?faces-redirect=true";
	private static final String PAG_LISTA 	 = "/pages/user/listaHospedagem?faces-redirect=true";
	private static final String PAG_EDITAR   = "/pages/user/editarHospedagem?faces-redirect=true";
	
	//Atributos
	private Hospedagem hospedagem;
	private List<Hospedagem> lista;
	private List<Hospedagem> listaFiltrada;
	private String tipoSelecionado;
	private String statusSelecionado;
	

	@Inject
	private HospedagemBean hospedagemBean;
	
	//Services
	@Inject
	private HospedagemService hospedagemService;
	

	//Getters and Setters
	public List<Hospedagem> getLista() {
		return lista;
	}

	public void setLista(List<Hospedagem> lista) {
		this.lista = lista;
	}

	public List<Hospedagem> getListaFiltrada() {
		return listaFiltrada;
	}

	public void setListaFiltrada(List<Hospedagem> listaFiltrada) {
		this.listaFiltrada = listaFiltrada;
	}

	public HospedagemService getHospedagemService() {
		return hospedagemService;
	}

	public void setHospedagemService(HospedagemService hospedagemService) {
		this.hospedagemService = hospedagemService;
	}
	
	public TipoHospedagemEnum[] getTiposHospedagem(){
		return TipoHospedagemEnum.values();
	}
	
	public StatusHospedagemEnum[] getStatusHospedagem(){
		return StatusHospedagemEnum.values();
	}

	public String getTipoSelecionado() {
		return tipoSelecionado;
	}

	public void setTipoSelecionado(String tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}

	public String getStatusSelecionado() {
		return statusSelecionado;
	}

	public void setStatusSelecionado(String statusSelecionado) {
		this.statusSelecionado = statusSelecionado;
	}
	
	public Hospedagem getHospedagem() {
		return hospedagem;
	}

	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}
	
	public HospedagemBean getHospedagemBean() {
		return hospedagemBean;
	}

	public void setHospedagemBean(HospedagemBean hospedagemBean) {
		this.hospedagemBean = hospedagemBean;
	}
	
	
	//Navegação
	public String editar() {
		if (hospedagemBean.getHospedagem() != null){
			hospedagemBean.editar();
			return PAG_EDITAR;
		}else{
			MessagesController.addInfo("Atenção: ", "Selecione uma hospedagem antes de executar esta ação!");
			return null;
		}
	}
	
	public String visualizar() {
		if (hospedagemBean.getHospedagem() != null){
			return PAG_DETALHES;
		}else{
			MessagesController.addInfo("Atenção: ", "Selecione uma hospedagem antes de executar esta ação!");
			return null;
		}
		
	}
	
	public String remover(){
		if (hospedagemBean.getHospedagem() != null){
			hospedagemBean.remover();
			limpaConsulta();
			return PAG_LISTA;
		}else{
			MessagesController.addInfo("Atenção: ", "Selecione uma hospedagem antes de executar esta ação!");
			return null;
		}
	}

	public String abrir(){
		limpaConsulta();
		return PAG_LISTA;
	}
	
	
	public StreamedContent fichaHospede(){
		if (hospedagemBean.getHospedagem() != null){
			return hospedagemBean.ArquivoRetorno();
		}else {
			MessagesController.addInfo("Atenção: ", "Selecione uma hospedagem antes de executar essa ação!");
			return null;
		}
		
	}
	
	
	//consulta
	public void limpaConsulta() {
		tipoSelecionado = null;
		statusSelecionado = null;
		
		limpaListas();
	}
	
	private void limpaListas() {
		if(this.lista != null){
			this.lista.clear();
		}
		if(this.listaFiltrada != null){
			this.listaFiltrada.clear();
		}
		
	}

	public String realizaConsulta() {
		
		Hospedagem h = new Hospedagem();
		
		if(statusSelecionado != null && !statusSelecionado.equals("")) h.setStatus(statusSelecionado);
		if(  tipoSelecionado != null && !tipoSelecionado.equals("")  ) h.setTipo(tipoSelecionado);
		
		this.lista = hospedagemService.filtro(h);
		this.listaFiltrada = lista;
		
		return null;
	}

	
}
