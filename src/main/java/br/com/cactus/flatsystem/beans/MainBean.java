package br.com.cactus.flatsystem.beans;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.HospedagemService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.view.MessagesController;



/**
 * Classe para controlar a tela principal do sistema
 * @author Mario
 *
 */
@ViewScoped
public class MainBean {
	
	private static final String PAG = "/pages/user/successfulPage.xhtml?faces-redirect=true";
	
	private Caixa caixaAtual;
	private CartesianChartModel hospedagensChart = new CartesianChartModel();
	private CartesianChartModel caixaChart = new CartesianChartModel();
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private CaixaService caixaService;
	
	@Inject
	private HospedagemService hospedagemService;
	
	private void preencheGraficoCaixa() {
		try{
			caixaAtual = caixaService.caixaAtual();
			
			ChartSeries receitas = new ChartSeries("Receitas");
			receitas.set(sdf.format(new Date()), lancamentoService.getLancamentosDeReceita(caixaAtual));
			
			ChartSeries despesas = new ChartSeries("Despesas");
			despesas.set(sdf.format(new Date()), lancamentoService.getLancamentosDeDespesa(caixaAtual));
			
			caixaChart.addSeries(receitas);
			caixaChart.addSeries(despesas);
			
		} catch (CaixaFechadoException e) {
			MessagesController.addInfo("Info", "Não existe nenhum caixa aberto!");
			caixaChart.clear();
		}
	}
	
	private void preencheGraficoHospedagem() {
		hospedagensChart.clear();
		
		ChartSeries preReservas = new ChartSeries("Pré-Reservas");		
		preReservas.set(sdf.format(new Date()), hospedagemService.totalPreReservas());
		
		ChartSeries reservas = new ChartSeries("Reservas");
		reservas.set(sdf.format(new Date()), hospedagemService.totalReservas());
		
		ChartSeries hospedagem = new ChartSeries("Hospedagens");
		hospedagem.set(sdf.format(new Date()), hospedagemService.totalHospedagensAbertas());
		
		hospedagensChart.addSeries(preReservas);
		hospedagensChart.addSeries(reservas);
		hospedagensChart.addSeries(hospedagem);
	}
	
	public CartesianChartModel getCaixaChart() {
		caixaChart.clear();
		preencheGraficoCaixa();
		return caixaChart;
	}

	public void setCaixaChart(CartesianChartModel caixaChart) {
		this.caixaChart = caixaChart;
	}

	public CartesianChartModel getHospedagensChart() {
		preencheGraficoHospedagem();
		return hospedagensChart;
	}

	public void setHospedagensChart(CartesianChartModel hospedagensChart) {
		this.hospedagensChart = hospedagensChart;
	}
	
	public String paginaPrincipal(){
		caixaChart.clear();
		preencheGraficoHospedagem();
		return PAG;
	}
}
