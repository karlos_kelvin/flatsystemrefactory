package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import br.com.cactus.flatsystem.model.exception.DiaDaSemanaException;
import br.com.cactus.flatsystem.service.ApartamentoService;
import br.com.cactus.flatsystem.service.MapaService;
import br.com.cactus.flatsystem.util.DiaDaSemana;
import br.com.cactus.flatsystem.util.MapaSemanal;
import br.com.cactus.flatsystem.util.MontaColunasSemanais;
import javax.inject.Inject;


@Named
public class MapaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5545088755609196449L;
	
	private static final String PAG_MAPA = "/pages/user/mapaSemanal.xhtml?faces-redirect=true";
	
	private GregorianCalendar calendar;
	private DiaDaSemana[] semana = new DiaDaSemana[7];
	
	private Date dataInicial, dataFinal;

	private MapaService ms;

	private ApartamentoService aps;
	
	@Inject
	public MapaBean(ApartamentoService apartamentoService, MapaService ms) {
		calendar = new GregorianCalendar();

		aps = apartamentoService;
		this.ms = ms;
		
		atulizaSemana();
		
	}

	public Date getDataInicial() {
		return dataInicial;
	}



	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}



	public Date getDataFinal() {
		return dataFinal;
	}



	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}



	public String getDiaSemana(int i){
		String retorno = null;
		try {
			retorno = semana[i].toString();
		} catch (Exception e) {
			// TODO: handle exception
		} 
		return retorno;
	}
	
	public List<MapaSemanal> getMapaSemanal(){
		return ms.getMapaSemanal();
	}
	
	@PostConstruct
	private void atulizaSemana() {
		try {
			semana = MontaColunasSemanais.getDiasDaSemana(calendar);
			ms.setApartamentos(aps.lista());
			ms.setDias(semana);
			ms.montaDias();
		} catch (DiaDaSemanaException e) {
			e.printStackTrace();
		}
	}
	
	public String dataAtual(){
		calendar = new GregorianCalendar();
		atulizaSemana();
		return PAG_MAPA;
	}
	
	public String atualizar(){
		atulizaSemana();
		return PAG_MAPA;
	}

	public String previousWeek() {
		calendar.add(calendar.DAY_OF_MONTH, -7);
		atulizaSemana();
		return PAG_MAPA; 
	}

	public GregorianCalendar getCalendar() {
		return calendar;
	}

	public void setCalendar(GregorianCalendar calendar) {
		this.calendar = calendar;
	}

	public DiaDaSemana[] getSemana() {
		return semana;
	}

	public void setSemana(DiaDaSemana[] semana) {
		this.semana = semana;
	}

	public String nextWeek() {
		calendar.add(calendar.DAY_OF_MONTH, 7);
		atulizaSemana();
		return PAG_MAPA;
	}
}
