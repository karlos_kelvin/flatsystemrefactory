package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.service.ProdutoService;
import br.com.cactus.flatsystem.service.SaldoEstoqueService;
import br.com.cactus.flatsystem.view.MessagesController;



@Named
public class ProdutoBean implements Serializable {


	private final String PAG_CADASTRO = "/pages/user/cadastroProduto"+ "?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaProduto"+ "?faces-redirect=true";

	private static final long serialVersionUID = 1L;

	private Produto produto = new Produto();
	private List<Produto> lista;

	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private SaldoEstoqueService saldoEstoqueService;

	public String novo() {
		this.produto = new Produto();

		return PAG_CADASTRO;
	}
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}

	public String salvar() {
		String retorno = null;
		if (verificaCampos()) {
			produtoService.salvar(this.produto);
			this.novo();
			this.lista = null;
			retorno = PAG_LISTA;
		}
		return retorno;
	}

	public boolean verificaCampos() {
		boolean retorno = true;
		if (this.produto.getDescricao().equals("")) {
			MessagesController.addError("Campos Obrigatórios:",
					"Preencha o campo descrição!");
			retorno = false;
		}
		if (!(this.produto.getPreco() > 0)) {
			MessagesController.addError("Campos Obrigatórios:",
					"Preencha o campo preço!");
			retorno = false;
		}
		return retorno;
	}
	
	public String editar() {
		return PAG_CADASTRO;
	}

	public String update() {
		produtoService.update(this.produto);
		this.lista = null;
		return PAG_CADASTRO;
	}

	public String remover() {
		produtoService.remover(this.produto);
		this.novo();
		this.lista = null;
		return null;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getLista() {
		if (this.lista == null) {
			this.lista = produtoService.lista();
		}
		return this.lista;
	}

	public void setLista(List<Produto> lista) {
		this.lista = lista;
	}
	
	public String listaProdutos(){
		this.lista = null;
		return PAG_LISTA;
	}
	
}
