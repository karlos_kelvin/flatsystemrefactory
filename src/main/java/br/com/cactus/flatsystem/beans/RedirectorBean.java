package br.com.cactus.flatsystem.beans;

import javax.inject.Named;


@Named
public class RedirectorBean {
	public String userPage() {
		return "userPage";
	}

	public String adminPage() {
		return "adminPage";
	}

	public String index() {
		return "index";
	}

	public String successfulPage() {
		return "successfulPages";
	}
}
