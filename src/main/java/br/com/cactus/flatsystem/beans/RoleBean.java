package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;


import br.com.cactus.flatsystem.entities.Role;
import br.com.cactus.flatsystem.service.RoleService;



@ViewScoped
public class RoleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Role role = new Role();
	private List<Role> lista;
	
	@Inject
	private RoleService roleService;
	
	public String novo(){
		role = new Role();
		
		return "#";//"/pages/user/cadastroApartamentos";
	}
	
	public String salvar(){
		this.novo();
		this.lista = null;
		return "#";//"/pages/user/cadastroApartamentos";
	}
	
	public String cancelar(){
		this.lista = null;
		return "#";//"/pages/user/cadastroApartamentos";	
	}
	
	public String editar(){
		return "#";//"/pages/user/cadastroApartamentos";
	}
	
	public String update(){
		roleService.update(role);
		this.lista = null;
		return "#";//"/pages/user/cadastroApartamentos";
	}
	
	public String remover(){		
		roleService.remover(role);
		this.novo();
		this.lista = null;
		return "#";//"/pages/user/cadastroApartamentos";
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Role> getLista() {
		if (this.lista == null){
			lista = roleService.lista();
		}
		return lista;
	}

	public void setLista(List<Role> lista) {
		this.lista = lista;
	}
	
	public List<SelectItem> getPermissoes() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		for (int i = 0; i < getLista().size(); i++){
			lst.add(new SelectItem('S', "Sim"));
		}
		return lst;
	}
	
	
	
}
