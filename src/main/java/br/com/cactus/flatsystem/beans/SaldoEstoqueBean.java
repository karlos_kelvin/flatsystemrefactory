package br.com.cactus.flatsystem.beans;

import java.util.List;



import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.entities.SaldoEstoque;
import br.com.cactus.flatsystem.service.ProdutoService;
import br.com.cactus.flatsystem.service.SaldoEstoqueService;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@ViewScoped
public class SaldoEstoqueBean {

	private SaldoEstoque saldo_Estoque;
	private List<SaldoEstoque> saldoEstoque;
	
	private Produto produto = new Produto();
	private List<Produto> listaProduto;
	
	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private SaldoEstoqueService saldo_estoque_service;

	public String novo(){
		return "/pages/user/saldoEstoque";
	}

	public void setSaldoEstoque(List<SaldoEstoque> saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(List<Produto> listaProduto) {
		this.listaProduto = listaProduto;
	}

	
	

	
	
	
	
}
