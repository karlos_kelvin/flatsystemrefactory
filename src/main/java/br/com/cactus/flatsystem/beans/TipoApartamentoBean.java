package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.TipoApartamento;
import br.com.cactus.flatsystem.service.TipoApartamentoService;



@Named
public class TipoApartamentoBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8160861006239338486L;


	private final String PAG_CADASTRO = "/pages/user/cadastroTipoApartamento?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaTipoApartamento?faces-redirect=true";
	
	private TipoApartamento tipo_apartamento = new TipoApartamento();
	private List<TipoApartamento> lista;
	
	@Inject
	private TipoApartamentoService tipo_apartamento_service;
	
	public String novo(){
		tipo_apartamento = new TipoApartamento();
		
		return PAG_CADASTRO;
	}
	
	public String salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		tipo_apartamento_service.salvar(tipo_apartamento);
		FacesMessage msg = new FacesMessage("Tipo de Apartamento salvo com sucesso!");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
        context.addMessage(null, msg);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String editar(){
		return PAG_CADASTRO;
	}
	
	public String update(){
		tipo_apartamento_service.update(tipo_apartamento);
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String remover(){
		tipo_apartamento_service.remover(tipo_apartamento);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public TipoApartamento getTipo_apartamento() {
		return tipo_apartamento;
	}

	public void setTipo_apartamento(TipoApartamento tipo_apartamento) {
		this.tipo_apartamento = tipo_apartamento;
	}

	public List<TipoApartamento> getLista() {
		if (this.lista == null){
			lista = tipo_apartamento_service.lista();
		}
		return lista;
	}

	public void setLista(List<TipoApartamento> lista) {
		this.lista = lista;
	}
	
}
