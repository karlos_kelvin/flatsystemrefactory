package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;

import br.com.cactus.flatsystem.entities.TipoTransacao;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.service.TransacaoService;
import javax.inject.Inject;


@ViewScoped
public class TransacaoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String PAG_CADASTRO = "/pages/user/cadastroTransacao?faces-redirect=true";
	private static final String PAG_LISTA = "/pages/user/listaTransacoes?faces-redirect=true";

	private Transacao transacao = new Transacao();
	private List<Transacao> lista;
	private List<Transacao> transacoesCadastradas;
	
	@Inject
	private TransacaoService transacaoService;
	
	public String novo(){
		transacao = new Transacao();		
		return "/pages/user/cadastroTransacao";
	}
	
	public String salvar(){
		transacaoService.salvar(transacao);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String editar(){
		return PAG_CADASTRO;
	}
	
	public String update(){
		transacaoService.update(transacao);
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String remover(){		
		transacaoService.remover(transacao);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}

	public List<Transacao> getLista() {
		if (this.lista == null){
			lista = transacaoService.lista();
		}
		return lista;
	}

	public void setLista(List<Transacao> lista) {
		this.lista = lista;
	}
	
	public SelectItem[] getGenderValues() {
	    SelectItem[] items = new SelectItem[TipoTransacao.values().length];
	    int i = 0;
	    for(TipoTransacao t: TipoTransacao.values()) {
	      items[i++] = new SelectItem(t, t.getNome());
	    }
	    return items;
	}
	
	public boolean isDisable(long id){
		boolean retorno = true;
		if (id > 2) {
			retorno = false;
		}
		return retorno;
	}
	
	public List<Transacao> getTransacoesCadastradas(){
		this.transacoesCadastradas = transacaoService.listaSemSangriaSuprimento();
		return this.transacoesCadastradas;
	}

	public void setTransacoesCadastradas(List<Transacao> transacoesCadastradas) {
		this.transacoesCadastradas = transacoesCadastradas;
	}

	public String listaTransacoes(){
		this.lista = null;
		return PAG_LISTA;
	}
}
