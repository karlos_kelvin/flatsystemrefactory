package br.com.cactus.flatsystem.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Role;
import br.com.cactus.flatsystem.entities.User;
import br.com.cactus.flatsystem.service.RoleService;
import br.com.cactus.flatsystem.service.UserService;
import br.com.cactus.flatsystem.view.MessagesController;



@ViewScoped
public class UserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String PAG_CAD = "/pages/admin/cadastroUsuarios?faces-redirect=true";
	private static final String PAG_LISTA = "/pages/admin/listaUsuarios?faces-redirect=true";
	
	private User user = new User();

	private List<User> lista;
	private List<Role> listaPermissoes;
	private String roleSelecionada;
	private String confirmacaoSenha;
	
	@Inject
	private UserService usuarioService;
	
	@Inject
	private RoleService roleService;
	
	public String novo(){		
		user = new User();
		confirmacaoSenha = "";
		return PAG_CAD;
	}
	
	public String salvar() {
		
	if (!verificaSenha()){	
		return null;
	}else{
		FiltraPermissao();
		user.setRoles(listaPermissoes);
		usuarioService.salvar(user);			
		this.novo();
		this.lista = null;
	}
			
		return PAG_LISTA;
	}
	
	
	public String cancelar(){
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String editar(){
		return PAG_CAD;
	}
	
	public String update(){
		usuarioService.atualizar(user);
		this.lista = null;
		return PAG_LISTA;
	}
	
	public String remover(){		
		usuarioService.remover(user);
		this.novo();
		this.lista = null;
		return PAG_LISTA;
	}
	
	public boolean verificaSenha(){
		if (!user.getPassword().equals(this.confirmacaoSenha)){
			MessagesController.addWarn("Atenção", "Atenção, senhas não conferem");
			
			return false;
		}
		
		return true;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getLista() {
		if (this.lista == null){
			lista = usuarioService.lista();
		}
		return lista;
	}

	public void setLista(List<User> lista) {
		this.lista = lista;
	}
	
        /*
        *   Comentado pois está Utilizando Spring Security
        *
	public List<SelectItem> getPermissoes() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		listaPermissoes = roleService.lista();
		for (int i = 0; i < listaPermissoes.size(); i++){
			String descRole = "";
			if (listaPermissoes.get(i).getAuthority().equals("ROLE_ADMIN"))
				descRole = "Administrador";
			else if (listaPermissoes.get(i).getAuthority().equals("ROLE_USER"))	
				descRole = "Funcionário";
			lst.add(new SelectItem(listaPermissoes.get(i).getId(), descRole));
		}
		return lst;
	}
        */
	
	public void FiltraPermissao(){
		for (int i = 0; i < listaPermissoes.size(); i++){
			if ( listaPermissoes.get(i).getId() != Long.valueOf( getRoleSelecionada() ) ) {
				listaPermissoes.remove(i);
			}
		}		
	}

	public String getRoleSelecionada() {
		return roleSelecionada;
	}

	public void setRoleSelecionada(String roleSelecionada) {
		this.roleSelecionada = roleSelecionada;
	}

	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}

	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}
	
	public String listaUsuarios(){
		this.lista = null;
		return PAG_LISTA;
	}
	
}
