package br.com.cactus.flatsystem.beans;

import javax.faces.bean.SessionScoped;

import br.com.cactus.flatsystem.entities.User;


@SessionScoped
public class UserSession {
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isLoggedIn(){
		return user != null;
	}
}
