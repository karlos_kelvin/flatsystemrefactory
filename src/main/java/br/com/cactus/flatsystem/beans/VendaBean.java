package br.com.cactus.flatsystem.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Cliente;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.entities.Venda;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.service.CaixaService;
import br.com.cactus.flatsystem.service.CalendarioService;
import br.com.cactus.flatsystem.service.ClienteService;
import br.com.cactus.flatsystem.service.FormasPagamentoService;
import br.com.cactus.flatsystem.service.LancamentoService;
import br.com.cactus.flatsystem.service.ProdutoService;
import br.com.cactus.flatsystem.service.VendaService;
import br.com.cactus.flatsystem.view.MessagesController;



@ViewScoped
public class VendaBean {
	
	private final String PAG_CADASTRO = "/pages/user/vender.xhtml?faces-redirect=true";
	private final String PAG_LISTA = "/pages/user/listaVendas.xhtml?faces-redirect=true";

	private String nomeProduto;
	private float quantidade;
	
	private Cliente cliente;
	private FormasPagamento formaPagamento;
	private Transacao transacao;
	
	private Venda venda = new Venda();
	private List<Venda> lista;
	private List<Cliente> listaCliente;
	@SuppressWarnings("unused")
	private List<Hospedagem> listaHospedagensAbertas;
	private List<FormasPagamento> listFormasPagamento;
	private List<Produto> listProduto;
	
	private Produto produto = new Produto();
	private ItemMovimento item = new ItemMovimento();
	
	@Inject
	private ClienteService clienteService;
	@Inject
	private ProdutoService produtoService;
	@Inject
	private VendaService vendaService;
	@Inject
	private FormasPagamentoService formasPagamentoService;
	@Inject
	private CalendarioService calendarioService;
	@Inject
	private LancamentoService lancamentoService;
	@Inject
	private CaixaService caixaService;
	
	private void inicializa(){
		this.venda = new Venda();		
		this.produto = new Produto();
		this.item = new ItemMovimento();
		this.transacao = null;
		this.cliente = null;
		this.formaPagamento = null;
		this.quantidade = 0;
		
	}
	
	public String novo() {
		inicializa();
		return PAG_CADASTRO;
	}
	
	public List<Hospedagem> getListaHospedagensAbertas() {
		if (listaHospedagensAbertas == null) {
			listaHospedagensAbertas = calendarioService.lista();
		}
		return listaHospedagensAbertas;
	}

	public void setListaHospedagensAbertas(List<Hospedagem> listaHospedagensAbertas) {
		this.listaHospedagensAbertas = listaHospedagensAbertas;
	}

	public String novoItemHospedagem() {
		this.venda = new Venda(true);
		this.produto = new Produto();
		this.item = new ItemMovimento();
		return "/pages/user/cadastroItemHospedagem";
	}

	public String editar() {
		cliente = venda.getCliente();
		formaPagamento = venda.getLancamento().getFormaPagamento();
		
		try{
			transacao = venda.getLancamento().getTransacao();
		}catch(NullPointerException e){
			//quando a venda é para hospedagem ela não tem um lancamento
		}
		
		return PAG_CADASTRO;
	}
	
	public String continuar(){
		return PAG_CADASTRO;
	}

	public String update() {
		vendaService.update(this.venda);
		this.lista = null;
		return PAG_CADASTRO;
	}
	
	public String salvarItemHospedagem() {
		String retorno = null;
	
		this.venda.setData(new Date());
		this.venda.setLancamento(null);
		vendaService.salvar(this.venda);
		this.novo();
		this.lista = null;
		MessagesController.addInfo("info:", "Dados salvos com sucesso!");
		retorno = PAG_LISTA; 
		
		return retorno;// voltar página
	}

	public String salvar() {
		String retorno = null;
		if(venda.getHospedagem() != null){// só testa vendas para clientes
			salvarItemHospedagem();
		}else if (verificaCampos()) {
			try {
				criaLancamento();
				this.venda.setData(new Date());
				vendaService.salvar(this.venda);
				this.novo();
				this.lista = null;
				MessagesController.addInfo("info:", "Dados salvos com sucesso!");
				retorno = PAG_LISTA; 
			} catch (CaixaFechadoException e) {
				MessagesController.addError("Erro", "Não foi possível realizar operação pois não existe caixa aberto!");
			}
		}
		return retorno;// voltar página
	}

	private void criaLancamento() throws CaixaFechadoException {
			Lancamento l = this.venda.getLancamento();
			l.setConfirmado(true);
			Caixa c = caixaService.caixaAtual();
			l.setCaixa(c);
			l.setData(new Date());
			l.setFormaPagamento(this.formaPagamento);//alterado para usar a forma de pagamento do lançamento
			l.setValor(this.venda.getTotal());			
	}

	public boolean verificaCampos() {
		boolean retorno = true;
		
		if(this.venda.getItensMovimento().size() <= 0){
			MessagesController.addError("Erro", "Lista de Itens Vazia!");
			retorno = false;
		}
		
		if (this.transacao ==null){
			MessagesController.addError("Erro", "Escolha uma transação!");
			retorno = false;
		}else{
			this.venda.getLancamento().setTransacao(this.transacao);
		}

		if(this.cliente == null) {
			MessagesController.addError("Erro", "Escolha um cliente!");
			retorno = false;
		}else{
			this.venda.setCliente(this.cliente);
		}
		
		if (this.formaPagamento == null) {
			MessagesController.addError("Erro", "Escolha uma forma de pagamento!");
			retorno = false;
		}else{
			//this.venda.setFormaPagamento(this.formaPagamento); ///desnecessário
			this.venda.getLancamento().setFormaPagamento(formaPagamento);
		}
		
		return retorno;
	}

	public String remover() {
		vendaService.remover(this.venda);
		this.novo();
		this.lista = null;
		return null;
	}

	public List<String> completeProduto(String query){
		List<String> retorno = new ArrayList<String>();
		listProduto = produtoService.lista();
		String produto = null;
		for (int i = 0; i < listProduto.size(); i++) {
			produto = listProduto.get(i).getId()+" - ";
			produto += listProduto.get(i).getDescricao();
			if (produto.contains(query)){
				retorno.add(produto);
			}
		}
		
		return retorno;
	}
	
	public List<String> completeCliente(String query){
		List<String> retorno = new ArrayList<String>();
		listaCliente = clienteService.lista();
		String cliente = null;
		for (int i = 0; i < listaCliente.size();i++) {
			cliente = listaCliente.get(i).getId()+"";
			cliente += " - "+ listaCliente.get(i).getNome();
			if (cliente.contains(query)){
				retorno.add(cliente);
			}
		}
		return retorno;
	}
	
	public List<String> completePagamento(String query){
		List<String> retorno = new ArrayList<String>();
		listFormasPagamento = formasPagamentoService.lista();
		String pagamento = null;
		for (int i = 0; i < listFormasPagamento.size();i++) {
			pagamento = listFormasPagamento.get(i).getId()+" - ";
			pagamento += listFormasPagamento.get(i).getDescricao();
			if (pagamento.contains(query)){
				retorno.add(pagamento);
			}
		}
		return retorno;
	}

	public void add(){
		ItemMovimento im = new ItemMovimento(produto, quantidade, this.venda);
		List<ItemMovimento> listaItens = this.venda.getItensMovimento();
		
	
		if (im.getQuantidade() > 0  ) {
			if(estoqueSuficiente(im)){
				if (listaItens.contains(im)) {
					ItemMovimento i = listaItens.get(listaItens.indexOf(im));
					i.setQuantidade(i.getQuantidade() + im.getQuantidade());
				}else{
					this.venda.getItensMovimento().add(im);
				}
				nomeProduto = ""; quantidade = 0; this.produto = null;
			}else{
				String nomeProduto = im.getProduto().getId()+"-"+im.getProduto().getDescricao();
				MessagesController.addWarn("Atenção", "Produto" +nomeProduto+" sem saldo suficiente!");
			}
		}else{
			MessagesController.addWarn("Atenção", "Produto sem quantidade definida!");
		}
		
	}
	
	private boolean estoqueSuficiente(ItemMovimento i) {
		return this.produtoService.temEstoqueSuficiente(i);
	}

	public String editarItem(){
		
		return null;
	}
	
	public String removerItem(){
		int i = this.venda.getItensMovimento().indexOf(item);
		this.venda.getItensMovimento().remove(i);
		return null;
	}
	
	public String cancelar(){
		return "/pages/user/successfulPage";
	}
	
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public List<Venda> getLista() {
		if(this.lista == null){
			this.lista = vendaService.lista();
		}
		return lista;
	}
	public void setLista(List<Venda> lista) {
		this.lista = lista;
	}

	public List<Cliente> getListaCliente() {
		listaCliente = clienteService.lista();
		return listaCliente;
	}

	public List<Produto> getListProduto() {
		listProduto = produtoService.lista();
		return listProduto;
	}

	public void setListProduto(List<Produto> listProduto) {
		this.listProduto = listProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public ItemMovimento getItem() {
		return item;
	}

	public void setItem(ItemMovimento item) {
		this.item = item;
	}

	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public FormasPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormasPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}
	
	public String listaVendas(){
		this.lista = null;
		return PAG_LISTA;
	}
}
