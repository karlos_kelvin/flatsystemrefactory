package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Apartamento;

public interface ApartamentoDAO {
	List<Apartamento> findAll();
	
	void save(Apartamento apartamento);
	void update(Apartamento apartamento);
	void remove(Apartamento apartamento);
	Apartamento getById(Long id);
}
