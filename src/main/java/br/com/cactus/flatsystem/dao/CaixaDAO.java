package br.com.cactus.flatsystem.dao;

import java.util.Date;
import java.util.List;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;

public interface CaixaDAO {

	List<Caixa> findAll();
	
	void save(Caixa caixa);
	void update(Caixa caixa);
	void remove(Caixa caixa);
	Caixa getById(Long id);
	Caixa getCaixaAtual() throws CaixaFechadoException;
	
	List<Caixa> getByClause(String clause);

	Caixa getCaixaPorData(Date data);
	
}
