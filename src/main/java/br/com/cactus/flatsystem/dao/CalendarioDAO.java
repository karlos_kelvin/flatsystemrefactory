package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Hospedagem;

public interface CalendarioDAO {
	List<Hospedagem> findAll();
	List<Hospedagem> listaHospedagens(String tipo);
	
	void save(Hospedagem hospedagem);
	void update(Hospedagem hospedagem);
	void remove(Hospedagem hospedagem);
	Hospedagem getById(Long id);
}
