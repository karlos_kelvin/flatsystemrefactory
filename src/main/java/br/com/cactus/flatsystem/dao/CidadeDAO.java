package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Cidade;
import br.com.cactus.flatsystem.entities.Estado;

public interface CidadeDAO {

	public List<Cidade> consultaCidade(Estado estado);
	
	public Cidade getById(Long id);
}
