package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Cliente;

public interface ClienteDAO {

	List<Cliente> findAll();
	
	void save(Cliente cliente);
	void update(Cliente cliente);
	void remove(Cliente cliente);
	Cliente getById(Long id);
	List<Cliente> buscaPorPaginacao(int posicaoPrimeiraLinha, int maximoPorPagina);
	int countAll();

	int getClientePorCPF(String cpf);
	int getClientePorCPFUpdate(String cpf, Long id);

}
