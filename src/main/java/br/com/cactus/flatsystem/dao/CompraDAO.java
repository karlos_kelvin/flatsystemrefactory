package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Compra;

public interface CompraDAO {
	List<Compra> findAll();
	
	void save(Compra compra);
	void update(Compra compra);
	void remove(Compra compra);
	Compra getById(Long id);

}
