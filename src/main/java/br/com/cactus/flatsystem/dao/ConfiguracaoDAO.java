package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Configuracao;

public interface ConfiguracaoDAO {

	void save(Configuracao configuracao);
	void update(Configuracao configuracao);
	Configuracao getById(Long id);
	List<Configuracao> findAll();
	
}
