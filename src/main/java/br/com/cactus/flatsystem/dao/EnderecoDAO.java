package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Endereco;

public interface EnderecoDAO {
	List<Endereco> findAll();	
	void save(Endereco endereco);
	void update(Endereco endereco);
	void remove(Endereco endereco);
	Endereco getById(Long id);
}
