package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Estabelecimento;

public interface EstabelecimentoDAO {

	List<Estabelecimento> findAll();
	void salvar(Estabelecimento estabelecimento);
	void update(Estabelecimento estabelecimento);
	void remove(Estabelecimento estabelecimento);
	Estabelecimento getById(Long id);
}
