package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Estado;

public interface EstadoDAO {

	public List<Estado> findAll();
	
	public Estado getById(Long id);

	public Estado getBySigla(String sigla);
}
