package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.FormasPagamento;

public interface FormasPagamentoDAO {
	List<FormasPagamento> findAll();
	
	void save(FormasPagamento forma_pagamento);
	void update(FormasPagamento forma_pagamento);
	void remove(FormasPagamento forma_pagamento);
	FormasPagamento getById(Long id);
}
