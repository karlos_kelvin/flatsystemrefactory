package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Fornecedor;


public interface FornecedorDAO {

	List<Fornecedor> findAll();	
	void save(Fornecedor fornecedor);
	void update(Fornecedor fornecedor);
	void remove(Fornecedor fornecedor);
	Fornecedor getById(Long id);
}
