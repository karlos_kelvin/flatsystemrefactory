package br.com.cactus.flatsystem.dao;

import java.util.Date;
import java.util.List;

import br.com.cactus.flatsystem.entities.Hospedagem;

public interface HospedagemDAO {
	List<Hospedagem> findAll();
	
	void save(Hospedagem hospedagem);
	void update(Hospedagem hospedagem);
	void remove(Hospedagem hospedagem);
	Hospedagem getById(Long id);
	
	List<Hospedagem> getHospedagensPeriodo(Date dataInicial, Date dataFinal, int apartamentoId);

	List<Hospedagem> hospedagensAbertas();

	List<Hospedagem> getPreReservas();

	List<Hospedagem> getReservas();
	
	List<Hospedagem> filtro(Hospedagem hospedagem);

}
