package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Venda;

public interface ItemVendaDAO {
	List<ItemMovimento> findAll();
	
	List<ItemMovimento> findByVenda(Venda venda);
	
	void save(ItemMovimento itemVenda);
	void update(ItemMovimento itemVenda);
	void remove(ItemMovimento itemVenda);
	ItemMovimento getById(Long id);

}
