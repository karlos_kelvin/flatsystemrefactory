package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.Transacao;

public interface LancamentoDAO {

	List<Lancamento> finAll();
	
	void save(Lancamento lancamento);
	void update(Lancamento lancamento);
	void remove(Lancamento lancamento);
	Lancamento getById(Long id);
	List<Lancamento> getLancamentoTransacoes(Caixa caixa);
	List<Lancamento> getLancamentoFormasPagamento(Caixa caixa);
	List<Lancamento> getLancamentosCaixa(Caixa caixa);

	List<Lancamento> getLancamentosPorTransacao(Caixa caixa, Transacao transacao);
	List<Lancamento> getLancamentosPorFormaDePagamento(Caixa caixa, FormasPagamento transacao);

	List<Lancamento> getLancamentosDeDespesa(Caixa caixa);

	List<Lancamento> getLancamentosDeReceita(Caixa caixa);
}
