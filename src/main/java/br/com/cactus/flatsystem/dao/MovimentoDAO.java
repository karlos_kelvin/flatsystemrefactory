package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Movimento;

public interface MovimentoDAO {
	List<Movimento> findAll();
	
	void save(Movimento movimento);
	void update(Movimento movimento);
	void remove(Movimento movimento);
	Movimento getById(Long id);
}
