package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Produto;

public interface ProdutoDAO {

	List<Produto> findAll();
	
	void save(Produto produto);
	void update(Produto produto);
	void remove(Produto produto);
	Produto getById(Long id);
}