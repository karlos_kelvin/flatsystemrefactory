package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Role;

public interface RoleDAO {

	List<Role> findAll();

	void save(Role role);

	void update(Role role);

	void remove(Role role);

	Role getById(Long id);
}
