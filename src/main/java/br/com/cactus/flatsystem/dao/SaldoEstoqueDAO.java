package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.entities.SaldoEstoque;

public interface SaldoEstoqueDAO {
	List<SaldoEstoque> findAll();
	
	void save(SaldoEstoque saldo_estoque);
	void update(SaldoEstoque saldo_estoque);
	void remove(SaldoEstoque saldo_estoque);
	SaldoEstoque getById(Long id);
	List<Number> getSaldo(Produto produto);
	Number getTotalCompra(Produto produto);
	Number getTotalVenda(Produto produto);
}
