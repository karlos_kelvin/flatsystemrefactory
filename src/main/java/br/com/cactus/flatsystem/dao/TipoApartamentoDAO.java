package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.TipoApartamento;

public interface TipoApartamentoDAO {

	List<TipoApartamento> findAll();
	
	void save(TipoApartamento tipo_apartamento);
	void update(TipoApartamento tipo_apartamento);
	void remove(TipoApartamento tipo_apartamento);
	
	TipoApartamento getById(Long id);
	
}
