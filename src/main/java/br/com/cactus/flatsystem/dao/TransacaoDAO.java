package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Transacao;

public interface TransacaoDAO {
	List<Transacao> findAll();
	
	List<Transacao> findSemSangriaSuprimento();
	
	void save(Transacao transacao);
	void update(Transacao transacao);
	void remove(Transacao transacao);
	Transacao getById(Long id);
	Transacao getSuprimento();
	Transacao getSangria();

	List<Transacao> getByClause(String string);

	List<Transacao> getTransacoesDeReceita();
}
