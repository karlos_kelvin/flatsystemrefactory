package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.User;

public interface UserDAO {
	List<User> findAll();

	void save(User user);

	void update(User user);

	void remove(User user);

	User getById(Long id);
	
	User login(String username, String password);
}
