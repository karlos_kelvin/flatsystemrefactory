package br.com.cactus.flatsystem.dao;

import java.util.List;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.entities.Venda;



public interface VendaDAO {
	List<Venda> findAll();
	
	List<Venda> findByHospedagem(Hospedagem hospedagem);
	
	void save(Venda venda);
	void update(Venda venda);
	void remove(Venda venda);
	Venda getById(Long id);
	
	List<Venda> getVendaPorCaixa(Caixa caixa);
}
