package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.ApartamentoDAO;
import br.com.cactus.flatsystem.entities.Apartamento;


public class ApartamentoDAOImpl implements ApartamentoDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
	public List<Apartamento> findAll() {
		return entityManager.createQuery("FROM Apartamento a ORDER BY a.id").getResultList();
	}

	
	@Override
	public void save(Apartamento apartamento) {
		entityManager.persist(apartamento);
	}

	@Override
	public void update(Apartamento apartamento) {
		entityManager.merge(apartamento);
	}

	@Override
	public void remove(Apartamento apartamento) {
		Query query = entityManager.createQuery("DELETE FROM Apartamento a WHERE a.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  apartamento.getId()).executeUpdate();
	}

	@Override
	public Apartamento getById(Long id) {
		return entityManager.find(Apartamento.class, id);
	}


}
