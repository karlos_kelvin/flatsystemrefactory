package br.com.cactus.flatsystem.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.CaixaDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;

public class CaixaDAOImpl implements CaixaDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Caixa> findAll() {
		return entityManager.createQuery("FROM Caixa c ORDER BY c.id").getResultList();
	}

	@Override
	public void save(Caixa caixa) {
		entityManager.persist(caixa);
	}

	@Override
	public void update(Caixa caixa) {
		entityManager.merge(caixa);
	}

	@Override
	public void remove(Caixa caixa) {
		caixa = entityManager.getReference(Caixa.class, caixa.getId());
		entityManager.remove(caixa);
		
	}

	@Override
	public Caixa getById(Long id) {
		return entityManager.find(Caixa.class, id);
	}
	
	@Override
	public Caixa getCaixaAtual() throws CaixaFechadoException{
		Caixa retorno = null;
		String consulta = "select c from Caixa c where c.dataFechamento = null";
		TypedQuery<Caixa> query = (TypedQuery<Caixa>) entityManager.createNativeQuery(consulta, Caixa.class);
		
		try{
			retorno = query.getSingleResult();
		}catch(javax.persistence.NoResultException e){
			throw new CaixaFechadoException();
		}
		
		return retorno;
	}

	@Override
	public Caixa getCaixaPorData(Date data) {
		Caixa retorno = null;
		String consulta = "select c from Caixa c where c.dataFechamento = null";
		TypedQuery<Caixa> query =  (TypedQuery<Caixa>) entityManager.createNativeQuery(consulta, Caixa.class);
		
		try{
			retorno = query.getSingleResult();
		}catch(javax.persistence.NoResultException e){
			
		}
		
		return retorno;
	}

	/**
	 * A implementa��o j� cont�m o in�cio da consulta, � necess�rio passar apenas a cl�usula WHERE para o m�todo.
	 * O objeto � referenciado pela letra 'c'.
	 * Ex�mplo: "WHERE c.atributo ..."
	 * @param clause
	 * @return
	 */
	@Override
	public List<Caixa> getByClause(String clause) {
		return entityManager.createQuery("FROM Caixa c "+ clause).getResultList();
	}

}
