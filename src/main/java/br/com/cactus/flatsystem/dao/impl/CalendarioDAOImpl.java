package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.CalendarioDAO;
import br.com.cactus.flatsystem.entities.Hospedagem;

public class CalendarioDAOImpl implements CalendarioDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Hospedagem> findAll() {		
		Query q =  entityManager.createQuery("FROM Hospedagem h where h.status = ?");
		List<Hospedagem> lista = q.setParameter(1, "A").getResultList();
		q = null;
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Hospedagem> listaHospedagens(String tipo) {	
		Query q = null;
		try {
			q =  entityManager.createQuery("FROM Hospedagem h where h.status = ?1 and h.tipo = ?2");
			q.setParameter(1, "A");
			q.setParameter(2, tipo);
			return q.getResultList();		
		}finally {
			q = null;
		}		
	}
	
	@Override
	public void save(Hospedagem hospedagem) {
		entityManager.persist(hospedagem);
	}

	@Override
	public void update(Hospedagem hospedagem) {
		entityManager.merge(hospedagem);
	}

	@Override
	public void remove(Hospedagem hospedagem) {
		Query query = entityManager.createQuery("DELETE FROM Hospedagem h WHERE h.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  hospedagem.getId()).executeUpdate();
	}

	@Override
	public Hospedagem getById(Long id) {
		return entityManager.find(Hospedagem.class, id);
	}


}
