package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.CidadeDAO;
import br.com.cactus.flatsystem.entities.Cidade;
import br.com.cactus.flatsystem.entities.Estado;

public class CidadeDAOImpl implements CidadeDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Cidade> consultaCidade(Estado estado) {
		List<Cidade> retorno = null;
		
		TypedQuery<Cidade> query = (TypedQuery<Cidade>) entityManager.createNativeQuery("SELECT c FROM Cidade c WHERE c.estado.idEstado = :idEstado", Cidade.class);
		query.setParameter("idEstado", estado.getIdEstado());
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			
		}
		
		return retorno;
	}

	@Override
	public Cidade getById(Long id) {
		return (Cidade)  entityManager.find(Cidade.class, id);
	}

}
