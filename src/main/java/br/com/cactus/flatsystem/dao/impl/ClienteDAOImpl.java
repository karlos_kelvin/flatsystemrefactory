package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.ClienteDAO;
import br.com.cactus.flatsystem.entities.Cliente;

public class ClienteDAOImpl implements ClienteDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Cliente> findAll() {
		return entityManager.createQuery("FROM Cliente c ORDER BY c.id").getResultList();
	}

	
	@Override
	public void save(Cliente cliente) {
		entityManager.persist(cliente);
	}

	@Override
	public void update(Cliente cliente) {
		entityManager.merge(cliente);
	}

	@Override
	public void remove(Cliente cliente) {
		cliente = getById(cliente.getId());
		entityManager.remove(cliente);
	}

	@Override
	public Cliente getById(Long id) {
		return entityManager.find(Cliente.class, id);
	}


	@Override
	public List<Cliente> buscaPorPaginacao(int posicaoPrimeiraLinha,
			int maximoPorPagina) {
		
		 Query query = entityManager.createQuery("select c from Cliente c");
	     query.setFirstResult(posicaoPrimeiraLinha);
	     query.setMaxResults(maximoPorPagina);
	 
	     return query.getResultList();
	}


	@Override
	public int countAll() {
		Query query = entityManager.createQuery("select COUNT(c) from Cliente c");
		 
        Number result = (Number) query.getSingleResult();
 
        return result.intValue();
	}


	@Override
	public int getClientePorCPF(String cpf) {
		int retorno;
		
		TypedQuery<Number> query =  (TypedQuery<Number>) entityManager.createNativeQuery("SELECT COUNT(c) FROM Cliente c WHERE c.cpf = :cpf", Number.class);
		query.setParameter("cpf", cpf);
		
		try {
			retorno = query.getSingleResult().intValue();
		} catch (NoResultException e) {
			retorno = 0;
		}
		return retorno;
	}


	@Override
	public int getClientePorCPFUpdate(String cpf, Long id) {
		int retorno;
		
		TypedQuery<Number> query = (TypedQuery<Number>) entityManager.createNativeQuery("SELECT COUNT(c) FROM Cliente c WHERE c.cpf = :cpf and c.id != :id", Number.class);
		query.setParameter("cpf", cpf);
	    query.setParameter("id", id);
		
		try {
			retorno = query.getSingleResult().intValue();
		} catch (NoResultException e) {
			retorno = 0;
		}
		return retorno;
	}

}
