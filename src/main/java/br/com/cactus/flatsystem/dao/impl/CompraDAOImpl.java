package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.CompraDAO;
import br.com.cactus.flatsystem.entities.Compra;

public class CompraDAOImpl implements CompraDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Compra> findAll() {
		return entityManager.createQuery("FROM Compra").getResultList();
	}

	@Override
	public void save(Compra compra) {
		entityManager.persist(compra);
	}

	@Override
	public void update(Compra compra) {
		entityManager.merge(compra);
	}

	@Override
	public void remove(Compra compra) {
		Query query = entityManager.createQuery("DELETE FROM Compra c WHERE c.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  compra.getId()).executeUpdate();
	}

	@Override
	public Compra getById(Long id) {
		return entityManager.find(Compra.class, id);
	}

}
