package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import br.com.cactus.flatsystem.dao.ConfiguracaoDAO;
import br.com.cactus.flatsystem.entities.Configuracao;

public class ConfiguracaoDAOImpl implements ConfiguracaoDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Configuracao configuracao) {
		entityManager.persist(configuracao);

	}

	@Override
	public void update(Configuracao configuracao) {
		entityManager.merge(configuracao);

	}

	@Override
	public Configuracao getById(Long id) {
		entityManager.find(Configuracao.class, id);
		return null;
	}

	@Override
	public List<Configuracao> findAll() {
		return entityManager.createQuery("FROM Configuracao").getResultList();
	}

}
