package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.EnderecoDAO;
import br.com.cactus.flatsystem.entities.Endereco;

public class EnderecoDAOImpl implements EnderecoDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Endereco> findAll() {
		return entityManager.createQuery("FROM Endereco").getResultList();
	}

	@Override
	public void save(Endereco endereco) {
		entityManager.persist(endereco);
	}

	@Override
	public void update(Endereco endereco) {
		entityManager.merge(endereco);
	}

	@Override
	public void remove(Endereco endereco) {
		Query query = entityManager.createQuery("DELETE FROM Endereco e WHERE e.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  endereco.getId()).executeUpdate();

	}

	@Override
	public Endereco getById(Long id) {
		return entityManager.find(Endereco.class, id);
	}

}
