package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.EstabelecimentoDAO;
import br.com.cactus.flatsystem.entities.Estabelecimento;

public class EstabelecimentoDAOImpl implements EstabelecimentoDAO {

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public List<Estabelecimento> findAll() {
		return entityManager.createQuery("FROM Estabelecimento").getResultList();
	}

	@Override
	public void salvar(Estabelecimento estabelecimento) {
		entityManager.persist(estabelecimento);
		
	}

	@Override
	public void update(Estabelecimento estabelecimento) {
		entityManager.merge(estabelecimento);
		
	}

	@Override
	public void remove(Estabelecimento estabelecimento) {
		Query query = entityManager.createQuery("DELETE FROM Estabelecimento e WHERE e.id = :id");
		int deletedCount = query.setParameter("id", estabelecimento.getId()).executeUpdate();
	}

	@Override
	public Estabelecimento getById(Long id) {
		return entityManager.find(Estabelecimento.class, id);
	}

}
