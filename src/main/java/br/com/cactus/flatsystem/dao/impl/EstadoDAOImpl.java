package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.EstadoDAO;
import br.com.cactus.flatsystem.entities.Estado;

public class EstadoDAOImpl implements EstadoDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Estado> findAll() {
		return entityManager.createQuery("FROM Estado").getResultList();
	}

	@Override
	public Estado getById(Long id) {
		return (Estado)  entityManager.find(Estado.class, id);
	}

	@Override
	public Estado getBySigla(String sigla) {
		TypedQuery<Estado> query = (TypedQuery<Estado>) entityManager.createNativeQuery("FROM Estado e WHERE e.sigla = :sigla", Estado.class);
		query.setParameter("sigla", sigla);
		return query.getSingleResult();
	}
	

}
