package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.dao.FormasPagamentoDAO;

public class FormasPagamentoDAOImpl implements FormasPagamentoDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FormasPagamento> findAll() {
		return entityManager.createQuery("FROM Formas_Pagamento").getResultList();
	}

	
	@Override
	public void save(FormasPagamento formas_pagamento) {
		entityManager.persist(formas_pagamento);
	}

	@Override
	public void update(FormasPagamento formas_pagamento) {
		entityManager.merge(formas_pagamento);
	}

	@Override
	public void remove(FormasPagamento formas_pagamento) {
		//entityManager.remove(formas_pagamento);
		Query query = entityManager.createQuery("DELETE FROM Formas_Pagamento a WHERE a.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  formas_pagamento.getId()).executeUpdate();
	}

	@Override
	public FormasPagamento getById(Long id) {
		return entityManager.find(FormasPagamento.class, id);
	}


}
