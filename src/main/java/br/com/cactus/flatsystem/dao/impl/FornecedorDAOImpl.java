package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.FornecedorDAO;
import br.com.cactus.flatsystem.entities.Fornecedor;

public class FornecedorDAOImpl implements FornecedorDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Fornecedor> findAll() {
		return entityManager.createQuery("FROM Fornecedor").getResultList();
	}

	@Override
	public void save(Fornecedor fornecedor) {
		entityManager.persist(fornecedor);
	}

	@Override
	public void update(Fornecedor fornecedor) {
		entityManager.merge(fornecedor);
	}

	@Override
	public void remove(Fornecedor fornecedor) {
		Query query = entityManager.createQuery("DELETE FROM Fornecedor f WHERE f.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  fornecedor.getId()).executeUpdate();
	}

	@Override
	public Fornecedor getById(Long id) {
		return entityManager.find(Fornecedor.class, id);
	}

}
