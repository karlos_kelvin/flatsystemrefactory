package br.com.cactus.flatsystem.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;

import br.com.cactus.flatsystem.dao.HospedagemDAO;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.util.StatusHospedagemEnum;
import br.com.cactus.flatsystem.util.TipoHospedagemEnum;

public class HospedagemDAOImpl implements HospedagemDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	private EasyCriteria<Hospedagem> easyCriteria;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Hospedagem> findAll() {
		return entityManager.createQuery("FROM Hospedagem h ORDER BY h.id").getResultList();
	}
	
	@Override
	public void save(Hospedagem hospedagem) {
		entityManager.persist(hospedagem);
	}

	@Override
	public void update(Hospedagem hospedagem) {
		entityManager.merge(hospedagem);
	}

	@Override
	public void remove(Hospedagem hospedagem) {
		Query query = entityManager.createQuery("DELETE FROM Hospedagem h WHERE h.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  hospedagem.getId()).executeUpdate();
		//hospedagem = getById(hospedagem.getId());
		//entityManager.remove(hospedagem);
	}

	@Override
	public Hospedagem getById(Long id) {
		return entityManager.find(Hospedagem.class, id);
	}

	@Override
	public List<Hospedagem> getHospedagensPeriodo(Date dataInicial,Date dataFinal,int apartamentoId) {
		
		String sql = "Select h FROM Hospedagem h "
				+ "where dataprevcheckin <= :data_fin "
				+ "and dataprevcheckout >= :data_ini "
				+ "and apartamento_id = :apartamento_id "
				+ "and status = :status "
				+ "order by apartamento_id, dataprevcheckin";
		
		TypedQuery<Hospedagem> qry = (TypedQuery<Hospedagem>) entityManager.createNativeQuery(sql, Hospedagem.class);
		qry.setParameter("data_fin", dataFinal);
		qry.setParameter("data_ini", dataInicial);
		qry.setParameter("status", "A");
		qry.setParameter("apartamento_id",  apartamentoId);
		
		List<Hospedagem> resultado = (List<Hospedagem>) qry.getResultList();
		
		return resultado;
	}

	@Override
	public List<Hospedagem> hospedagensAbertas() {
		List<Hospedagem> retorno = null;
		String sql = "Select h FROM Hospedagem h "
				+ " where h.status = :status "
				+ " and h.tipo = :tipo "
				+ " order by apartamento_id";
		
		TypedQuery<Hospedagem> qry = (TypedQuery<Hospedagem>) entityManager.createNativeQuery(sql, Hospedagem.class);
		qry.setParameter("status", StatusHospedagemEnum.ABERTO.getFlag());
		qry.setParameter("tipo", TipoHospedagemEnum.HOSPEDAGEM.getFlag());
		retorno = (List<Hospedagem>) qry.getResultList();
		
		return retorno;
	}

	@Override
	public List<Hospedagem> getPreReservas() {
		List<Hospedagem> retorno = null;
		String sql = "Select h FROM Hospedagem h "
				+ " where h.status = :status "
				+ " and h.tipo = :tipo "
				+ "order by apartamento_id";
		
		TypedQuery<Hospedagem> qry = (TypedQuery<Hospedagem>) entityManager.createNativeQuery(sql, Hospedagem.class);
		qry.setParameter("status", StatusHospedagemEnum.ABERTO.getFlag());
		qry.setParameter("tipo", TipoHospedagemEnum.PRE_RESERVA.getFlag());
		
		retorno = (List<Hospedagem>) qry.getResultList();
		
		return retorno;
	}

	@Override
	public List<Hospedagem> getReservas() {
		List<Hospedagem> retorno = null;
		String sql = "Select h FROM Hospedagem h "
				+ " where h.status = :status "
				+ " and h.tipo = :tipo "
				+ "order by apartamento_id";
		
		TypedQuery<Hospedagem> qry = (TypedQuery<Hospedagem>) entityManager.createNativeQuery(sql, Hospedagem.class);
		qry.setParameter("status", StatusHospedagemEnum.ABERTO.getFlag());
		qry.setParameter("tipo", TipoHospedagemEnum.RESERVA.getFlag());
		
		retorno = (List<Hospedagem>) qry.getResultList();
		
		return retorno;
	}

	@Override
	public List<Hospedagem> filtro(Hospedagem hospedagem) {
		easyCriteria = EasyCriteriaFactory.createQueryCriteria(entityManager, Hospedagem.class);
		if (hospedagem.getTipo()!= null) {
			easyCriteria.andEquals("tipo", hospedagem.getTipo());
		}
		if (hospedagem.getStatus() != null) {
			easyCriteria.andEquals("status", hospedagem.getStatus());
		}
		return easyCriteria.getResultList();
	}

}
