package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.ItemVendaDAO;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Venda;

public class ItemVendaDAOImpl implements ItemVendaDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemMovimento> findAll() {
		return entityManager.createQuery("FROM ItemMovimento").getResultList();
	}

	@Override
	public void save(ItemMovimento itemMovimento) {		
		entityManager.persist(itemMovimento);
	}

	@Override
	public void update(ItemMovimento itemMovimento) {
		entityManager.merge(itemMovimento);
	}

	@Override
	public void remove(ItemMovimento itemMovimento) {
		Query query = entityManager.createQuery("DELETE FROM ItemMovimento i WHERE i.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  itemMovimento.getId()).executeUpdate();
	}

	@Override
	public ItemMovimento getById(Long id) {
		return entityManager.find(ItemMovimento.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemMovimento> findByVenda(Venda venda) {
		Query query = entityManager.createQuery("select i FROM ItemMovimento i WHERE i.movimento = :venda");
		return query.setParameter("venda",venda).getResultList();
	}

	

}
