package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.LancamentoDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.TipoTransacao;
import br.com.cactus.flatsystem.entities.Transacao;

public class LancamentoDAOImpl implements LancamentoDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Lancamento> finAll() {
		
		return entityManager.createQuery("FROM Lancamento").getResultList();
	}

	@Override
	public void save(Lancamento lancamento) {
		if (lancamento.getCaixa() == null) {
			lancamento.setCaixa(null);
		}else{
			lancamento.getCaixa().getLancamentos().add(lancamento);
		}
		entityManager.persist(lancamento);
		
	}

	@Override
	public void update(Lancamento lancamento) {
		entityManager.merge(lancamento);
		
	}

	@Override
	public void remove(Lancamento lancamento) {
		Query query = entityManager.createQuery("DELETE FROM Lancamento i WHERE i.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id", lancamento.getId()).executeUpdate();
		
	}

	@Override
	public Lancamento getById(Long id) {	
		return entityManager.find(Lancamento.class, id);
	}

	@Override
	public List<Lancamento> getLancamentoTransacoes(Caixa caixa) {
		List<Lancamento> retorno = null;
		String consulta = " select NEW  br.com.mkl.entities.Lancamento(l.transacao,sum(l.valor) as valor) "
				+ " from Lancamento l "
				+ " join l.transacao t "
				+ " where l.caixa.id = :caixaId "
				+ " group by l.transacao";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixaId",caixa.getId());
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			
		}
		
		return retorno;
	}

	@Override
	public List<Lancamento> getLancamentoFormasPagamento(Caixa caixa) {
		List<Lancamento> retorno = null;
		String consulta = " select NEW  br.com.mkl.entities.Lancamento(l.formaPagamento, "
				+ " sum( CASE WHEN t.tipo = 0 THEN (l.valor * -1) ELSE l.valor END ) as valor ) "
				+ " from Lancamento l "
				+ " join l.formaPagamento f "
				+ " join l.transacao t "
				+ " where l.caixa.id = :caixaId "
				+ " group by l.formaPagamento";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixaId",caixa.getId());
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			
		}
		
		return retorno;
	}

	@Override
	public List<Lancamento> getLancamentosCaixa(Caixa caixa) {
		List<Lancamento> retorno = null;
		String consulta = " select l  from Lancamento l  where l.caixa.id = :caixaId ";
				
		TypedQuery<Lancamento> query =  (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixaId",caixa.getId());
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			
		}
		
		return retorno;
	}

	@Override
	public List<Lancamento> getLancamentosPorTransacao(Caixa caixa, Transacao transacao) {
		List<Lancamento> retorno = null;
		String consulta = " SELECT l "
				+ " FROM Lancamento l "
				+ " WHERE l.caixa = :caixa AND l.transacao = :transacao "
				+ " ORDER BY l.id ";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixa",caixa);
		query.setParameter("transacao",transacao);
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			throw e;
		}
		
		return retorno;
	}

	@Override
	public List<Lancamento> getLancamentosPorFormaDePagamento(Caixa caixa, FormasPagamento formaPagamento) {
		List<Lancamento> retorno = null;
		String consulta = " SELECT l "
				+ " FROM Lancamento l "
				+ " WHERE l.caixa = :caixa AND l.formaPagamento = :formaPagamento "
				+ " ORDER BY l.id ";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixa",caixa);
		query.setParameter("formaPagamento",formaPagamento);
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			throw e;
		}
		
		return retorno;

	}

	@Override
	public List<Lancamento> getLancamentosDeDespesa(Caixa caixa) {
		List<Lancamento> retorno = null;
		String consulta = " SELECT l "
				+ " FROM Lancamento l "
				+ " WHERE l.caixa = :caixa AND l.transacao.tipo = :despesa "
				+ " ORDER BY l.id ";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixa",caixa);
		query.setParameter("despesa",TipoTransacao.DESPESA);
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			throw e;
		}
		
		return retorno;
	}

	@Override
	public List<Lancamento> getLancamentosDeReceita(Caixa caixa) {
		List<Lancamento> retorno = null;
		String consulta = " SELECT l "
				+ " FROM Lancamento l "
				+ " WHERE l.caixa = :caixa AND l.transacao.tipo = :despesa "
				+ " ORDER BY l.id ";
				
		TypedQuery<Lancamento> query = (TypedQuery<Lancamento>) entityManager.createNativeQuery(consulta, Lancamento.class);
		query.setParameter("caixa",caixa);
		query.setParameter("despesa",TipoTransacao.RECEITA);
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			throw e;
		}
		
		return retorno;
	}

}
