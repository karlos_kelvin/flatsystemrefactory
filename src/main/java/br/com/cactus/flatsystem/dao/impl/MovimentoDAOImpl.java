package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.cactus.flatsystem.dao.MovimentoDAO;
import br.com.cactus.flatsystem.entities.Movimento;

public class MovimentoDAOImpl implements MovimentoDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Movimento> findAll() {
		return entityManager.createQuery("FROM Movimento").getResultList();
	}

	@Override
	public void save(Movimento movimento) {
		entityManager.persist(movimento);
	}

	@Override
	public void update(Movimento movimento) {
		entityManager.merge(movimento);
	}

	@Override
	public void remove(Movimento movimento) {
		entityManager.remove(movimento);
		//Query query = entityManager.createQuery("DELETE FROM Movimento m WHERE m.id = :id");		
		//SuppressWarnings("unused")
		//int deletedCount = query.setParameter("id",  movimento.getId()).executeUpdate();
	}

	@Override
	public Movimento getById(Long id) {
		return entityManager.find(Movimento.class, id);
	}

}
