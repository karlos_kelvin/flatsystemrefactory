package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.ProdutoDAO;
import br.com.cactus.flatsystem.entities.Produto;

public class ProdutoDAOImpl implements ProdutoDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> findAll() {
		return entityManager.createQuery("FROM Produto").getResultList();
	}

	@Override
	public void save(Produto produto) {
		entityManager.persist(produto);

	}

	@Override
	public void update(Produto produto) {
		entityManager.merge(produto);

	}

	@Override
	public void remove(Produto produto) {
		Query query = entityManager.createQuery("DELETE FROM Produto p WHERE p.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  produto.getId()).executeUpdate();
	}

	@Override
	public Produto getById(Long id) {
		return entityManager.find(Produto.class, id);
	}

	public static float getQuantidadeAtual(Produto produto) {
		//método não implementado
		return 0;
		
	}
}
