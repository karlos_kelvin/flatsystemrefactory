package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.dao.RoleDAO;
import br.com.cactus.flatsystem.entities.Role;

public class RoleDAOImpl implements RoleDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> findAll() {
		return entityManager.createQuery("FROM Role").getResultList();
	}

	
	@Override
	public void save(Role role) {
		entityManager.persist(role);
	}

	@Override
	public void update(Role role) {
		entityManager.merge(role);
	}

	@Override
	public void remove(Role role) {
		Query query = entityManager.createQuery("DELETE FROM Roles r WHERE r.role_id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("role_id",  role.getId()).executeUpdate();
	}

	@Override
	public Role getById(Long id) {
		return entityManager.find(Role.class, id);
	}


}
