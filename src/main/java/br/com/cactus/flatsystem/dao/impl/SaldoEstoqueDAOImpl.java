package br.com.cactus.flatsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.entities.SaldoEstoque;
import br.com.cactus.flatsystem.dao.SaldoEstoqueDAO;

public class SaldoEstoqueDAOImpl implements SaldoEstoqueDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<SaldoEstoque> findAll() {
		return entityManager.createQuery("FROM Saldo_Estoque").getResultList();
	}

	@Override
	public void save(SaldoEstoque saldo_estoque) {
		entityManager.persist(saldo_estoque);
	}

	@Override
	public void update(SaldoEstoque saldo_estoque) {
		entityManager.merge(saldo_estoque);
	}

	@Override
	public void remove(SaldoEstoque formas_pagamento) {
		Query query = entityManager
				.createQuery("DELETE FROM Saldo_Estoque s WHERE s.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id", formas_pagamento.getId())
				.executeUpdate();
	}

	@Override
	public SaldoEstoque getById(Long id) {
		return entityManager.find(SaldoEstoque.class, id);
	}
	
	public Number getTotalCompra(Produto produto){
		String consulta = null;
		TypedQuery<Number> query = null;
		Number numero = null;
		
		consulta = "SELECT DISTINCT coalesce((SELECT SUM(s.qtde) FROM Saldo_Estoque s WHERE s.tipo = 'C' AND s.produto_id = :cod_produto),0) AS total_compra "
				+ " FROM Saldo_Estoque s WHERE s.produto_id = :cod_produto";
		
		query  = (TypedQuery<Number>) entityManager.createNativeQuery(consulta, Number.class);
		query.setParameter("cod_produto",produto.getId());
		
		try {
			numero = query.getSingleResult();
		} catch (NoResultException e) {
			numero = 0;
		}
		
		return numero;
	}
	
	public Number getTotalVenda(Produto produto){
		String consulta = null;
		TypedQuery<Number> query = null;
		Number numero = null;
		
		consulta = "SELECT DISTINCT coalesce(SUM(s.qtde),0) AS total_venda FROM Saldo_Estoque s WHERE s.tipo = 'V' and s.produto_id = :cod_produto";
		
		query = (TypedQuery<Number>) entityManager.createNativeQuery(consulta, Number.class);
		query.setParameter("cod_produto",produto.getId());
		
		try {
			numero = query.getSingleResult();
		} catch (NoResultException e) {
			numero = 0;
		}
		
		return numero;
	}

	@Override
	public List<Number> getSaldo(Produto produto) {
		Number numero = null;
		List<Number> retorno = new ArrayList<Number>();
		
		String consulta = null;
		TypedQuery<Number> query = null;
		
		if (produto != null) {
			
		 try{	
				if (produto.getId() > 0){
				
					
				/*	
					String consulta ="SELECT (coalesce((SELECT SUM(s.qtde) FROM Saldo_Estoque s WHERE s.tipo = 'C' AND s.produto_id = :cod_produto),0) - coalesce(SUM(s.qtde),0)) AS saldo"
							+ " FROM Saldo_Estoque s WHERE s.tipo = 'V' AND s.produto_id = :cod_produto";
		
					TypedQuery<Number> query = entityManager.createQuery(consulta, Number.class);
					query.setParameter("cod_produto",produto.getId());
		
					try {
						numero = query.getSingleResult();
						retorno.add(numero);
					} catch (Exception e){
						System.out.println("dka�fjsdl�a");
					}
				*/
					consulta = "SELECT DISTINCT coalesce((SELECT SUM(s.qtde) FROM Saldo_Estoque s WHERE s.tipo = 'C' AND s.produto_id = :cod_produto),0) AS total_compra "
							+ " FROM Saldo_Estoque s WHERE s.produto_id = :cod_produto";
					
					query  = (TypedQuery<Number>) entityManager.createNativeQuery(consulta, Number.class);
					query.setParameter("cod_produto",produto.getId());
					
					try {
						numero = query.getSingleResult();
						retorno.add(numero);
					} catch (Exception e){
						System.out.println("dka�fjsdl�a");
					}
					
					consulta = "SELECT DISTINCT coalesce(SUM(s.qtde),0) AS total_venda FROM Saldo_Estoque s WHERE s.tipo = 'V' and s.produto_id = :cod_produto";
					
					query = (TypedQuery<Number>) entityManager.createNativeQuery(consulta, Number.class);
					query.setParameter("cod_produto",produto.getId());
					
					try {
						numero = query.getSingleResult();
						retorno.add(numero);
					} catch (Exception e){
						System.out.println("dka�fjsdl�a");
					}
				}
			}catch (NullPointerException e) {
				
			}
		}
		return retorno;
	}

}
