package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import br.com.cactus.flatsystem.entities.TipoApartamento;
import br.com.cactus.flatsystem.dao.TipoApartamentoDAO;

public class TipoApartamentoDAOImpl implements TipoApartamentoDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<TipoApartamento> findAll() {
		return entityManager.createQuery(" FROM Tipo_Apartamento ta ORDER BY ta.id").getResultList();
	}

	@Override
	public void save(TipoApartamento tipo_apartamento) {
		entityManager.persist(tipo_apartamento);

	}

	@Override
	public void update(TipoApartamento tipo_apartamento) {
		entityManager.merge(tipo_apartamento);

	}

	@Override
	public void remove(TipoApartamento tipo_apartamento) {
		Query query = entityManager.createQuery("DELETE FROM Tipo_Apartamento ta WHERE ta.id = :id");
		int deletedCount = query.setParameter("id", tipo_apartamento.getId()).executeUpdate();

	}

	@Override
	public TipoApartamento getById(Long id) {
		return entityManager.find(TipoApartamento.class, id);
	}

}
