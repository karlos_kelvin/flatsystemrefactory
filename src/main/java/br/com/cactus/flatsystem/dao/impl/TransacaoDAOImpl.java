package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.TransacaoDAO;
import br.com.cactus.flatsystem.entities.TipoTransacao;
import br.com.cactus.flatsystem.entities.Transacao;

public class TransacaoDAOImpl implements TransacaoDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Transacao> findAll() {
		return entityManager.createQuery("FROM Transacao").getResultList();
	}
	
	public List<Transacao> findSemSangriaSuprimento(){
		return entityManager.createQuery("FROM Transacao t WHERE t.id > 2").getResultList();
	}

	
	@Override
	public void save(Transacao transacao) {
		entityManager.persist(transacao);
	}

	@Override
	public void update(Transacao transacao) {
		entityManager.merge(transacao);
	}

	@Override
	public void remove(Transacao transacao) {
		Query query = entityManager.createQuery("DELETE FROM Transacao t WHERE t.id = :id");
		@SuppressWarnings("unused")
		int deletedCount = query.setParameter("id",  transacao.getId()).executeUpdate();
	}

	@Override
	public Transacao getById(Long id) {
		return entityManager.find(Transacao.class, id);
	}

	@Override
	public Transacao getSuprimento() {
		return getById( new Long(2) );
	}

	@Override
	public Transacao getSangria() {
		return getById( new Long(1) );
	}

	@Override
	public List<Transacao> getByClause(String consulta) {
		Query query = entityManager.createQuery("FROM Transacao t "+consulta);
		return query.getResultList();
	}

	@Override
	public List<Transacao> getTransacoesDeReceita() {
		List<Transacao> retorno = null;
		String consulta = " SELECT t "
				+ " FROM Transacao t "
				+ " WHERE t.id > 2 and t.tipo = :tipo "
				+ " ORDER BY t.id ";
				
		TypedQuery<Transacao> query = (TypedQuery<Transacao>) entityManager.createNativeQuery(consulta, Transacao.class);
		query.setParameter("tipo",TipoTransacao.RECEITA);
		
		try{
			retorno = query.getResultList();
		}catch(javax.persistence.NoResultException e){
			throw e;
		}
		
		return retorno;	}


}
