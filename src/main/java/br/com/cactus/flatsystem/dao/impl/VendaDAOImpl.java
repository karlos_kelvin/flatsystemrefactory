package br.com.cactus.flatsystem.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import br.com.cactus.flatsystem.dao.VendaDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.entities.Venda;

public class VendaDAOImpl implements VendaDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Venda> findAll() {
		return entityManager.createQuery("FROM Venda").getResultList();
	}

	@Override
	public void save(Venda venda) {
		entityManager.persist(venda);
	}

	@Override
	public void update(Venda venda) {
		entityManager.merge(venda);
	}

	@Override
	public void remove(Venda venda) {
		venda = entityManager.getReference(Venda.class, venda.getId());
		entityManager.remove(venda);
	}

	@Override
	public Venda getById(Long id) {
		return entityManager.find(Venda.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Venda> findByHospedagem(Hospedagem hospedagem) {
		Query query = entityManager.createQuery("select v FROM Venda v WHERE v.hospedagem = :hospedagem");
		return query.setParameter("hospedagem",hospedagem).getResultList();
	}

	@Override
	public List<Venda> getVendaPorCaixa(Caixa caixa) {
		List<Venda> retorno = null;
		String consulta = "select v from Venda v join v.lancamento l where l.caixa = :caixa";
		TypedQuery<Venda> query = (TypedQuery<Venda>) entityManager.createNativeQuery(consulta, Venda.class);
		try{
			return query.setParameter("caixa", caixa).getResultList();
		}catch(javax.persistence.NoResultException e){
			System.out.println("nada");

			return null;
		}
	}

}
