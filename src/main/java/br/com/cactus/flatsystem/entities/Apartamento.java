package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="apartamento")
@SequenceGenerator(name="apartamento_codigo_seq", sequenceName="apartamento_codigo_seq", allocationSize=1)
public class Apartamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="apartamento_codigo_seq")
	private Long id;	
	private int numero_apartamento;
	
	private int capacidade;
	private double valor_diaria;
	private String descricao;
	
	
	@OneToOne
	@JoinColumn(name = "tipo_apartamento_id")	
	private TipoApartamento tipo = new TipoApartamento();
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getNumero_apartamento() {
		return numero_apartamento;
	}
	public void setNumero_apartamento(int numero_apartamento) {
		this.numero_apartamento = numero_apartamento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public double getValor_diaria() {
		return valor_diaria;
	}
	public void setValor_diaria(double valor_diaria) {
		this.valor_diaria = valor_diaria;
	}
	public TipoApartamento getTipo() {
		return tipo;
	}
	public void setTipo(TipoApartamento tipo) {
		this.tipo = tipo;
	}
	
	

}
