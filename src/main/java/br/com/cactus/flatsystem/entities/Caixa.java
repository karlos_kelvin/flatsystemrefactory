package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="caixa_codigo_seq", sequenceName="caixa_codigo_seq", allocationSize=1)
public class Caixa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6340914836039790969L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="caixa_codigo_seq")
	@Column(name="caixa_id")
	private Long id;
	
	@OneToMany(	mappedBy = "caixa", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch=FetchType.EAGER)
	private List<Lancamento> lancamentos = new ArrayList<Lancamento>();
	
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dataAbertura;
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dataFechamento;
	
	public Caixa() {
		super();
	}
	
	public Caixa(Date dataAbertura) {
		super();
		this.dataAbertura = dataAbertura;
	}

	public double getTotal(){
		double valor = 0;
		for (Lancamento l : lancamentos) {
			if(l.getTransacao().getTipo().getValor()==0){// eh uma despesa 
				valor -= l.getValor();
			}else{
				valor += l.getValor();
			}
		}
		return valor;
	}	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataAbertura == null) ? 0 : dataAbertura.hashCode());
		result = prime * result
				+ ((dataFechamento == null) ? 0 : dataFechamento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lancamentos == null) ? 0 : lancamentos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caixa other = (Caixa) obj;
		if (dataAbertura == null) {
			if (other.dataAbertura != null)
				return false;
		} else if (!dataAbertura.equals(other.dataAbertura))
			return false;
		if (dataFechamento == null) {
			if (other.dataFechamento != null)
				return false;
		} else if (!dataFechamento.equals(other.dataFechamento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lancamentos == null) {
			if (other.lancamentos != null)
				return false;
		} else if (!lancamentos.equals(other.lancamentos))
			return false;
		return true;
	}

	/**
	 * Método para saber se um caixa está aberto ou não
	 * @return Retorna true caso a data de fechamento do caixa seja igual a null
	 */
	public boolean isClosed() {
		return (dataFechamento != null);
	}


}
