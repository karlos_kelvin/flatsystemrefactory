package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Compra extends Movimento implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421703655718964549L;
	
	@OneToOne
	@JoinColumn(name="fornecedor_id", nullable=true)
	Fornecedor fornecedor;

	public Compra() {
		super();
	}
	
	public Compra(boolean semLancamento){
		super();
		if (semLancamento) {
				this.lancamento = null;
		}
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public Produto getProduto() throws NullPointerException{
		return this.itensMovimento.get(0).getProduto();
	}

	public float getQuantidadeProduto() throws NullPointerException{
		return this.itensMovimento.get(0).getQuantidade();
	}
	
	/**
	 * 
	 * @return retorna o primeiro item da lista
	 */
	public ItemMovimento getItemMovimento() throws NullPointerException{
		return this.itensMovimento.get(0);
	}

}
