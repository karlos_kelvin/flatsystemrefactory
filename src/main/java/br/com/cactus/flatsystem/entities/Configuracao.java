package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="config_codigo_seq", sequenceName="config_codigo_seq", allocationSize=1)
public class Configuracao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4660735641327824338L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="config_codigo_seq")
	@Column(name="configuracao_id")
	private long id;
	
	@Temporal(TemporalType.TIME)
	private Date horaInicioHospedagem;

	@Temporal(TemporalType.TIME)
	private Date toleranciaHospedagem;
	
	private boolean calculaMeiaHospedagem;
	
	@OneToMany
	private List<WebService> webServices;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getHoraInicioHospedagem() {
		return horaInicioHospedagem;
	}

	public void setHoraInicioHospedagem(Date horaInicioHospedagem) {
		this.horaInicioHospedagem = horaInicioHospedagem;
	}
	
	public Date getToleranciaHospedagem() {
		return toleranciaHospedagem;
	}

	public void setToleranciaHospedagem(Date toleranciaHospedagem) {
		this.toleranciaHospedagem = toleranciaHospedagem;
	}
	
	public boolean isCalculaMeiaHospedagem() {
		return calculaMeiaHospedagem;
	}

	public void setCalculaMeiaHospedagem(boolean calculaMeiaHospedagem) {
		this.calculaMeiaHospedagem = calculaMeiaHospedagem;
	}

	public List<WebService> getWebServices() {
		return webServices;
	}

	public void setWebServices(List<WebService> webServices) {
		this.webServices = webServices;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (calculaMeiaHospedagem ? 1231 : 1237);
		result = prime
				* result
				+ ((horaInicioHospedagem == null) ? 0 : horaInicioHospedagem
						.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((toleranciaHospedagem == null) ? 0 : toleranciaHospedagem
						.hashCode());
		result = prime * result
				+ ((webServices == null) ? 0 : webServices.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuracao other = (Configuracao) obj;
		if (calculaMeiaHospedagem != other.calculaMeiaHospedagem)
			return false;
		if (horaInicioHospedagem == null) {
			if (other.horaInicioHospedagem != null)
				return false;
		} else if (!horaInicioHospedagem.equals(other.horaInicioHospedagem))
			return false;
		if (id != other.id)
			return false;
		if (toleranciaHospedagem == null) {
			if (other.toleranciaHospedagem != null)
				return false;
		} else if (!toleranciaHospedagem.equals(other.toleranciaHospedagem))
			return false;
		if (webServices == null) {
			if (other.webServices != null)
				return false;
		} else if (!webServices.equals(other.webServices))
			return false;
		return true;
	}
		
}
