package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.cactus.flatsystem.model.exception.SetMatrizException;

@Entity
@SequenceGenerator(name="estabelecimento_codigo_seq", sequenceName="estabelecimento_codigo_seq", allocationSize=1)
public class Estabelecimento implements Serializable{

	private static final long serialVersionUID = 4849564364877662299L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="estabelecimento_codigo_seq")
	@Column(name="estabelecimento_id")
	private Long id;
	
	private String cnpj;
	private String ie;
	private String im;
	private String razaoSocial;
	private String fantasia;
	private String telefone;
	private String email;
	private String fax;
	
	/*
	@ManyToOne
	@JoinColumn(name="matriz_id")
	private Estabelecimento matriz = new Estabelecimento();
	*/
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "endereco_id")	
	private Endereco endereco = new Endereco();
	
	/*
	@OneToMany(mappedBy="matriz")
	private List<Estabelecimento> filiais = new ArrayList<Estabelecimento>();
	*/
	
	public Estabelecimento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getFantasia() {
		return fantasia;
	}

	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/*
	public Estabelecimento getMatrizObj() {
		return matriz;
	}

	public void setMatrizObj(Estabelecimento matrizObj) throws SetMatrizException {
		if (matrizObj != null) {
			if (this.filiais.size() > 0) {
				throw new SetMatrizException("Impossóvel setar matriz como matriz!");
			}else{
				this.matriz = matrizObj;
			}			
		}else{
			this.matriz = matrizObj;			
		}
		
		
	}

	public boolean isMatriz() {
		boolean retorno = false;
		if ((this.matriz != null) && (this.matriz.getId() > 0)) {
			retorno = true;
		}
		return retorno;
	}

	public List<Estabelecimento> getFiliais() {
		return filiais;
	}

	public void setFiliais(List<Estabelecimento> filiais) {
		this.filiais = filiais;
	}
	
	*/
	 

}
