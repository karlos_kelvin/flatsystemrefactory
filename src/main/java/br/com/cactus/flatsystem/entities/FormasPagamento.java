package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="formas_pagamento")
@SequenceGenerator(name="formapag_codigo_seq", sequenceName="formapag_codigo_seq", allocationSize=1)
public class FormasPagamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="formapag_codigo_seq")
	@Column(name = "fpg_id")
	private Long id;	
	private String descricao;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
