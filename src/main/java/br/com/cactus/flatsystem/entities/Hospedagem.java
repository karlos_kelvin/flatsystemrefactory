package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import br.com.cactus.flatsystem.util.StatusHospedagemEnum;

@Entity
@SequenceGenerator(name="hospedagem_codigo_seq", sequenceName="hospedagem_codigo_seq", allocationSize=1)
public class Hospedagem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3088805666582993200L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="hospedagem_codigo_seq")
	@Column(name="hospedagem_id")
	private Long id;

	private Date   dataReserva;
	
	@Temporal(TemporalType.DATE)
	private Date   dataPrevCheckin;
	
	@Temporal(TemporalType.DATE)
	private Date   dataPrevCheckout;
	
	@Temporal(TemporalType.TIME)
	private Date   horaCheckin;
	
	@Temporal(TemporalType.TIME)
	private Date   horaCheckout;
	
	private Date   dataDeposito;
	
	private int    acompanhantes;
	
	private double qtdDiarias;

	private double valorDiaria;
	private double desconto;
	private double acrescimo;
	private double valorTotal;
	private double valorDepositado;
	@Column(length=2000)
	private String observacoes;
	@Column(length=1)
	private String tipo;
	@Column(length=1)
	private String status;
	
	private int meioTransporte;
	private int motivoViagem;
	private String ultimaProcedencia;
	private String proxDestino;
	
	@OneToMany(mappedBy="hospedagem")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Venda> vendas;
	
	@Transient
	private List<String> listaAcompanhantes;
	
	@OneToMany(mappedBy="hospedagem", cascade = { CascadeType.REMOVE, CascadeType.ALL, CascadeType.MERGE, CascadeType.PERSIST }, fetch=FetchType.EAGER)
	private List<Lancamento> listaLancamentos = new ArrayList<Lancamento>();
	
	@OneToOne
	@JoinColumn(name = "cliente_id")	
	private Cliente cliente = new Cliente();
	
	@OneToOne
	@JoinColumn(name = "apartamento_id")	
	private Apartamento apartamento = new Apartamento();

	
	public Hospedagem() {
		super();
	}
	
	public Hospedagem(StatusHospedagemEnum status){
		this.status = status.getFlag();
	}
	
	public List<Lancamento> getListaLancamentos() {
		return listaLancamentos;
	}
	public void setListaLancamentos(List<Lancamento> listaLancamentos) {
		this.listaLancamentos = listaLancamentos;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDataReserva() {
		return dataReserva;
	}
	public void setDataReserva(Date dataReserva) {
		this.dataReserva = dataReserva;
	}
	public Date getDataPrevCheckin() {
		return dataPrevCheckin;
	}
	public void setDataPrevCheckin(Date dataPrevCheckin) {
		this.dataPrevCheckin = dataPrevCheckin;
	}
	public Date getDataPrevCheckout() {
		return dataPrevCheckout;
	}
	public void setDataPrevCheckout(Date dataPrevCheckout) {
		this.dataPrevCheckout = dataPrevCheckout;
	}
	public Date getDataDeposito() {
		return dataDeposito;
	}
	public void setDataDeposito(Date dataDeposito) {
		this.dataDeposito = dataDeposito;
	}
	public double getValorDepositado() {
		return valorDepositado;
	}
	public void setValorDepositado(double valorDepositado) {
		this.valorDepositado = valorDepositado;
	}
	public int getAcompanhantes() {
		return acompanhantes;
	}
	public void setAcompanhantes(int acompanhantes) {
		this.acompanhantes = acompanhantes;
	}
	public Double getValor() {
		return valorDiaria;
	}
	public void setValor(Double valor) {
		this.valorDiaria = valor;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Apartamento getApartamento() {
		return apartamento;
	}
	public void setApartamento(Apartamento apartamento) {
		this.apartamento = apartamento;
	}
	
	public String getNomeCliente(){
		return cliente.getNome();
	}
	
	public String getApartamentoReservado(){
		if (apartamento.getNumero_apartamento() > 0) {
			return String.valueOf(apartamento.getNumero_apartamento());
		} else {
			return "";
		}
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoHospedagem() {
		switch (getTipo()) {
			case "P" : return "Pré-Reserva";
			case "R" : return "Reserva";
			case "H" : return "Hospedagem";
			default  : return "Nenhum tipo selecionado";
		}
	}
	public String getStatus() {
		return status;
	}
	
	public String getTipoStatus(){
		switch(getStatus()){
			case "A" : return "ABERTO";
			case "F" : return "ENCERRADO";
			default  : return "";
		}
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
	public void setLancamento(Lancamento lancamento) {
		this.listaLancamentos.add(lancamento);
	}
	public double getDesconto() {
		return desconto;
	}
	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}
	public double getAcrescimo() {
		return acrescimo;
	}
	public void setAcrescimo(double acrescimo) {
		this.acrescimo = acrescimo;
	}
	
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public Date getHoraCheckin() {
		return horaCheckin;
	}
	public void setHoraCheckin(Date horaCheckin) {
		this.horaCheckin = horaCheckin;
	}
	public Date getHoraCheckout() {
		return horaCheckout;
	}
	public void setHoraCheckout(Date horaCheckout) {
		this.horaCheckout = horaCheckout;
	}
	public double getQtdDiarias() {
		return qtdDiarias;
	}
	public void setQtdDiarias(double qtdDiarias) {
		this.qtdDiarias = qtdDiarias;
	}
	public int getMeioTransporte() {
		return meioTransporte;
	}
	public void setMeioTransporte(int meioTransporte) {
		this.meioTransporte = meioTransporte;
	}
	public int getMotivoViagem() {
		return motivoViagem;
	}
	public void setMotivoViagem(int motivoViagem) {
		this.motivoViagem = motivoViagem;
	}
	public String getUltimaProcedencia() {
		return ultimaProcedencia;
	}
	public void setUltimaProcedencia(String ultimaProcedencia) {
		this.ultimaProcedencia = ultimaProcedencia;
	}
	public String getProxDestino() {
		return proxDestino;
	}
	public void setProxDestino(String proxDestino) {
		this.proxDestino = proxDestino;
	}
	
	/**
	 * Método para saber a quantidade de pessoas na hospedagem.
	 * @return retorna a quantidade de acompanhantes mais um.
	 */
	public int totalDeHospedes(){
		int retorno = 0;
		retorno = this.acompanhantes + 1;
		return retorno;
	}
	public List<Venda> getVendas() {
		return vendas;
	}
	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public boolean isAberta() {
		boolean retorno = false;

		if (getStatus().equals(StatusHospedagemEnum.ABERTO.getFlag())) {
			retorno = true;
		}

		return retorno;
	}
	
	public float getTotalItensHospedagem() {

		float result = 0;
		
		for (Venda v : vendas) {
			result += v.getTotal();
		}
		
		return result;
	}
	
	/**
	 * Retorna o valor da total da hospedagem
	 *  
	 * @param diarias
	 * @return
	 */
	public double calculaTotalHospedagem(double diarias){
		double retorno = 0;
		retorno = diarias * getValor() + getTotalItensHospedagem();
		valorTotal = retorno;
		return retorno;
	}
	
	public double getTotalLancamentosConfirmados() {

		double valorConfirmado = 0;

		for (Lancamento l : getListaLancamentos()) {
			if (l.isConfirmado()) {
				if (l.isDespesa()) {
					valorConfirmado -= l.getValor();
				} else {
					valorConfirmado += l.getValor();
				}
			}
		}

		return valorConfirmado;
	}

	/**
	 * retorna true caso o chekin ocorra antes do checkout
	 * @return
	 */
	public boolean validadeDatas() {
		return dataPrevCheckin.before(dataPrevCheckout);
	}
}
