package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="item_codigo_seq", sequenceName="item_codigo_seq", allocationSize=1)
public class ItemMovimento implements Serializable {
	
	
	private static final long serialVersionUID = -3044526052807735246L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="item_codigo_seq")
	@Column(name="item_id")
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "produto_id")	
	private Produto produto;
	
	private float quantidade;
	
	@ManyToOne
	@JoinColumn(name="movimento_id")
	private Movimento movimento;
	
	public Movimento getMovimento() {
		return movimento;
	}

	public void setMovimento(Movimento movimento) {
		this.movimento = movimento;
	}

	public float getTotal(){
		float result = 0;
		result = this.produto.getPreco() * this.quantidade;
		return result;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public float getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((movimento == null) ? 0 : movimento.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + Float.floatToIntBits(quantidade);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemMovimento other = (ItemMovimento) obj;
		
		if (movimento == null) {
			if (other.movimento != null)
				return false;
		} else if (!movimento.equals(other.movimento))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		
		return true;
	}
	
	public ItemMovimento(Produto produto, float quantidade, Movimento movimento) {
		super();
		this.produto = produto;
		this.quantidade = quantidade;
		this.movimento = movimento;
	}
	public ItemMovimento() {
		super();
	}	

}
