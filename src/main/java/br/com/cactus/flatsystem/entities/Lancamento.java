package br.com.cactus.flatsystem.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import java.io.Serializable;
import java.util.Date;

@Entity
@SequenceGenerator(name="lancamento_codigo_seq", sequenceName="lancamento_codigo_seq", allocationSize=1)
public class Lancamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9023129719486899538L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lancamento_codigo_seq")
	@Column(name="lanc_id")
	private Long id;

	@OneToOne
	@JoinColumn(name="transacao_id")
	private Transacao transacao = new Transacao();
	
	@OneToOne
	@JoinColumn(name="forma_pagamento_id", nullable=true)
	private FormasPagamento formaPagamento = new FormasPagamento();
	
	private double valor;
	private String observacao;
	private Date data;
	private Date dataConfirmado;
	private boolean confirmado;
	
	@ManyToOne
	@JoinColumn(name="caixa_id", nullable=true)
	private Caixa caixa = new Caixa();
	
	@ManyToOne
	@JoinColumn(name="hospedagem_id", nullable=true)
	private Hospedagem hospedagem;
	
	public Lancamento(Transacao transacao, FormasPagamento formaPagamento, double valor, String observacao, Date data, boolean confirmado){
		super();
		this.transacao = transacao;
		this.formaPagamento = formaPagamento;
		this.valor = valor;
		this.observacao = observacao;
		this.data = data;
		this.confirmado = confirmado;
	}
	
	public Lancamento(Transacao transacao, double valor, String observacao) {
		super();
		this.transacao = transacao;
		this.valor = valor;
		this.observacao = observacao;
	}

	public Lancamento( Transacao transacao, double valor) {
		super();
		this.valor = valor;
		this.transacao  = transacao;
	}
	
	public Lancamento(Long id, Transacao transacao, double valor, String observacao) {
		super();
		this.id    = id;
		this.valor = valor;
		this.transacao  = transacao;
		this.observacao = observacao;
	}
	
	public Lancamento(FormasPagamento formaPagamento, double valor, String observacao) {
		super();
		this.formaPagamento = formaPagamento;
		this.valor = valor;
		this.observacao = observacao;
	}
	
	public Lancamento(FormasPagamento formaPagamento, double valor) {
		super();
		this.formaPagamento = formaPagamento;
		this.valor = valor;
	
	}
	
	
	public Lancamento() {
		super();
	}
	
	public Lancamento(Caixa caixaAtual, Transacao transacaoSangria, double valor, String obs) {
		super();
		this.transacao  = transacao;
		this.valor 		= valor;
		this.observacao = observacao;
		this.caixa 		= caixa;
	}

	public Transacao getTransacao() {
		return transacao;
	}
	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}
	/**
	 * 
	 * @return retorna o valor de acordo com a transação, positivo caso seja receita e negativo caso seja despesa
	 */
	public double getValorPorTransacao(){
		double retorno = this.valor;
		if(transacao.getTipo().getValor() == 0){
			retorno = this.valor * -1;
		}
		return retorno;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public boolean isConfirmado() {
		return confirmado;
	}
	
	public void setConfirmado(boolean confirmado) {
		this.confirmado = confirmado;
		if (this.confirmado){
			this.dataConfirmado = new Date();
		}else{
			this.dataConfirmado = null;
		}
	}
	public Long getId() {
		return id;
	}
	
	public String getLancamentoConfirmado() {
		if (confirmado) 
			return "Sim";
		else	
			return "Não";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (confirmado ? 1231 : 1237);
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result
				+ ((formaPagamento == null) ? 0 : formaPagamento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((transacao == null) ? 0 : transacao.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamento other = (Lancamento) obj;
		if (confirmado != other.confirmado)
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (formaPagamento == null) {
			if (other.formaPagamento != null)
				return false;
		} else if (!formaPagamento.equals(other.formaPagamento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (transacao == null) {
			if (other.transacao != null)
				return false;
		} else if (!transacao.equals(other.transacao))
			return false;
		if (Double.doubleToLongBits(valor) != Double
				.doubleToLongBits(other.valor))
			return false;
		return true;
	}

	public FormasPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormasPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Hospedagem getHospedagem() {
		return hospedagem;
	}

	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}

	public Date getDataConfirmado() {
		return dataConfirmado;
	}

	public void setDataConfirmado(Date dataConfirmado) {
		this.dataConfirmado = dataConfirmado;
	}
	
	public boolean isDespesa(){
		return transacao.getTipo() == TipoTransacao.DESPESA;
	}

}