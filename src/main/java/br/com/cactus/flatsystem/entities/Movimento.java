package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(name="movimento_codigo_seq", sequenceName="movimento_codigo_seq", allocationSize=1)
public abstract class Movimento implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8899627815224245027L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="movimento_codigo_seq")
	@Column(name="movimento_id")
	private Long id;
	
	@OneToMany(mappedBy = "movimento", cascade = {CascadeType.PERSIST, CascadeType.REMOVE} )
	@LazyCollection(LazyCollectionOption.FALSE)
	List<ItemMovimento> itensMovimento = new ArrayList<ItemMovimento>();
		
	private Date data;
	
	@OneToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE})
	@JoinColumn(name="lancamento_id", nullable=true)
	protected Lancamento lancamento = new Lancamento();
	
	public float getTotal(){
		float result = 0;
		for (ItemMovimento item : itensMovimento) {
			result += item.getTotal();
		}
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemMovimento> getItensMovimento() {
		return itensMovimento;
	}

	public void setItensMovimento(List<ItemMovimento> itensVenda) {
		this.itensMovimento = itensVenda;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itensMovimento == null) ? 0 : itensMovimento.hashCode());
		result = prime * result
				+ ((lancamento == null) ? 0 : lancamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimento other = (Movimento) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itensMovimento == null) {
			if (other.itensMovimento != null)
				return false;
		} else if (!itensMovimento.equals(other.itensMovimento))
			return false;
		if (lancamento == null) {
			if (other.lancamento != null)
				return false;
		} else if (!lancamento.equals(other.lancamento))
			return false;
		return true;
	}
	
}
