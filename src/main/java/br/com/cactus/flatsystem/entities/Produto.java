package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.cactus.flatsystem.dao.impl.ProdutoDAOImpl;

@Entity
@Table(name = "produto")
@SequenceGenerator(name="produto_codigo_seq", sequenceName="produto_codigo_seq", allocationSize=1)
public class Produto implements Serializable{
		
	private static final long serialVersionUID = -4963056010978401223L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="produto_codigo_seq")
	@Column(name="produto_id")
	private Long id;
	
	private String descricao;
	private float preco;
	private float estoqueMinimo;
	
	@Transient
	private double total_compra;
	
	@Transient
	private double total_venda;
	
	@Transient
	private double saldo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getQuantidade() {
		return ProdutoDAOImpl.getQuantidadeAtual(this);
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public float getEstoqueMinimo() {
		return estoqueMinimo;
	}
	public void setEstoqueMinimo(float estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}
	
	public double getTotal_compra() {
		return total_compra;
	}
	public void setTotal_compra(double total_compra) {
		this.total_compra = total_compra;
	}
	public double getTotal_venda() {
		return total_venda;
	}
	public void setTotal_venda(double total_venda) {
		this.total_venda = total_venda;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + Float.floatToIntBits(estoqueMinimo);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Float.floatToIntBits(preco);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (Float.floatToIntBits(estoqueMinimo) != Float
				.floatToIntBits(other.estoqueMinimo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Float.floatToIntBits(preco) != Float.floatToIntBits(other.preco))
			return false;
		return true;
	}
	
	

}
