package br.com.cactus.flatsystem.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="role")
@SequenceGenerator(name="role_codigo_seq", sequenceName="role_codigo_seq", allocationSize=1)
public class Role {
	private static final long serialVersionUID = -3968396919486158590L;

	private Long id;
	private String description;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="role_codigo_seq")
	@Column(name="role_id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="role_description", unique=true)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
