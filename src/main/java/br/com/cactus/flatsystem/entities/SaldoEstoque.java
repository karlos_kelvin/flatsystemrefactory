package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name="saldo_estoque", uniqueConstraints={@UniqueConstraint(columnNames={"produto_id","movimentoId"})})
@SequenceGenerator(name="saldoestoque_codigo_seq", sequenceName="saldoestoque_codigo_seq", allocationSize=1)
public class SaldoEstoque implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="saldoestoque_codigo_seq")
	@Column(name = "id")
	private Long id;	
	private Long produto_id;	
	private int movimentoId;
	private String tipo;
	private Date data;
	private double qtde;
	
	@Transient
	private double total_compra;
	@Transient
	private double total_venda;
	@Transient
	private double saldo;
	
	public SaldoEstoque(Double total_venda, Double total_compra, Double saldo) {
		super();
		this.total_compra = total_compra;
		this.total_venda = total_venda;
		this.saldo = saldo;
	}
	
	public SaldoEstoque(){
		super();
	}
	
	public Long getProduto_id() {
		return produto_id;
	}
	public void setProduto_id(Long produto_id) {
		this.produto_id = produto_id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public double getQtde() {
		return qtde;
	}
	public void setQtde(double qtde) {
		this.qtde = qtde;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public double getTotal_compra() {
		return total_compra;
	}

	public void setTotal_compra(double total_compra) {
		this.total_compra = total_compra;
	}

	public double getTotal_venda() {
		return total_venda;
	}

	public void setTotal_venda(double total_venda) {
		this.total_venda = total_venda;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	
}
