package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="tipo_apartamento_codigo_seq", sequenceName="tipo_apartamento_codigo_seq", allocationSize=1)
public class TipoApartamento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6593342126120787628L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tipo_apartamento_codigo_seq")
	@Column(name="tipo_apartamento_id")
	private Long id;
	
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
