package br.com.cactus.flatsystem.entities;

public enum TipoTransacao {
	
	DESPESA("Despesa",0),
	RECEITA("Receita",1);
	
	private final String nome;
	private final int valor; 
	
	TipoTransacao(String nome, int valor){
		this.nome = nome;
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public int getValor() {
		return valor;
	}

}