package br.com.cactus.flatsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="transacao")
@SequenceGenerator(name="transacao_codigo_seq", sequenceName="transacao_codigo_seq", allocationSize=1)
public class Transacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="transacao_codigo_seq")
	@Column(name = "transacao_id")
	private Long id;	
	@Column(name="descricao", unique=true)
	private String descricao;	
	
	private TipoTransacao tipo;
	
	
	public TipoTransacao getTipo() {
		return tipo;
	}
	public void setTipo(TipoTransacao tipo) {
		this.tipo = tipo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao.toUpperCase();
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Transacao() {
		descricao = new String();
	}
	

}
