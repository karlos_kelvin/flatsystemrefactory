package br.com.cactus.flatsystem.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Venda extends Movimento implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8382091396833780607L;

	@ManyToOne
	@JoinColumn(name="cliente_id", nullable=true)
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="hospedagem_id", nullable=true)
	private Hospedagem hospedagem;
	
	public Venda() {		
		super();
	}
	
	public Venda(boolean semLancamento){
		super();
		if (semLancamento) {
			this.lancamento = null;
		}	
	}
	
	public Venda(Cliente cliente, Hospedagem hospedagem, Lancamento lancamento) {
		super();
		this.cliente = cliente;
		this.hospedagem = hospedagem;
		this.lancamento = lancamento;
	}
	
	public Venda(Cliente cliente, Hospedagem hospedagem) {
		super();
		this.cliente = cliente;
		this.hospedagem = hospedagem;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Hospedagem getHospedagem() {
		return hospedagem;
	}

	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}

}
