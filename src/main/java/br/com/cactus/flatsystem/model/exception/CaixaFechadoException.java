package br.com.cactus.flatsystem.model.exception;

/**
 * Exceção lançada quando alguma operação que necessita de um caixa aberto é excutada com caixa fechado.
 * @author Mário
 *
 */
public class CaixaFechadoException extends Exception {
	private final String MESSAGE = "Nenhum Caixa Aberto Atualmente!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
