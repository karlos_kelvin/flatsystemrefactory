package br.com.cactus.flatsystem.model.exception;

public class DiaDaSemanaException extends Exception {

	public DiaDaSemanaException() {
	}

	public DiaDaSemanaException(String message) {
		super(message);
	}

	public DiaDaSemanaException(Throwable cause) {
		super(cause);
	}

	public DiaDaSemanaException(String message, Throwable cause) {
		super(message, cause);
	}

	public DiaDaSemanaException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
