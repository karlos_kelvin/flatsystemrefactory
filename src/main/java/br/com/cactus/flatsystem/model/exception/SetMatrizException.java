package br.com.cactus.flatsystem.model.exception;

public class SetMatrizException extends Exception {

	public SetMatrizException(String mensagem) {
		super(mensagem);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7002916676455175821L;

}
