/*Insert role_Admin e role_User*/
INSERT INTO role(role_id, role_description)
    VALUES (1, 'ROLE_ADMIN');
COMMIT;

INSERT INTO role(role_id, role_description)
    VALUES (2, 'ROLE_USER');
COMMIT;

SELECT SETVAL('role_codigo_seq', (select max(ROLE_ID)+1 FROM ROLE), false);
COMMIT;

/* Insert Usuario ADMIN*/
INSERT INTO users(user_id, user_password, user_username)
    VALUES (1, 'admin', 'admin');
COMMIT;

SELECT SETVAL('user_codigo_seq', (select max(USER_ID)+1 FROM USERS), false);
COMMIT;

/*Insert User-Roles*/
INSERT INTO user_roles(user_id, role_id)
    VALUES (1, 1);
COMMIT;

INSERT INTO TRANSACAO VALUES(1,'SANGRIA',0);
COMMIT;

INSERT INTO TRANSACAO VALUES(2,'SUPRIMENTO',1);
COMMIT;

SELECT SETVAL('transacao_codigo_seq', (select max(TRANSACAO_ID)+1 FROM TRANSACAO), false);
COMMIT;

INSERT INTO FORMAS_PAGAMENTO VALUES(1,'DINHEIRO');
COMMIT;

SELECT SETVAL('formapag_codigo_seq', (select max(FPG_ID)+1 FROM FORMAS_PAGAMENTO), false);
COMMIT;