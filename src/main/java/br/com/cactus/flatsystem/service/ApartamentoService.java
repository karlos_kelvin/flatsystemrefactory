package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.ApartamentoDAO;
import br.com.cactus.flatsystem.entities.Apartamento;


public class ApartamentoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private ApartamentoDAO apartamentoDAO;
	
	
	@Inject
	public ApartamentoService(ApartamentoDAO apartamentoDAO) {
		this.apartamentoDAO = apartamentoDAO;
	}
	
	public void salvar(Apartamento apartamento){
		Long codigo = apartamento.getId();
		if (codigo == null || codigo == 0) {
			apartamentoDAO.save(apartamento);
		} else {
			apartamentoDAO.update(apartamento);
		}
	}
	
	public List<Apartamento> lista(){
		return this.apartamentoDAO.findAll();
	}
	
	public void update(Apartamento apartamento){
		apartamentoDAO.update(apartamento);
	}
	
	public void remover(Apartamento apartamento){
		apartamento = apartamentoDAO.getById(apartamento.getId());
		apartamentoDAO.remove(apartamento);
	}
	
}
