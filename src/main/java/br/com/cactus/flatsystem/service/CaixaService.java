package br.com.cactus.flatsystem.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.CaixaDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;


public class CaixaService {
	
	@Inject
	private CaixaDAO caixaDAO;
	
	public void salvar(Caixa caixa){
		if (caixa.getId() == null || caixa.getId() == 0) {
			caixaDAO.save(caixa);
		} else {
			caixaDAO.update(caixa);
		}
		
	}
	
	public void update(Caixa caixa){
		caixaDAO.update(caixa);
	}
	
	public void remover(Caixa caixa){
		caixaDAO.remove(caixa);
	}
	
	public List<Caixa> lista(){
		return caixaDAO.findAll();
	}
	
	public Caixa caixaAtual() throws CaixaFechadoException {
		return caixaDAO.getCaixaAtual();
	}
	
	public Caixa caixaPorData(Date data){
		return caixaDAO.getCaixaPorData(data);
	}
	
	public Caixa getById(Long id){
		return caixaDAO.getById(id);
	}
	
	public boolean temCaixaAberto(){
		boolean retorno = false;
		try{
			caixaDAO.getCaixaAtual();
			retorno = true;
		}catch(CaixaFechadoException e){
			
		}
		return retorno;
	}
		
	public List<Caixa> listaCaixasFechados(){
		List<Caixa> retorno = null;

		retorno = caixaDAO.getByClause(" WHERE c.dataFechamento != NULL" );
		
		return retorno;
	}
}
