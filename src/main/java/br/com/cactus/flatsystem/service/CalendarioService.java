package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.CalendarioDAO;
import br.com.cactus.flatsystem.entities.Hospedagem;


public class CalendarioService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private CalendarioDAO calendarioDAO;
	
	public void salvar(Hospedagem hospedagem){
		Long codigo = hospedagem.getId();
		if (codigo == null || codigo == 0) {
			calendarioDAO.save(hospedagem);
		} else {
			calendarioDAO.update(hospedagem);
		}
	}
	
	public List<Hospedagem> lista(){
		return calendarioDAO.findAll();
	}
	
	public List<Hospedagem> HospedagensAbertas(){
		return calendarioDAO.listaHospedagens("H");
	}
	
	public List<Hospedagem> PreReservasAbertas(){
		return calendarioDAO.listaHospedagens("P");
	}
	
	public List<Hospedagem> ReservasAbertas(){
		return calendarioDAO.listaHospedagens("R");
	}
	
	public void update(Hospedagem hospedagem){
		calendarioDAO.update(hospedagem);
	}
	
	public void remover(Hospedagem hospedagem){
		hospedagem = calendarioDAO.getById(hospedagem.getId());
		calendarioDAO.remove(hospedagem);
	}
	
	public Hospedagem findById(int hospedagemId) {
		return calendarioDAO.getById(Long.parseLong(hospedagemId+""));
	}
	
}
