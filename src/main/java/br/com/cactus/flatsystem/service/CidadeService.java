package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.CidadeDAO;
import br.com.cactus.flatsystem.entities.Cidade;
import br.com.cactus.flatsystem.entities.Estado;


public class CidadeService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CidadeDAO cidadeDAO; 
	
	
	public List<Cidade> buscaPorEstado(Estado estado){
		return cidadeDAO.consultaCidade(estado);
	}
	
	public Cidade getById(Long id){
		return cidadeDAO.getById(id);
	}
}
