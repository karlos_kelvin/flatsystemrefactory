package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.ClienteDAO;
import br.com.cactus.flatsystem.entities.Cliente;
import br.com.cactus.flatsystem.view.MessagesController;


public class ClienteService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ClienteDAO clienteDAO;
	
	private Cliente cliente;
	
	public void salvar(Cliente cliente){
	 if(cliente.getId() == null || cliente.getId() == 0){
		 clienteDAO.save(cliente);
	 }else {
		 clienteDAO.update(cliente);
	 }
		
	}
	
	public boolean verificaCPF(String cpf){
		boolean retorno = false;
		
		if (!cpf.equals("")){
			int numero = clienteDAO.getClientePorCPF(cpf);
			
			if (numero > 0){
				MessagesController.addWarn("CPF já cadastrado!", 
						"Já existe um cliente cadastrado com esse CPF!");
				retorno = true;
			}
		}
		return retorno;
	}
	
	public boolean verificaCPFUpdate(String cpf, Long id){
		boolean retorno = false;
		
		if (!cpf.equals("")){
			int numero = clienteDAO.getClientePorCPFUpdate(cpf, id);
			
			if (numero > 0){
				MessagesController.addWarn("CPF já Cadastrado!", "Já existe um cliente cadastrado com esse CPF!");
				retorno = true;
			}
		}
		return retorno;
	}
	
	public List<Cliente> lista(){
		return clienteDAO.findAll();
	}
	
	public void update(Cliente cliente){
		clienteDAO.update(cliente);
	}
	
	public void remover(Cliente cliente){
		cliente = clienteDAO.getById(cliente.getId());
		clienteDAO.remove(cliente);
	}

	public Cliente findById(Long id) {
		return clienteDAO.getById(id);
	}
}
