package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.CompraDAO;
import br.com.cactus.flatsystem.entities.Compra;


public class CompraService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5308079039788095983L;
	
	
	
	@Inject
	private CompraDAO compraDao;
	
	public void salvar(Compra compra){
		if(compra.getId() == null || compra.getId() == 0){
			compraDao.save(compra);
		}else{
			compraDao.update(compra);
		}
	}
	
	public List<Compra> lista(){
		return compraDao.findAll();
	}
	
	public void update(Compra compra){
		compraDao.update(compra);
	}
	
	public void remover(Compra compra){
		compraDao.remove(compra);
	}

}
