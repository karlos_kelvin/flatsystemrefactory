package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.ConfiguracaoDAO;
import br.com.cactus.flatsystem.entities.Configuracao;


public class ConfiguracaoService {
	
	@Inject
	private ConfiguracaoDAO configDAO;
	
	public void salvar(Configuracao configuracao){
		if (configuracao.getId() == 0){
			configDAO.save(configuracao);
		}else {
			configDAO.update(configuracao);
		}
	}
	
	public void update(Configuracao configuracao){
		configDAO.update(configuracao);
	}
	
	public List<Configuracao> lista(){
		return configDAO.findAll();
	}
	

}
