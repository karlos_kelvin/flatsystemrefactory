package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.EnderecoDAO;
import br.com.cactus.flatsystem.entities.Endereco;


public class EnderecoService {
	
        @Inject
        private EnderecoDAO enderecoDAO;

	public void salvar(Endereco endereco){
		if (endereco.getId() == null || endereco.getId() == 0){
			enderecoDAO.save(endereco);
		}else{
			enderecoDAO.update(endereco);
		}
	}
	
	public List<Endereco> lista(){
		return enderecoDAO.findAll();
	}
	
	public void update(Endereco endereco){
		enderecoDAO.update(endereco);
	}
	
	public void remover(Endereco endereco){
		endereco = enderecoDAO.getById(endereco.getId());
		enderecoDAO.remove(endereco);
	}
	
}
