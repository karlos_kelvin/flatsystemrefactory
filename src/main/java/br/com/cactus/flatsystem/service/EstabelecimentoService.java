package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.EstabelecimentoDAO;
import br.com.cactus.flatsystem.entities.Estabelecimento;


public class EstabelecimentoService {
	
        @Inject
	private EstabelecimentoDAO estabelecimentoDAO;
	
	public void salvar(Estabelecimento estabelecimento){
		if (estabelecimento.getId() == null || estabelecimento.getId() == 0){
			estabelecimentoDAO.salvar(estabelecimento);
		}else{
			estabelecimentoDAO.update(estabelecimento);
		}
	}
	
	public List<Estabelecimento> lista(){
		return estabelecimentoDAO.findAll();
	}
	
	public void update(Estabelecimento estabelecimento){
		estabelecimentoDAO.update(estabelecimento);
	}
	
	public void remover(Estabelecimento estabelecimento){
		estabelecimento = estabelecimentoDAO.getById(estabelecimento.getId());
		estabelecimentoDAO.remove(estabelecimento);
	}

}
