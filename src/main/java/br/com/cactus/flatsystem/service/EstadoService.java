package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.EstadoDAO;
import br.com.cactus.flatsystem.entities.Estado;


public class EstadoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
        @Inject
	private EstadoDAO estadoDAO;
	
	public List<Estado> lista(){
		return estadoDAO.findAll();
	}
	
	public Estado getById(Long id){
		return estadoDAO.getById(id);
	}
	
	public Estado getBySigla(String sigla){
		return estadoDAO.getBySigla(sigla);
	}
}
