package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.dao.FormasPagamentoDAO;


public class FormasPagamentoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

        @Inject
        private FormasPagamentoDAO formas_pagamentoDAO;
	
	public void salvar(FormasPagamento formas_pagamento){
		Long codigo = formas_pagamento.getId();
		if (codigo == null || codigo == 0) {
			formas_pagamentoDAO.save(formas_pagamento);
		} else {
			formas_pagamentoDAO.update(formas_pagamento);
		}
	}
	
	public List<FormasPagamento> lista(){
		return formas_pagamentoDAO.findAll();
	}
	
	public void update(FormasPagamento formas_pagamento){
		formas_pagamentoDAO.update(formas_pagamento);
	}
	
	public void remover(FormasPagamento formas_pagamento){
		formas_pagamento = formas_pagamentoDAO.getById(formas_pagamento.getId());
		formas_pagamentoDAO.remove(formas_pagamento);
	}

	public FormasPagamento findById(int pagamentoId) {
		return formas_pagamentoDAO.getById(Long.parseLong(pagamentoId+""));
	}
	
}
