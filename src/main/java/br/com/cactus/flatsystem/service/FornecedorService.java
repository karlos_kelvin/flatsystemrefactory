package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.FornecedorDAO;
import br.com.cactus.flatsystem.entities.Fornecedor;


public class FornecedorService {
	
        @Inject
	private FornecedorDAO fornecedorDAO;
	
	public void salvar(Fornecedor fornecedor){
		if (fornecedor.getId() == null || fornecedor.getId() == 0){
			fornecedorDAO.save(fornecedor);
		}else{
			fornecedorDAO.update(fornecedor);
		}
	}
	
	public List<Fornecedor> lista(){
		return fornecedorDAO.findAll();
	}
	
	public void update(Fornecedor fornecedor){
		fornecedorDAO.update(fornecedor);
	}
	
	public void remover(Fornecedor fornecedor){
		fornecedor = fornecedorDAO.getById(fornecedor.getId());
		fornecedorDAO.remove(fornecedor);
	}

}
