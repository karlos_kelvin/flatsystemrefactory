package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.ClienteDAO;
import br.com.cactus.flatsystem.dao.HospedagemDAO;
import br.com.cactus.flatsystem.entities.Hospedagem;


public class HospedagemService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

        @Inject
        private HospedagemDAO hospedagemDAO;
	
	@Inject
	private ClienteDAO clienteDAO;
	
	public void salvar(Hospedagem hospedagem){
		Long codigo = hospedagem.getId();
		
		if (codigo == null || codigo == 0) {
			hospedagemDAO.save(hospedagem);
		} else {
			hospedagemDAO.update(hospedagem);
		}
	}
	
	public List<Hospedagem> lista(){
		return hospedagemDAO.findAll();
	}
	
	public void update(Hospedagem hospedagem){
		hospedagemDAO.update(hospedagem);
	}
	
	public void remover(Hospedagem hospedagem){
		hospedagem = hospedagemDAO.getById(hospedagem.getId());
		hospedagemDAO.remove(hospedagem);
	}
	
	public Hospedagem findById(Hospedagem hospedagem) {
		return hospedagemDAO.getById(hospedagem.getId());
	}

	public double totalHospedagensAbertas() {
		double retorno = 0;
		List<Hospedagem> lista = hospedagemDAO.hospedagensAbertas();
		retorno = lista.size();
		return retorno;
	}

	public int getTotalHospedes() {
		int retorno = 0;
		List<Hospedagem> abertas = hospedagemDAO.hospedagensAbertas();
		for (Hospedagem hospedagem : abertas) {
			retorno += hospedagem.totalDeHospedes();
		}
		return retorno;
	}

	public double totalPreReservas() {
		double retorno = 0;
		List<Hospedagem> lista = hospedagemDAO.getPreReservas();
		retorno = lista.size();
		return retorno;
	}

	public double totalReservas() {
		double retorno = 0;
		List<Hospedagem> lista = hospedagemDAO.getReservas();
		retorno = lista.size();
		return retorno;
	}
	
	public List<Hospedagem> filtro(Hospedagem hospedagem) {
		return hospedagemDAO.filtro(hospedagem);
	}

	public ClienteDAO getClienteDAO() {
		return clienteDAO;
	}

	public void setClienteDAO(ClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}
	
}
