package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.ItemVendaDAO;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Venda;


public class ItemVendaService {

	@Inject
	private ItemVendaDAO itemVendaDAO;

	public void salvar(ItemMovimento itemMovimento){
		if (itemMovimento.getId() == null || itemMovimento.getId() == 0){
			itemVendaDAO.save(itemMovimento);
		}else{
			itemVendaDAO.update(itemMovimento);
		}
	}
	
	public List<ItemMovimento> lista(){
		return itemVendaDAO.findAll();
	}
	
	public List<ItemMovimento> listaItemHospedagem(Venda venda){
		return itemVendaDAO.findByVenda(venda);
	}
	
	public void update(ItemMovimento itemMovimento){
		itemVendaDAO.update(itemMovimento);
	}
	
	public void remover(ItemMovimento itemMovimento){
		itemMovimento = itemVendaDAO.getById(itemMovimento.getId());
		itemVendaDAO.remove(itemMovimento);
	}

	public List<String> getClientes(String query) {
		return null;
	}
	
}
