package br.com.cactus.flatsystem.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.CaixaDAO;
import br.com.cactus.flatsystem.dao.LancamentoDAO;
import br.com.cactus.flatsystem.dao.TransacaoDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.FormasPagamento;
import br.com.cactus.flatsystem.entities.Lancamento;
import br.com.cactus.flatsystem.entities.TipoTransacao;
import br.com.cactus.flatsystem.entities.Transacao;
import br.com.cactus.flatsystem.model.exception.CaixaFechadoException;
import br.com.cactus.flatsystem.dao.FormasPagamentoDAO;


public class LancamentoService {

	@Inject
	private LancamentoDAO lancamentoDAO;
	
	@Inject
	private CaixaDAO caixaDAO;
	
	@Inject
	private TransacaoDAO transacaoDAO;
	
	@Inject
	private FormasPagamentoDAO formasPagamentoDAO;
	
	public void salvar(Lancamento lancamento){
		lancamentoDAO.save(lancamento);
	}
	
	public void update(Lancamento lancamento){
		lancamentoDAO.update(lancamento);
	}
	
	public void remover(Lancamento lancamento){
		lancamento = lancamentoDAO.getById(lancamento.getId());
		lancamentoDAO.remove(lancamento);
	}
	
	public List<Lancamento> lista(){
		return lancamentoDAO.finAll();
	}
	
	/**
	 * 
	 * @return retorna o somat�rio dos lan�amentos por transa��o 
	 */
	public List<Lancamento> getLancamentoTransacoes(Caixa caixa){
		return lancamentoDAO.getLancamentoTransacoes(caixa);
	}
	
	public Lancamento findByID(Lancamento lancamento){
		return lancamentoDAO.getById(lancamento.getId());
	}
	
	/**
	 * 
	 * @return retorna todos o lan�amentos de uma transa��o 
	 */
	public List<Lancamento> getLancamentoTransacoes(Caixa caixa, Transacao transacao){
		return lancamentoDAO.getLancamentosPorTransacao(caixa, transacao);
	}
	
	/**
	 * Retorna total dos Lan�amentos cuja transa��o possui {@link TipoTransacao}  com valor 1
	 * @param caixa
	 * @return
	 * @see Transacao
	 * @see TipoTransacao
	 */
	public double getLancamentosDeDespesa(Caixa caixa){
		double retorno = 0;
		List<Lancamento> lancamentos = lancamentoDAO.getLancamentosDeDespesa(caixa);
		
		for (Lancamento lancamento : lancamentos) {
			retorno += lancamento.getValor();
		}
		
		return retorno;
	}
	
	/**
	 * Retorna total dos Lan�amentos cuja transa��o possui {@link TipoTransacao}  com valor 0
	 * @param caixa
	 * @return
	 * @see Transacao
	 * @see TipoTransacao
	 */
	public double getLancamentosDeReceita(Caixa caixa){
		double retorno = 0;
		List<Lancamento> lancamentos = lancamentoDAO.getLancamentosDeReceita(caixa);
		
		for (Lancamento lancamento : lancamentos) {
			retorno += lancamento.getValor();
		}
		
		return retorno;
	}
	
	public List<Lancamento> getLancamentoFormasPagamento(Caixa caixa){
		return lancamentoDAO.getLancamentoFormasPagamento(caixa);
	}
	
	public List<Lancamento> getLancamentosCaixa(Caixa caixa){
		return lancamentoDAO.getLancamentosCaixa(caixa);
	}
	
	public boolean criaSangria(double valor, String obs) throws CaixaFechadoException{
		boolean retorno = true;
		Caixa caixaAtual = caixaDAO.getCaixaAtual();
		Transacao sangria = transacaoDAO.getById( new Long(1) );
		FormasPagamento fp = formasPagamentoDAO.getById( new Long(1) );
		if (caixaAtual != null) {
			Lancamento l = new Lancamento();
			l.setCaixa(caixaAtual);
			l.setTransacao(sangria);
			l.setValor(valor);
			l.setObservacao(obs);
			l.setFormaPagamento(fp);
			l.setConfirmado(true);
			l.setData(new Date());
			salvar(l);
		}else{
			throw new CaixaFechadoException();
		}
		
		return retorno;
	}
	
	public boolean criaSuprimento(double valor, String obs) throws CaixaFechadoException{
		boolean retorno = true;
		Caixa caixaAtual = caixaDAO.getCaixaAtual();		
		Transacao suprimento = transacaoDAO.getById(new Long(2));
		FormasPagamento fp = formasPagamentoDAO.getById( new Long(1) );
		
		if (caixaAtual != null) {
			Lancamento l = new Lancamento();
			l.setCaixa(caixaAtual);
			l.setTransacao(suprimento);
			l.setValor(valor);
			l.setObservacao(obs);
			l.setFormaPagamento(fp);
			l.setConfirmado(true);
			l.setData(new Date());
			salvar(l);
		}else{
			throw new CaixaFechadoException();
		}
		
		return retorno;
	}

	public List<Lancamento> getTodosLancamentosPorFormaDePagamento(Caixa caixa,	FormasPagamento formaPagamento) {
		return lancamentoDAO.getLancamentosPorFormaDePagamento(caixa, formaPagamento);
	}
}
