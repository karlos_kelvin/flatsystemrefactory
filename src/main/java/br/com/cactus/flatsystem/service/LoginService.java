package br.com.cactus.flatsystem.service;

import br.com.cactus.flatsystem.entities.User;

public interface LoginService {
	User login(String username, String password) throws IllegalArgumentException;
}
