package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.HospedagemDAO;
import br.com.cactus.flatsystem.entities.Apartamento;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.util.DiaDaSemana;
import br.com.cactus.flatsystem.util.MapaSemanal;


public class MapaService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2647901359488898855L;

	@Inject
	private HospedagemDAO hospedagemDAO;
	
	private List<Apartamento> apartamentos;
	private DiaDaSemana dias[];
	
	private List<MapaSemanal> mapaSemanal;

	public List<Hospedagem> getHospedagens(int apartamentoId){
		Date dataInicial = dias[0].getData(); 
		Date dataFinal = dias[6].getData();
		
		return hospedagemDAO.getHospedagensPeriodo(dataInicial, dataFinal, apartamentoId);
	}

	/*
	 * Metodo que monta a DataTable com os dias da semana corrente e a lista de apartamento com os dias se estão ocupados ou não
	 */
	public void montaDias() {
		List<Hospedagem> hospedagens = null;
		mapaSemanal = new ArrayList<>();
		/*
		 * La�o para percorrer os apartamentos cadastrados
		 */
		for (Apartamento a : apartamentos) {
			hospedagens = getHospedagens( new Long( a.getId()).intValue()  );
			MapaSemanal mp = new MapaSemanal();
			mp.setApartamento_id(a.getNumero_apartamento());
			/*
			 * laco que percorre os dias da semana em questão
			 */
			for (int i = 0; i < dias.length; i++) {	
				Date data = dias[i].getData();
				mp.dias[i] = " ";
				/*
				 * laço que percorre as hospedagens que existam pro apartamento em questão
				 */
				for (Hospedagem h : hospedagens) {
					
					/*
					 * testa se o dia esta dentro do intervalo datacheckin e datacheckout dessa hospedagem
					 */
					if (dataNoIntervalo(data,h.getDataPrevCheckin(),h.getDataPrevCheckout())) {
						
						SimpleDateFormat formatador = new SimpleDateFormat("yyyy/MM/dd");
						Date dataCheckin = new Date(formatador.format(h.getDataPrevCheckin()));
						Date dataCheckout = new Date(formatador.format(h.getDataPrevCheckout()));
						
						/*
						 * teste se o dia em questão é igual a data de checkout ou checkin para mostra a hora de checkout e a hora de checkin
						 */
						if(data.equals(dataCheckout) || data.equals(dataCheckin)){
							
							//SimpleDateFormat spf = new SimpleDateFormat("HH:mm");
							
							if (data.equals(dataCheckout)){
								  //String hora = spf.format(h.getDataPrevCheckout());
								  mp.dias[i] = mp.dias[i] + " Saida: " + String.valueOf(h.getHoraCheckout());
						    }	
							
							if (data.equals(dataCheckin)){
								  //String hora = spf.format(h.getDataPrevCheckin());
								  mp.dias[i] = mp.dias[i] +  " Entrada: " + String.valueOf(h.getHoraCheckin());
							}
						   
						}else{
							mp.dias[i] = mp.dias[i] + "X";
						}
						  
						
					}else{
						mp.dias[i] = mp.dias[i] +  "";
					}
				}
				
			}
			/*
			 * adiciona um objeto de mapa semanal a lista de MapaSemanal
			 */
			mapaSemanal.add(mp);
		}

	}
	
	public List<MapaSemanal> getMapaSemanal() {
		return mapaSemanal;
	}
	
	public void setApartamentos(List<Apartamento> apartamentos) {
		this.apartamentos = apartamentos;
	}

	public void setDias(DiaDaSemana[] dias) {
		this.dias = dias;
	}

	private boolean dataNoIntervalo(Date data, Date dataPrevCheckin,Date dataPrevCheckout) {
		boolean retorno = false;
		SimpleDateFormat spf = new SimpleDateFormat("yyyy/MM/dd");
		
		Date dataIni = new Date(spf.format(dataPrevCheckin));
		Date dataFin = new Date(spf.format(dataPrevCheckout));
		
		if ( (data.after(dataIni) || data.equals(dataIni)) && (data.before(dataFin) || data.equals(dataFin)) ) {
			retorno = true;
		}
		
		return retorno;
	}

	/** 
     * Calcula a diferença de duas datas em dias 
     * <br> 
     * <b>Importante:</b> Quando realiza a diferença em dias entre duas datas, este método considera as horas restantes e as converte em fração de dias. 
     * @param dataInicial 
     * @param dataFinal 
     * @return quantidade de dias existentes entre a dataInicial e dataFinal. 
     */  
    public static double diferencaEmDias(Date dataInicial, Date dataFinal){  
        double result = 0;  
        long diferenca = dataFinal.getTime() - dataInicial.getTime();  
        double diferencaEmDias = (diferenca /1000) / 60 / 60 /24; //resultado é a diferença entre as datas em dias  
        long horasRestantes = (diferenca /1000) / 60 / 60 %24; //calcula as horas restantes  
        result = diferencaEmDias + (horasRestantes /24d); //transforma as horas restantes em fração de dias  
      
        return result;  
    }  
      
    /** 
     * Calcula a diferençaa de duas datas em horas 
     * <br> 
     * <b>Importante:</b> Quando realiza a diferença em horas entre duas datas, este método considera os minutos restantes e os converte em fraçao de horas. 
     * @param dataInicial 
     * @param dataFinal 
     * @return quantidade de horas existentes entre a dataInicial e dataFinal. 
     */  
    public static double diferencaEmHoras(Date dataInicial, Date dataFinal){  
        double result = 0;  
        long diferenca = dataFinal.getTime() - dataInicial.getTime();  
        long diferencaEmHoras = (diferenca /1000) / 60 / 60;  
        long minutosRestantes = (diferenca / 1000)/60 %60;  
        double horasRestantes = minutosRestantes / 60d;  
        result = diferencaEmHoras + (horasRestantes);  
          
        return result;  
    }  
      
    /** 
     * Calcula a diferença de duas datas em minutos 
     * <br> 
     * <b>Importante:</b> Quando realiza a diferença em minutos entre duas datas, este método considera os segundos restantes e os converte em fração de minutos. 
     * @param dataInicial 
     * @param dataFinal 
     * @return quantidade de minutos existentes entre a dataInicial e dataFinal. 
     */  
    public static double diferencaEmMinutos(Date dataInicial, Date dataFinal){  
        double result = 0;  
        long diferenca = dataFinal.getTime() - dataInicial.getTime();  
        double diferencaEmMinutos = (diferenca /1000) / 60; //resultado é a diferença entre as datas em minutos  
        long segundosRestantes = (diferenca / 1000)%60; //calcula os segundos restantes  
        result = diferencaEmMinutos + (segundosRestantes /60d); //transforma os segundos restantes em minutos  
      
        return result;  
    }  

}
