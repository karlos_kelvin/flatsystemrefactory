package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;


import br.com.cactus.flatsystem.dao.ProdutoDAO;
import br.com.cactus.flatsystem.entities.ItemMovimento;
import br.com.cactus.flatsystem.entities.Produto;
import javax.inject.Inject;

public class ProdutoService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ProdutoDAO produtoDAO;
	
	@Inject
	private SaldoEstoqueService saldoService;
	
	public void salvar(Produto produto){
		if (produto.getId() == null || produto.getId() == 0){
			produtoDAO.save(produto);
		}else{
			produtoDAO.update(produto);
		}
	}
	
	public List<Produto> lista(){
		return produtoDAO.findAll();
	}
	
	public void update(Produto produto){
		produtoDAO.update(produto);
	}
	
	public void remover(Produto produto){
		produto = produtoDAO.getById(produto.getId());
		produtoDAO.remove(produto);
	}
	
	public Produto findById(int id){
		return this.produtoDAO.getById(Long.parseLong(id+""));
	}

	public boolean temEstoqueSuficiente(ItemMovimento i) {
		boolean retorno = false;
		List<Number> saldo = saldoService.getSaldoEstoque(i.getProduto());
		
		if (saldo.size() > 0){
			if (saldo.get(0).doubleValue() >= i.getQuantidade()){
				retorno = true;
			}
		}
		return retorno;
	}
}
