package br.com.cactus.flatsystem.service;

import java.util.HashMap;

import javax.faces.context.FacesContext;

import org.primefaces.model.StreamedContent;

import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.util.RelatorioUtil;


public class RelatorioService {
	
	private FacesContext context;
	private RelatorioUtil relatorioUtil = new RelatorioUtil();
	
	public RelatorioService(FacesContext context) {
		this.context = context;
	}

	public StreamedContent getRelatorioCaixa(Caixa caixa){
		StreamedContent arquivoRetorno = null;

		HashMap<String, Long> paramentroRelatorio = new HashMap<String, Long>();
		paramentroRelatorio.put("CaixaID", caixa.getId());

		try {
			arquivoRetorno = relatorioUtil.geraRelatorio(paramentroRelatorio,
					"ResumoCaixa",
					"ResumoCaixa - Nº " + String.valueOf(caixa.getId()), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return arquivoRetorno;
	}
	
}
