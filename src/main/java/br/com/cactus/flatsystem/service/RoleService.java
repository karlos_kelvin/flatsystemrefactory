package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.RoleDAO;
import br.com.cactus.flatsystem.entities.Role;


public class RoleService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private RoleDAO roleDAO;
	
	public void salvar(Role role){
		Long codigo = role.getId();
		if (codigo == null || codigo == 0) {
			roleDAO.save(role);
		} else {
			roleDAO.update(role);
		}
	}
	
	public List<Role> lista(){
		return roleDAO.findAll();
	}
	
	public void update(Role role){
		roleDAO.update(role);
	}
	
	public void remover(Role role){
		role = roleDAO.getById(role.getId());
		roleDAO.remove(role);
	}
	
}
