package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.Produto;
import br.com.cactus.flatsystem.entities.SaldoEstoque;
import br.com.cactus.flatsystem.dao.SaldoEstoqueDAO;


public class SaldoEstoqueService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private SaldoEstoqueDAO saldo_estoqueDAO;
	
	public void salvar(SaldoEstoque saldo_estoque){
		Long codigo = saldo_estoque.getId();
		if (codigo == null || codigo == 0) {
			saldo_estoqueDAO.save(saldo_estoque);
		} else {
			saldo_estoqueDAO.update(saldo_estoque);
		}
	}
	
	public List<SaldoEstoque> lista(){
		return saldo_estoqueDAO.findAll();
	}
	
	public List<Number> getSaldoEstoque(Produto produto){
		return saldo_estoqueDAO.getSaldo(produto);
	}
	
	
	public void update(SaldoEstoque saldo_estoque){
		saldo_estoqueDAO.update(saldo_estoque);
	}
	
	public void remover(SaldoEstoque saldo_estoque){
		saldo_estoque = saldo_estoqueDAO.getById(saldo_estoque.getId());
		saldo_estoqueDAO.remove(saldo_estoque);
	}
	
	public Number totalCompra(Produto produto){
		return saldo_estoqueDAO.getTotalCompra(produto);
	}
	
	public Number totalVenda(Produto produto){
		return saldo_estoqueDAO.getTotalVenda(produto);
	}
	
}
