package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.entities.TipoApartamento;
import br.com.cactus.flatsystem.dao.TipoApartamentoDAO;


public class TipoApartamentoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6085850132379426055L;

	private TipoApartamentoDAO tipo_apartamentoDAO;
	
	@Inject
	public TipoApartamentoService(TipoApartamentoDAO tipo_apartamentoDAO) {
		this.tipo_apartamentoDAO = tipo_apartamentoDAO;
	}
	
	public void salvar(TipoApartamento tipo_apartamento){
		Long codigo = tipo_apartamento.getId();
		if (codigo == null || codigo == 0){
			tipo_apartamentoDAO.save(tipo_apartamento);
		} else {
			tipo_apartamentoDAO.update(tipo_apartamento);
		}
	}
	
	public List<TipoApartamento> lista(){
		return this.tipo_apartamentoDAO.findAll();
	}
	
	public void update(TipoApartamento tipo_apartamento){
		tipo_apartamentoDAO.update(tipo_apartamento);
	}
	
	public void remover(TipoApartamento tipo_apartamento){
		tipo_apartamento = tipo_apartamentoDAO.getById(tipo_apartamento.getId());
		tipo_apartamentoDAO.remove(tipo_apartamento);
	}
	
}
