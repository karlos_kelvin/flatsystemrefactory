package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.TransacaoDAO;
import br.com.cactus.flatsystem.entities.Transacao;


public class TransacaoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private TransacaoDAO transacaoDAO;
	
	public void salvar(Transacao transacao){
		Long codigo = transacao.getId();
		if (codigo == null || codigo == 0) {
			transacaoDAO.save(transacao);
		} else {
			transacaoDAO.update(transacao);
		}
	}
	
	public List<Transacao> lista(){
		return transacaoDAO.findAll();
	}
	
	public List<Transacao> listaSemSangriaSuprimento(){
		return transacaoDAO.findSemSangriaSuprimento();
	}
	
	public void update(Transacao transacao){
		transacaoDAO.update(transacao);
	}
	
	public void remover(Transacao transacao){
		transacao = transacaoDAO.getById(transacao.getId());
		transacaoDAO.remove(transacao);
	}

	public Transacao findById(int transacaoid) {
		return transacaoDAO.getById(Long.parseLong(transacaoid+""));
	}
	
	
	/**
	 * Método para retornar todas as transações cadastradas exceto as padrões SANGRIA E SUPRIMENTO e Só Receitas
	 * @return
	 */
	public List<Transacao> transacoesDeReceita(){
		List<Transacao> retorno = new ArrayList<Transacao>();
		retorno = transacaoDAO.getTransacoesDeReceita();
		return retorno;
	}
	
}
