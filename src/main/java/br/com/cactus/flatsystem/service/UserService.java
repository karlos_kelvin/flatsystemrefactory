package br.com.cactus.flatsystem.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.UserDAO;
import br.com.cactus.flatsystem.entities.User;


public class UserService implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserDAO userDAO;
	
	public void salvar(User user) {
		Long codigo = user.getId();
		if (codigo == null || codigo == 0) {
			userDAO.save(user);
		}else{
			userDAO.update(user);
		}
	}
	
	public List<User> lista(){
		return userDAO.findAll();
	}
	
	public void atualizar(User user) {
		userDAO.update(user);
	}
	
	public void remover(User user) {
		user = userDAO.getById(user.getId());
		userDAO.remove(user);
	}

}
