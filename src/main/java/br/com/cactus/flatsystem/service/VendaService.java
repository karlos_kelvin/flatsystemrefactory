package br.com.cactus.flatsystem.service;

import java.util.List;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.VendaDAO;
import br.com.cactus.flatsystem.entities.Caixa;
import br.com.cactus.flatsystem.entities.Hospedagem;
import br.com.cactus.flatsystem.entities.Venda;


public class VendaService {

	@Inject
	private VendaDAO vendaDAO;

	public void salvar(Venda venda){
		if (venda.getId() == null || venda.getId() == 0){
			vendaDAO.save(venda);
		}else{
			vendaDAO.update(venda);
		}
	}
	
	public List<Venda> lista(){
		return vendaDAO.findAll();
	}
	
	public List<Venda> listaVendasHospedagem(Hospedagem hospedagem){
		return vendaDAO.findByHospedagem(hospedagem);
	}
	
	public void update(Venda venda){
		vendaDAO.update(venda);
	}
	
	public void remover(Venda venda){
		//venda = vendaDAO.getById(venda.getId());
		vendaDAO.remove(venda);
	}

	public List<String> getClientes(String query) {
		return null;
	}

	public List<Venda> getVendaPorCaixa(Caixa caixa) {
		return vendaDAO.getVendaPorCaixa(caixa);
	}
	
}
