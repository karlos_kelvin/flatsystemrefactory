package br.com.cactus.flatsystem.service.impl;

import javax.inject.Inject;

import br.com.cactus.flatsystem.dao.UserDAO;
import br.com.cactus.flatsystem.entities.User;
import br.com.cactus.flatsystem.service.LoginService;


public class LoginServiceImpl implements LoginService {
    
	@Inject
	private UserDAO dao;

	@Override
	public User login(String username, String password)
			throws IllegalArgumentException {
		if (isEmptyOrNull(username) || isEmptyOrNull(password)) {
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Sample error message", "PrimeFaces makes no mistakes"));
			throw new IllegalArgumentException(
					"Atenção, Usuário ou senha vazios!");
		}
		User u = dao.login(username, password);
		
		if (u == null) {
			throw new IllegalArgumentException(
					"Erro: Usuario ou Senha incorretos!");
		}
		return u;
	}

	private boolean isEmptyOrNull(String s) {
		return s == null || s.equals("");
	}
}
