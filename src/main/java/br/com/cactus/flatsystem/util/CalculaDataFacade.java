package br.com.cactus.flatsystem.util;

import java.util.Date;

public class CalculaDataFacade {
	/**
	 * Calcula a diferença de duas datas em dias <br>
	 * <b>Importante:</b>
	 * 
	 * @param dataInicial
	 * @param dataFinal
	 * @return quantidade de dias existentes entre a dataInicial e dataFinal.
	 */
	public static int diferencaEmDias(Date dataInicial, Date dataFinal) {
		double result = 0;
		long diferenca = dataFinal.getTime() - dataInicial.getTime();

		double diferencaEmDias = (diferenca / 1000) / 60 / 60 / 24;

		result = diferencaEmDias;

		Double retorno = result;

		//hospedagem.setQtdDiarias(retorno.intValue());

		return retorno.intValue();
	}
}
