package br.com.cactus.flatsystem.util;

import java.util.List;

import org.primefaces.model.LazyDataModel;

import br.com.cactus.flatsystem.entities.Cliente;
import br.com.cactus.flatsystem.service.ClienteService;
import javax.inject.Inject;

public class ClienteLazyList extends LazyDataModel<Cliente>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8988296132483798751L;
	
	private List<Cliente> clientes;
	
	@Inject
	private ClienteService clienteService;
	/*
	public List<Cliente> load(int posicaoPrimeiraLinha, int maximoPorPagina, String ordenarPeloCampo, SortOrder ordernarAscOuDesc, Map<String, String> filtros){
		
		String ordenacao = ordernarAscOuDesc.toString();
		
		if(SortOrder.UNSORTED.equals(ordernarAscOuDesc)){
			ordenacao = SortOrder.ASCENDING.toString();
		}
		
		//clientes = clienteService.buscaPorPaginacao(posicaoPrimeiraLinha, maximoPorPagina);
		
		if (getRowCount() <= 0){
			//setRowCount(clienteService.countAll());
		}
		
		setPageSize(maximoPorPagina);
		
		return clientes;
	}*/
	
	public Cliente getRowData(String rowKey){
		
		for (Cliente cliente : clientes){
			if (rowKey.equals(String.valueOf(cliente.getId())))
				return cliente;
		}
		
		return null;
	}
	
	public Object getRowKey(Cliente cliente){
		return cliente.getId();
	}
	
	public void SetRowIndex(int rowIndex){
		//solução para evitar ArithmeticException
		if (rowIndex == -1 || getPageSize() == 0){
			super.setRowIndex(-1);
		} else{
			super.setRowIndex(rowIndex % getPageSize());
		}
	}

}
