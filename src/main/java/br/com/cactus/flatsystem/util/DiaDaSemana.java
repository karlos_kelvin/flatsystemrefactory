package br.com.cactus.flatsystem.util;

import java.util.Date;

import br.com.cactus.flatsystem.model.exception.DiaDaSemanaException;

public class DiaDaSemana {
	private String nome;
	private int diaSemana;
	private int dd;
	private int mm;
	private int yyyy;
	
	/**
	 * 
	 * @param  diaSemana - 1 : Domingo, 2: Segunda etc
	 * @param  dd - dia da data 
	 * @param  mm - mês da data
	 * @param  yyyy - ano da data
	 * @throws DiaDaSemanaException - quando o dia semana não for encotrado em um Enum Correspondente
     * @see    java.util.Calendar
     * @see    br.com.cactus.flatsystem.util.DiaDaSemanaEnum
	 */
	public DiaDaSemana(int diaSemana, int dd, int mm, int yyyy) throws DiaDaSemanaException {
		super();
		this.diaSemana = diaSemana;
		this.dd = dd;
		this.mm = mm;
		this.yyyy = yyyy;
		this.nome = setNome(diaSemana);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(int diaSemana) {
		this.diaSemana = diaSemana;
	}

	public int getDd() {
		return dd;
	}

	public void setDd(int dd) {
		this.dd = dd;
	}

	public int getMm() {
		return mm;
	}

	public void setMm(int mm) {
		this.mm = mm;
	}

	public int getYyyy() {
		return yyyy;
	}

	public void setYyyy(int yyyy) {
		this.yyyy = yyyy;
	}

	public DiaDaSemana(DiaDaSemanaEnum diaEnum, short dd, short mm, short yyyy) throws DiaDaSemanaException {
		super();
		this.diaSemana = diaEnum.getValor();
		this.dd = dd;
		this.mm = mm;
		this.yyyy = yyyy;
		this.nome = diaEnum.getNome();
	}
	
	private String setNome(int diaSemana) throws DiaDaSemanaException {
		String retorno = null;
		for (DiaDaSemanaEnum d : DiaDaSemanaEnum.values()) {
			if (d.getValor() == diaSemana) {
				retorno = d.getNome();
				break;
			}
		}
		if(retorno == null) throw new DiaDaSemanaException("Nenhuma semana foi encontrada para o valor: "+diaSemana);
		return retorno;
	}

	@Override
	public String toString() {
		return  nome + "\n" + dd + "/" + mm + "/" + yyyy ;
	}

	public Date getData() {		
		return new Date(yyyy + "/" + mm + "/" + dd);
	}
	
	
	
}
