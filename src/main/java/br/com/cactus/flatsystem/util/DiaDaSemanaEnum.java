package br.com.mkl.util;

public enum DiaDaSemanaEnum {
	DOMINGO(1,"Domingo\n"),
	SEGUNDA(2,"Segunda\n"),
	TERCA  (3,"Terça\n"),
	QUARTA (4,"Quarta\n"),
	QUINTA (5,"Quinta\n"),
	SEXTA  (6,"Sexta\n"),
	SABADO (7,"Sábado\n");
	
	private final int valor;
	private final String nome;
	
	private DiaDaSemanaEnum(int valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	public int getValor() {
		return valor;
	}
	public String getNome() {
		return nome;
	}
	
	
}
