package br.com.cactus.flatsystem.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.cactus.flatsystem.entities.Configuracao;
import br.com.cactus.flatsystem.entities.Hospedagem;

public class DiariasFacade {
	/**
	 * Retorna quantas diárias devem ser pagas de acordo com a  {@link Configuracao} atual 
	 * 
	 * @author Karlos Kelvin
	 * @return Total de diárias
	 */
	public static double getTotalDiariasHospedagem(Configuracao config, Hospedagem hospedagem) {
		double QtdDiarias = CalculaDataFacade.diferencaEmDias(hospedagem.getDataPrevCheckin(), hospedagem.getDataPrevCheckout());

		GregorianCalendar resultado = new GregorianCalendar();
		resultado.setTimeInMillis(config.getHoraInicioHospedagem().getTime());

		resultado.add(Calendar.HOUR, config.getToleranciaHospedagem()
				.getHours());
		resultado.add(Calendar.MINUTE, config.getToleranciaHospedagem()
				.getMinutes());

		Date soma = new Date();
		soma.setTime(resultado.getTimeInMillis());

		resultado = new GregorianCalendar();
		resultado.setTimeInMillis(config.getHoraInicioHospedagem().getTime());

		resultado.add(Calendar.HOUR, -config.getToleranciaHospedagem()
				.getHours());
		resultado.add(Calendar.MINUTE, -config.getToleranciaHospedagem()
				.getMinutes());

		Date sub = new Date();
		sub.setTime(resultado.getTimeInMillis());

		if (config.isCalculaMeiaHospedagem()) {
			if ((hospedagem.getHoraCheckin().after(sub) || hospedagem.getHoraCheckin().equals(sub))
					&& (hospedagem.getHoraCheckout().before(soma) || hospedagem.getHoraCheckout().equals(soma))) {
				return QtdDiarias;
			} else {
				if (hospedagem.getHoraCheckin().before(sub)) {
					QtdDiarias += 0.5;
				}

				if (hospedagem.getHoraCheckout().after(soma)) {
					QtdDiarias += 0.5;
				}

				return QtdDiarias;
			}
		} else {
			if ((hospedagem.getHoraCheckin().after(sub) || hospedagem.getHoraCheckin().equals(sub))
					&& (hospedagem.getHoraCheckout().before(soma) || hospedagem.getHoraCheckout().equals(soma))) {
				return QtdDiarias;
			} else {
				if (hospedagem.getHoraCheckin().before(sub)) {
					QtdDiarias += 1;
				}

				if (hospedagem.getHoraCheckout().after(soma)) {
					QtdDiarias += 1;
				}

				return QtdDiarias;
			}

		}

	}
}
