package br.com.cactus.flatsystem.util;

import br.com.cactus.flatsystem.entities.Hospedagem;

public class EventIPousada extends org.primefaces.model.DefaultScheduleEvent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Hospedagem hospedagem;

	public Hospedagem getHospedagem() {
		return hospedagem;
	}

	public void setHospedagem(Hospedagem hospedagem) {
		this.hospedagem = hospedagem;
	}

}
