package br.com.cactus.flatsystem.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * Classe utilizada para retornar o DataSource para gerar o relatório.
 * @author Lucas
 *
 */
public class JasperReportHandler {
	
	public static DataSource ds;
	
	public void setDs(DataSource ds) {
		this.ds = ds;
	}
	
	public static Connection getConexao() {
		Connection con = null;
		try {
			con = ds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public DataSource getDs() {
		return ds;
	}
	
}
