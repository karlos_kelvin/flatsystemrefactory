package br.com.cactus.flatsystem.util;

import java.util.GregorianCalendar;

import br.com.cactus.flatsystem.model.exception.DiaDaSemanaException;

public class MontaColunasSemanais {
	
	/**
	 * A partir de uma data o método retorna todos os dias da semana da data.
	 * 
	 * @param date
	 * @return array de DiaDaSemana
	 * @throws DiaDaSemanaException
	 * @see br.com.mkl.util.DiaDaSemana
	 */
	public static DiaDaSemana[] getDiasDaSemana(GregorianCalendar date) throws DiaDaSemanaException{
		DiaDaSemana[] retorno = new DiaDaSemana[7];
		
		GregorianCalendar c = new GregorianCalendar(date.get(date.YEAR), date.get(date.MONTH), date.get(date.DAY_OF_MONTH)); //data atual
		
		setToFirstDayOfWeek(c);
		
		int cont = 0;
		for(int i=0;i<7;i++){
			//GregorianCalendar ca = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), firstDay+i); //cria uma calendar para cada data
			c.add(c.DAY_OF_MONTH, cont);
			int dd        = c.get(c.DAY_OF_MONTH);
			int mm        = c.get(c.MONTH);
			int yyyy      = c.get(c.YEAR);
			int diaSemana = c.get(c.DAY_OF_WEEK);
			
			retorno[i] = new DiaDaSemana(diaSemana, dd, mm+1, yyyy);
			cont = 1;
		}		
		
		
		return retorno;
	}

	private static void setToFirstDayOfWeek(GregorianCalendar date) {
		int dia = date.get(date.DAY_OF_WEEK);
		
		switch (dia) {
		case 1:
			//date.get(date.DAY_OF_MONTH);
			break;
		case 2:
			date.add(date.DAY_OF_MONTH, -1);
			break;
		case 3:
			date.add(date.DAY_OF_MONTH, -2);
			break;
		case 4:
			date.add(date.DAY_OF_MONTH, -3);
			break;
		case 5:
			date.add(date.DAY_OF_MONTH, -4);
			break;
		case 6:
			date.add(date.DAY_OF_MONTH, -5);
			break;
		case 7:
			date.add(date.DAY_OF_MONTH, -6);
			break;
			
		default:
			break;
		}
		
	}
	
	
}
