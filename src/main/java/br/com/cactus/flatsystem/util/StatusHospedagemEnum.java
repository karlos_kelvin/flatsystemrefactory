package br.com.cactus.flatsystem.util;

public enum StatusHospedagemEnum {

	ABERTO("A","Aberto"), 
	FECHADO("F","Fechado");
	
	private final String flag;
	private final String descricao;

	public String getFlag() {
		return flag;
	}

	public String getDescricao() {
		return descricao;
	}

	private StatusHospedagemEnum(String flag, String descricao) {
		this.flag = flag;
		this.descricao = descricao;
	}
	
	
}
