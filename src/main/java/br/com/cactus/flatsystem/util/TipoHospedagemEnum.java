package br.com.cactus.flatsystem.util;

public enum TipoHospedagemEnum {

	PRE_RESERVA("P","Pré-Reserva"), 
	RESERVA("R","Reserva"),
	HOSPEDAGEM("H","Hospedagem");
	
	private final String flag;
	private final String descricao;

	public String getFlag() {
		return flag;
	}

	public String getDescricao() {
		return descricao;
	}

	private TipoHospedagemEnum(String flag, String descricao) {
		this.flag = flag;
		this.descricao = descricao;
	}
	
}
